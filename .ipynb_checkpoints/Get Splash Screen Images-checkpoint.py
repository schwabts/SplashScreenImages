# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py,md
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Splash Screen Images

# + [markdown] heading_collapsed=true
# ## Class Diagrams

# + hidden=true
# !pip install --q pylint --user

# + [markdown] hidden=true
# [pyreverse (1) - Linux Man Pages - SysTutorials](https://www.systutorials.com/docs/linux/man/1-pyreverse/)

# + hidden=true
# !pip install pydot

# + hidden=true
# !pyreverse -o dot -pdatetime datetime

# + hidden=true
import pydot

print("Creating graph ...")
(graph,) = pydot.graph_from_dot_file('classes_datetime.dot')
print("Creating PNG ...")
graph.write_png('classes_datetime.png')
print("done")

# + [markdown] hidden=true
# <img src="classes_datetime.png" />
# -

# ## Create New Collection Collecting Wallpapers From System

# ### Codebase 2-1 (Copy)

# +
from pathlib import Path
from datetime import date, timedelta
import shutil

# TODO: How about calling this SplashScreenCollectionFactory
#       or SplashScreenCollectionManager?
#       Then the Windows System folder with wallpapers is also to be handled
#       by SplashScreenCollection as well as the final folder Collection/
class SplashScreenCollector():
    ''' Provide code copying Splash Screen Images from Windows System folder
        to a new collection in a new folder named based on the current date.
    '''
    # Set source To Path To Folder Where Wallpapers Are Stored
    def __init__(self,username):
        ''' Set attribute  `windows_wallpapers_folder` to the path where wallpapers are stored
            and `src_file_list` to the list of names of all files in that folder
        '''
        self.username = username
        self.windows_wallpapers_folder = Path("/") / "Users" / username / \
            "AppData" / "Local" / "Packages" / \
            "Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy" / \
            "LocalState" / "Assets"
        self.src_file_list = [str(file) for file in self.windows_wallpapers_folder.iterdir()]
        self.get_target_from_timestamp()

    def get_windows_wallpapers_folder(self):
        return(self.windows_wallpapers_folder)

    def get_windows_wallpapers_number(self):
        return(len(self.src_file_list))

    def get_target(self):
        return(self.target)
        
    # Set target Deriving New Name For Destination Folder From Today's Time Stamp
    def get_target_from_timestamp(self):
        ''' Set attribute `target` to the a folder with the name "YYYYMMDD_i" in the current directory
            which does not yet exist using the first possible natural number i for this.

            Return Path() object of the path to this folder.
            
            Note:
            For Windows batch a solution of finding unique names was given in
            [How do I increment a folder name using Windows batch?](https://stackoverflow.com/questions/13328421/how-do-i-increment-a-folder-name-using-windows-batch).
        '''
        prefix = date.today().strftime("%Y%m%d") + "_"
        # count entries of current folder starting with "YYYYMMDD_" representing today
        list_prefixes_in_current_folder = list(Path(".").glob(prefix+"*"))  # Path().glob is iterable but no list
        running_index = len(list_prefixes_in_current_folder)
        # increment suffix for getting a new name
        running_index+=1
        # create Path object
        self.target = Path.cwd() / "{}{}".format(prefix,running_index)
        return(self.target)

    # Create dest_folder before copying since it obviously does not exist
    #     there are no (parent) folders to create between the current folder and dest_folder
    #     dest_folder was found to be a suitable name of a new folder which does not exist, yet.
    # Create Destination Folder
    def create_folder(self,path):
        ''' Create the (local) folder `path` the files in source are to be copied to

            Parameters:
            path    (Path)        path to folder in which to rename files
        '''
        if path.exists():
            # local target folder exists
            if any(Path(str(path)).iterdir()):
                # local folder path is not empty
                print(f"[FAIL] {path=} exists and is not empty")
                return(False)
            else:
                # local target folder is empty
                print(f"[ OK ] {path=} exists but is empty")
            return(True)
        else:
            print(f"[ OK ] {str(path)=} is missing")
            print(f'creating {path}/')
            path.mkdir(parents=False, exist_ok=False)
            return(True)
    
    # Copy Files
    def copy_files(self,from_path,to_path):
        ''' Copy files from source to target

            Parameters:
            from_path    (Path)         path to folder to copy files from
            to_path      (Path)         path to folder to copy files to
        '''
        print(f"copying from {str(from_path)=}")
        for path in from_path.iterdir():
            if path.is_file():
                if shutil.copy(path, to_path):
                    print(f"[ OK ] copied {str(path)=} to {str(to_path)}/")
                else:
                    print(f"[FAIL] unable to copy {str(path)=}")

    # Rename Files
    def rename_file(self,old_file,new_file):
        ''' Rename file with name old_file into new_file

            Parameters:
            old_file   (Path)       old name of file
            new_file   (Path)       new name of file
        '''
        try:
            old_file.rename(new_file)
            print("{}\n  -> {}".format(str(old_file),str(new_file)))
        except Exception as e:
            # print("{e}: unable to rename {old_file}")
            print(e)

    def remove_prefix(self,text,prefix):
        text_str = str(text)
        if text_str.startswith(prefix):
            return text_str[len(prefix):]
        return text_str  # or whatever

    def append_jpg_to_files_in_folder(self,path):
        ''' Add the extension ".jpg" to all files in folder `path`

            Parameters:
            path    (Path)         path to folder in which to rename files
        '''
        # DONE: rename all copied files in target/
        # TODO: check if folder is a Path-object
        for p in path.iterdir():
            # DONE: remove prefix "Path.cwd() /" from path
            # DOING: next line no longer needed?
            # p_str = remove_prefix(str(p), str(Path.cwd()))
            if p.is_file():
                new_name = p.stem + ".jpg"
                if p.rename(Path(p.parent) / new_name):
                    print(f"[ OK ] {p.stem} -> {new_name}")
                else:
                    print(f"[FAIL] unable to rename {str(p)=}")

    def collect(self):
        ''' Create new collection of wallpapers currently stored in the systems folder
        '''
        if self.create_folder(self.target):
            self.copy_files(self.windows_wallpapers_folder, self.target)
            self.append_jpg_to_files_in_folder(self.target)
        else:
            print(f'{self.target=}/ already created earlier!')


# + [markdown] heading_collapsed=true
# #### UML Class Diagrams

# + hidden=true
# !pyreverse -o dot -pSplashScreenCollector SplashScreenCollector

# + hidden=true
import pydot

print("Creating graph ...")
(graph,) = pydot.graph_from_dot_file('classes_SplashScreenCollector.dot')
print("Creating PNG ...")
graph.write_png('classes_SplashScreenCollector.png')
print("done")

# + [markdown] heading_collapsed=true
# #### TODO: Allow for manually selecting an alternative target

# + [markdown] hidden=true
# Uncomment the assignment if needed.

# + hidden=true
# collector.set_target('20220109_1', False)
# collector.set_target('C:/Users/schwa/Pictures/Splash Screen Images/20220109_1', True)
print(f'{collector.get_target()=}')
# -

# ### Usage

# +
collector = SplashScreenCollector("schwa")

print(f'{collector.get_windows_wallpapers_folder()=}')
print(f'{collector.get_windows_wallpapers_number()=}')
print(f'{collector.get_target()=}')

# print('\n'.join(collector.src_file_list))

# target = collector.get_target_from_timestamp()
# print(f"{collector.target=}")
# -

collector.collect()

# ## Show Wallpapers Already Collected

# ### Codebase 2-1 (Show)

# +
from pathlib import Path
from IPython.display import display, HTML, Image
import pandas as pd
from PIL import Image
from bs4 import BeautifulSoup
import requests 

# TODO: The Windows System folder with wallpapers is also to be handled
#       by SplashScreenCollection as well as the final folder Collection/
class SplashScreenCollection():
    ''' Manage a collection of Splash Screen Images in a folder by allowing for removal
        of non wallpaper files, clickable wallpaper display by HTML strings in boxes'
        results pointing the Google Reverse Image Search results, and creating commands
        for giving the wallpaper files meaningful names instead of Windows' hash keys.
    '''
    def __init__(self,folder_name,check_size=False):
        ''' Set attribute  `windows_wallpapers_folder` to the path where wallpapers are stored
            and `src_file_list` to the list of names of all files in that folder
            
            Parameters:
            folder_name       name of folder to search wallpapers in
            check_size        True:  Consider only the images with greatest dimensions
                              False: Take all images (dafault)
        '''
        self.check_size = check_size
        self.folder_name = folder_name
        self.folder = Path(".") / self.folder_name
        
        print(f'self.file_name_list = self.get_wallpapers(), {self.folder=}")')
        self.file_name_list = self._get_wallpapers()
    
    def catch(path,a):
        try:
            return(Image.open(str(path)).size[a])
        except Exception as e:
            return 0

    def _filter_wallpapers(self,path,width,height):
        '''
        Return True if `path` is the path name to an image file with a wallpaper
        Images are considered wallpapers if their size is `width` x `height`

        Parameters:
        path              Path() object of folder get wallpapers from
        width             Width an image must have for being able to be a wallpaper
        height            Height an image must have for being able to be a wallpaper
        check_size        True:  Consider only the images with greatest dimensions
                          False: Take all images (dafault)
        '''
        if not str(path).endswith(".jpg"):
            return(False)
        if self.check_size:
            if self.catch(path,0) != width:
                return(False)
            if self.catch(path,1) != height:
                return(False)
        return(True)
    
    def _get_wallpapers(self):
        '''
        Set attribute `wallpaper_path_names` to a list of path names of wallpapers in `self.folder`
        '''
        path_list = [path for path in self.folder.iterdir()]
        print(f'get_wallpapers: {len(path_list)} files in {str(self.folder)}')
        # print(f'    | {path_list=}')
        if self.check_size:
            width_list = [catch(path,0) for path in path_list if str(path).endswith(".jpg")]
            if len(width_list) == 0:
                # print("    | No images with positive width in {}".format(folder.name))
                return 0,0,[]
            width = max(width_list)
            # print(f'    | {width=}')

            height_list = [catch(path,1) for path in path_list if str(path).endswith(".jpg") and catch(path,0)==width]
            if len(height_list) == 0:
                # print("    | No images with positive height in {}".format(folder.name))
                return 0,0,[]
            height = max(height_list)
            # print(f'    | {height=}')

        # FIXME: NameError: free variable 'width' referenced before assignment in enclosing scope
        # TODO:  Obviously width = None and height = None if not check_size
        result = []
        if self.check_size:
            result = [str(path) for path in path_list if self._filter_wallpapers(path,width,height)]
            # print(f'    | {len(result)=} wallpapers of size {width}x{height}')
            # print(f'    | {result=}')
            return result
        else:
            result = [str(path) for path in path_list]
            # print(f'    | {len(result)=} wallpapers')
            # print(f'    | {result=}')
            return result
    
    def remove_files_from_folder_unless_listed(self): # ,path_list):
        '''
        Remove all files from folder `folder` that are not contained in the list `path_list` of path names
        
        No longer needed Parameters:
        path_list         list of path names of the files which must not be deleted
        folder            Path() object of folder get wallpapers from
        '''
        # folder = Path(".") / self.folder_name
        for path in self.folder.iterdir():
            if str(path) in self.file_name_list:
                print("keeping {} in {}/".format(str(path),self.folder_name))
            else:
                print("removing {} from {}/".format(str(path),self.folder_name))
                path.unlink()
    
    def remove_prefix(self,text, prefix):
        if text.startswith(prefix):
            return text[len(prefix):]
        return text  # or whatever

    # convert your links to html tags 
    def path_to_image_html(self,path):
        '''
        This function essentially converts a path to a local image to
        '<img src="'+ path + '" />' format. And one can put any
        formatting adjustments to control the height, aspect ratio, size etc.
        within as in the below example.

        Parameters:
        path              Path() object of local image
        '''
        # remove_prefix(str(path), str(Path.cwd())+'\\')
        result = '<img src="'+ str(path) + '" />'
        print(f'path_to_image_html({path=}):\n    {result}')
        return result

    def clickable_image_html(self,path):
        '''
        Convert path to a local image to the image tag returned by
        path_to_image_html() inside an <a href...> tag pointing to
        a link to the results of a Google Reverse Image Search
        obtained as answer to a post request.

        Parameters:
        path              Path() object of local image
        '''
        # DONE: check for quoted entries instead of for extension ".jpg"
        if str(path).startswith("<") and str(path).endswith(">"):
            # DONE: remove quotes from path if path is quoted
            return str(path).lstrip("<").rstrip(">")
        else:
            image_html_str = path_to_image_html(path)
            filePath = fr"{str(path)}"
            searchUrl = 'http://www.google.com/searchbyimage/upload'
            multipart = {'encoded_image': (filePath, open(filePath, 'rb')), 'image_content': ''}
            response = requests.post(searchUrl, files=multipart, allow_redirects=False)
            fetchUrl = response.headers['Location']
            # location = location_of_image(fetchUrl)
            return '<a href="{}">{}</a><br>'.format(fetchUrl,image_html_str) # "...{}".format(...,location)

    def display_dataframe(self): # ,file_list): # ,folder_name):
        '''
        For each file in the folder passed as Path object two rows of a dataframe
        with the quoted and unquoted name of the path to the file are created.
        Formatting with the function `clickable_image_html()` is applied when
        converting to html the result of which is passed to display(HTML())
        
        No longer needed Parameters:
        file_list     - list of pathes to the wallpapers to build a dataframe with
        folder_name   - name of folder with the wallpapers,
        '''
        wallpaper_column='Wallpapers in {}/'.format(str(self.folder_name))
        quoted_file_list = [ f'<{file}>' for file in self.file_name_list]
        # DONE: insert quote entries of file_list at rows with indices 1+(2)
        files = [None]*(len(self.file_name_list)+len(quoted_file_list))
        files[::2] = quoted_file_list
        files[1::2] = file_list
        # print(f'{files=}') # OK
        df = pd.DataFrame( files, columns = [wallpaper_column] )
        # OR TODO: quote entries of file_list and insert them at rows with indices 1+(2)
        
        # TODO: Maybe this won't work with clickable_image_html() being a method!
        format_dict = { wallpaper_column: self.clickable_image_html }
        html_str=df.to_html(escape=False, formatters=format_dict)
        display(HTML(html_str))
    
    def print_script_renaming_wallpapers(self): # ,file_list): # ,self.folder_name):
        '''
        Output a script renaming the files given by the list file_list all of which
        need to be in the folder with name folder_name in the current directory.

        Parameters:
        file_list            list of files to be renamed in folder with name folder_name
        folder_name          name of folder in current directory
        '''
        print("from pathlib import Path\n")
        print(f'folder_name = "{self.folder_name}"')
        print(f'folder = Path(".") / folder_name\n')
        for file_name in self.file_name_list:
            path = Path(file_name)
            print(f'old_file = folder / "{path.name}"')
            print(f'new_file = folder / "TTTTT.jpg"')
            print("rename_file(old_file,new_file)\n")
        print("print(\"\\nDo NOT forget to reload collection!\")")
    
    # Size of wallpapers in collection doesn't need to be standardised,
    # No need to check it because Collection/ does not contain non-wallpaper files!

    def display_and_prepare_renaming(self): # ,folder_name,check_size=False):
        '''
        Search wallpapers in folder with name folder_name and show them in a dataframe
        with links to results of respective Google Reverse Image Searches.

        Parameters:
        folder_name       name of folder to search wallpapers in
        check_size        True:  Consider only the images with greatest dimensions
                          False: Take all images (dafault)
        '''
        # folder = Path(".") / folder_name
        
        # print(f'file_name_list = get_wallpapers("{self.folder=}")')
        # file_name_list = _get_wallpapers()
        # print(f'    | {self.file_name_list=}')
        
        display_dataframe() # file_list=self.file_name_list) # ,folder_name=folder.name)
        if len(self.file_name_list)>0:
            print(self.print_script_renaming_wallpapers()) # self.file_name_list,folder.name))
        return(file_name_list)
    
    def reduce_to_wallpapers(self): # ,folder_name,check_size=False):
        '''
        Show dataframe of wallpapers in folder `folder_name` with links to Google Reverse
        Image Search results and remove all non-wallpaper files in that folder

        Parameters:
        folder_name       name of folder to search wallpapers in
        check_size        True:  Consider only the images with greatest dimensions
                          False: Take all images (dafault)
        '''
        # file_name_list = display_and_prepare_renaming(folder_name,check_size)
        self.display_and_prepare_renaming()
        self.remove_files_from_folder_unless_listed() # folder_name,file_name_list)
        return(self.file_name_list)
    
    def copy_wallpapers(self,source,target):
        '''Copy all files contained in list `source` to `target`

        Parameters:
        source   (list of strings)    list of relative path names of files to be copied
        target   (Path)               relative path to folder to copy files to

        Wanted Parameters (just because source should always match self.file_name_list):
        target   (Path)               relative path to folder to copy files to
        '''
        for path in [Path(name) for name in source]:
            if path.is_file():
                if not((target / path.name).is_file()):
                    shutil.copy(path, target)
                    print(f'[ OK ] Wallpaper "{str(target / path.name)}" collected')
                else:
                    print(f'[FAIL] Wallpaper "{str(target / path.name)}" already exists')



# -

# #### Get List of Wallpapers in `folder/`

# +
def catch(path,a):
    try:
        return(Image.open(str(path)).size[a])
    except Exception as e:
        return 0

def filter_wallpapers(path,width,height,check_size=False):
    '''
    Return True if `path` is the path name to an image file with a wallpaper
    Images are considered wallpapers if their size is `width` x `height`
    
    Parameters:
    path              Path() object of folder get wallpapers from
    width             Width an image must have for being able to be a wallpaper
    height            Height an image must have for being able to be a wallpaper
    check_size        True:  Consider only the images with greatest dimensions
                      False: Take all images (dafault)
    '''
    if not str(path).endswith(".jpg"):
        return(False)
    if check_size:
        if catch(path,0) != width:
            return(False)
        if catch(path,1) != height:
            return(False)
    return(True)

def get_wallpapers(folder,check_size=False):
    '''
    Return a list of path names of wallpapers in folder `folder`
    
    Parameters:
    folder            Path() object of folder get wallpapers from
    check_size        True:  Consider only the images with greatest dimensions
                      False: Take all images (dafault)
    '''
    path_list = [path for path in folder.iterdir()]
    print(f'get_wallpapers: {len(path_list)} files in {str(folder)}')
    # print(f'    | {path_list=}')
    if check_size:
        width_list = [catch(path,0) for path in path_list if str(path).endswith(".jpg")]
        if len(width_list) == 0:
            # print("    | No images with positive width in {}".format(folder.name))
            return 0,0,[]
        width = max(width_list)
        # print(f'    | {width=}')

        height_list = [catch(path,1) for path in path_list if str(path).endswith(".jpg") and catch(path,0)==width]
        if len(height_list) == 0:
            # print("    | No images with positive height in {}".format(folder.name))
            return 0,0,[]
        height = max(height_list)
        # print(f'    | {height=}')
    
    # FIXME: NameError: free variable 'width' referenced before assignment in enclosing scope
    # TODO:  Obviously width = None and height = None if not check_size
    result = []
    if check_size:
        result = [str(path) for path in path_list if filter_wallpapers(path,width,height,check_size)]
        # print(f'    | {len(result)=} wallpapers of size {width}x{height}')
        # print(f'    | {result=}')
        return result
    else:
        result = [str(path) for path in path_list]
        # print(f'    | {len(result)=} wallpapers')
        # print(f'    | {result=}')
        return result

def remove_files_from_folder_unless_listed(folder_name,path_list):
    '''
    Remove all files from folder `folder` that are not contained in the list `path_list` of path names
    
    Parameters:
    folder            Path() object of folder get wallpapers from
    path_list         list of path names of the files which must not be deleted
    '''
    folder = Path(".") / folder_name
    for path in folder.iterdir():
        if str(path) in path_list:
            print("keeping {} in {}/".format(str(path),folder_name))
        else:
            print("removing {} from {}/".format(str(path),folder_name))
            path.unlink()

            
def remove_non_wallpapers(folder_name):
    '''
    Remove all files from folder `folder` that are not contained in the list `path_list` of path names
    
    Parameters:
    folder_name       name of folder to get wallpapers from
    '''
    folder = Path(".") / folder_name
    counter = 0
    for path in folder.iterdir():
        file_str = remove_prefix(str(path), folder_name+'/')
        file_str = remove_prefix(file_str, folder_name+'\\')
        if file_str[0].isupper():
            counter+=1
            print("File {}: keeping {} in {}/".format(counter,file_str,folder_name))
        else:
            # print("removing {} from {}/".format(path,folder_name))
            path.unlink()



# -

# #### Create and Display Dataframe With  Images and Links to Results of Google Reverse Image Searches

# Using the package
# [Google-Images-Search - PyPI](https://pypi.org/project/Google-Images-Search/)
# requires setting up a Google developers account with
# * a project
# * enabled Google Custom Search API, and
# * generated API key credentials
#
# but does not facilitate **Reverse** Image Searches.

# The function `clickable_image_html(path)` below implements the **reverse image search workflow** as shown in the answer
# > [Google reverse image search using POST request](https://stackoverflow.com/questions/23270175/google-reverse-image-search-using-post-request)
#
# on Stack Overflow.

# +
from pathlib import Path
from IPython.display import display, HTML, Image
import pandas as pd
from PIL import Image
from bs4 import BeautifulSoup
import requests 

def location_of_image(url):
    '''
    Get the results of the Google Reverse Image Search given by `url` and 
    try to determine where the photo was taken
    
    Parameters:
    url             - link to  the results page of a Google Reverse Image Search
    '''
    usr_agent = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      # 'Chrome/61.0.3163.100 Safari/537.36'
    }
    def fetch_results(search_url):
        response = requests.get(search_url, headers=usr_agent)
        response.raise_for_status()
        return response.text

    def parse_results(raw_html):
        soup = BeautifulSoup(raw_html, 'html.parser')
        result_block = soup.find_all('div', attrs={'class': 'g'})
        for result in result_block:
            link = result.find('a', href=True)
            title = result.find('h3')
            text = link.get_text()
            if link and title:
                yield text[:text.index("https://")] # + " -> " + link['href']
    
    # return("location_of_image({})".format(url))
    
    html = fetch_results(url)
    results = list(parse_results(html))
    # results = ["links","from","Google"]
    results = [ "<li>{}</li>".format(elem) for elem in results ]
    return ''.join(['<ol>',''.join(results),'</ol>'])

def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text  # or whatever

# convert your links to html tags 
def path_to_image_html(path):
    '''
    This function essentially converts a path to a local image to
    '<img src="'+ path + '" />' format. And one can put any
    formatting adjustments to control the height, aspect ratio, size etc.
    within as in the below example.
    
    Parameters:
    path
    '''
    # remove_prefix(str(path), str(Path.cwd())+'\\')
    result = '<img src="'+ str(path) + '" />'
    print(f'path_to_image_html({path=}):\n    {result}')
    return result

def clickable_image_html(path):
    '''
    This function essentially converts a path to a local image to
    the image tag returned by path_to_image_html() inside an
    <a href...> tag pointing to a link to the results of a Google
    Reverse Image Search obtained as answer to a post request.
    
    Parameters:
    path
    '''
    # DONE: check for quoted entries instead of for extension ".jpg"
    if str(path).startswith("<") and str(path).endswith(">"):
        # DONE: remove quotes from path if path is quoted
        return str(path).lstrip("<").rstrip(">")
    else:
        image_html_str = path_to_image_html(path)
        filePath = fr"{str(path)}"
        searchUrl = 'http://www.google.com/searchbyimage/upload'
        multipart = {'encoded_image': (filePath, open(filePath, 'rb')), 'image_content': ''}
        response = requests.post(searchUrl, files=multipart, allow_redirects=False)
        fetchUrl = response.headers['Location']
        # location = location_of_image(fetchUrl)
        return '<a href="{}">{}</a><br>'.format(fetchUrl,image_html_str) # "...{}".format(...,location)

def display_wallpapers_df(file_list,folder_name):
    '''
    For each file in the folder passed as Path object two rows of a dataframe
    with the quoted and unquoted name of the path to the file are created.
    Formatting with the function `clickable_image_html()` is applied when
    converting to html the result of which is passed to display(HTML())
    
    Parameters:
    file_list     - list of pathes to the wallpapers to build a dataframe with
    folder_name   - name of folder with the wallpapers,
    '''
    wallpaper_column='Wallpapers in {}/'.format(str(folder_name))
    quoted_file_list = [ f'<{file}>' for file in file_list]
    # DONE: insert quote entries of file_list at rows with indices 1+(2)
    files = [None]*(len(file_list)+len(quoted_file_list))
    files[::2] = quoted_file_list
    files[1::2] = file_list
    # print(f'{files=}') # OK
    df = pd.DataFrame( files, columns = [wallpaper_column] )
    # OR TODO: quote entries of file_list and insert them at rows with indices 1+(2)
    format_dict = { wallpaper_column: clickable_image_html }
    html_str=df.to_html(escape=False, formatters=format_dict)
    display(HTML(html_str))



# -

# #### Create Code For Renaming Wallpapers in Specified `folder/`

# `rename_file()` was defined in subsection
# [1.5 Rename Files](#Rename-Files)

def print_script_renaming_wallpapers(file_list,folder_name):
    '''
    Output a script renaming the files given by the list file_list all of which
    need to be in the folder with name folder_name in the current directory.
    
    Parameters:
    file_list            list of files to be renamed in folder with name folder_name
    folder_name          name of folder in current directory
    '''
    print("from pathlib import Path\n")
    print(f'folder_name = "{folder_name}"')
    print(f'folder = Path(".") / folder_name\n')
    for file_name in file_list:
        path = Path(file_name)
        print(f'old_file = folder / "{path.name}"')
        print(f'new_file = folder / "TTTTT.jpg"')
        print("rename_file(old_file,new_file)\n")
    print("print(\"\\nDo NOT forget to reload collection!\")")


# #### Combined Collection Tasks

# Rename Files
def rename_file(old_file,new_file):
    ''' Rename file with name old_file into new_file

        Parameters:
        old_file   (Path)       old name of file
        new_file   (Path)       new name of file
    '''
    try:
        old_file.rename(new_file)
        print("{}\n  -> {}".format(str(old_file),str(new_file)))
    except Exception as e:
        # print("{e}: unable to rename {old_file}")
        print(e)


# +
# Size of wallpapers in collection doesn't need to be standardised,
# No need to check it because Collection/ does not contain non-wallpaper files!

def display_and_prepare_renaming_wallpapers(folder_name,check_size=False):
    '''
    Search wallpapers in folder with name folder_name and show them in a dataframe
    with links to results of respective Google Reverse Image Searches.
    
    Parameters:
    folder_name       name of folder to search wallpapers in
    check_size        True:  Consider only the images with greatest dimensions
                      False: Take all images (dafault)
    '''
    folder = Path(".") / folder_name
    print(f'file_name_list = get_wallpapers("{folder=}")')
    file_name_list = get_wallpapers(folder,check_size)
    print(f'    | {file_name_list=}')
    display_wallpapers_df(file_list=file_name_list,folder_name=folder.name)
    if len(file_name_list)>0:
        print(print_script_renaming_wallpapers(file_name_list,folder.name))
    return(file_name_list)



# -

def reduce_to_wallpapers(folder_name,check_size=False):
    '''
    Show dataframe of wallpapers in folder `folder_name` with links to Google Reverse
    Image Search results and remove all non-wallpaper files in that folder
    
    Parameters:
    folder_name       name of folder to search wallpapers in
    check_size        True:  Consider only the images with greatest dimensions
                      False: Take all images (dafault)
    '''
    file_name_list = display_and_prepare_renaming_wallpapers(folder_name,check_size)
    remove_files_from_folder_unless_listed(folder_name,file_name_list)
    return(file_name_list)


# #### Help for functions

# ?get_wallpapers

# ?remove_files_from_folder_unless_listed

# #### Show  folders with collections

# +
import pathlib

def add_spaces(text):
    return(text + '    ')

def add_newline(text):
    return(add_spaces(text + '\n'))

def print_subsection_for_folder(path):
    text = add_newline(f'#### Show Wallpapers in {path}/')
    text += add_newline(f'folder_name = "{path}"')
    if str(path).startswith( 'Collection' ):
        text += 'file_name_list = reduce_to_wallpapers(folder_name,False)'
    else:
        text += 'file_name_list = reduce_to_wallpapers(folder_name,True)'
    print("\n"+text)

for p in pathlib.Path('.').iterdir():
    if p.is_dir():
        if str(p).startswith( '.' ):
            continue
        print_subsection_for_folder(p)


# + [markdown] heading_collapsed=true
# ### 2021

# + [markdown] heading_collapsed=true hidden=true
# #### Show Wallpapers in 20211214_1/

# + hidden=true
file_name_list = display_and_prepare_renaming_wallpapers("20211214_1",True)

# + [markdown] heading_collapsed=true hidden=true
# #### Show Wallpapers in 20211214_2/

# + hidden=true
folder_name = "20211214_2"
file_name_list = display_and_prepare_renaming_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder = Path(".") / "20211214_2"

old_file = folder / "42ed0cdf80111860edb0fd0ebedc4d61ab0bbab864bd8fbe52ed44a027f7850c.jpg"
new_file = folder / "Naturpark bayerische Rhön mit Lupinenfeld bei Sonnenaufgang, Deutschland.jpg"
old_file.rename(new_file)

old_file = folder / "46c4dd0006b8e42cafd420164c7f4186b6595d3b87bea683c30ab82065b356f9.jpg"
new_file = folder / "Lanikai Beach in Kailua mit Moku Nui und Moku Iki, Oahu, HI, USA.jpg"
old_file.rename(new_file)

old_file = folder / "cf2067f09d5f988f2e5f3477ad4c79889b2e49cff9edaa09c904c3f17ef9b949.jpg"
new_file = folder / "Nationalpark Virgin-Islands.jpg"
old_file.rename(new_file)

old_file = folder / "d98d37454bbda7e45887b764482edb422b24eaf35ac6738ef0d663f0f197453c.jpg"
new_file = folder / "London, Luftaufnahme der Tower Bridge, England, UK.jpg"
old_file.rename(new_file)

old_file = folder / "daa75bc777aec60877cef680d76ad29a48bd8c4c596597c811a612da6e182323.jpg"
new_file = folder / "Los Angeles, Palm Trees Street zur Innenstadt, CA, USA.jpg"
old_file.rename(new_file)

old_file = folder / "e705d87513642691227a3e873bd9cb95c0991af676d9677dc0f752f342898f90.jpg"
new_file = folder / "Pyramiden von Gizeh, Kairo, Ägypten.jpg"
old_file.rename(new_file)

# + hidden=true
remove_non_wallpapers("20211214_2",file_name_list)
# # ?remove_non_wallpapers

# + [markdown] heading_collapsed=true hidden=true
# #### Show Wallpapers in 20211216_1/

# + hidden=true
folder_name = "20211216_1"
file_name_list = display_and_prepare_renaming_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder = Path(".") / folder_name

# old_file = folder / "2d697ab2e3cb6a9b93651304b8933e3a9c936fc969d00e45ffe4f7accd5eb1e2.jpg"
# new_file = folder / "Burg Berlanga de Duero, Provinz Soria, Kastilien und Leon, Spanien.jpg"
# old_file.rename(new_file)

# old_file = folder / "703a7716b10321e1c218dced95053070ba4ea7bd1a5968666b1196ddf9e53a04.jpg"
# new_file = folder / "Scheveningen, Den Haag, Niederlande.jpg"
# old_file.rename(new_file)

# old_file = folder / "cf7fbfa5f6d2ff49c06f62f31b580689b9406adc040950443685384c47bf9753.jpg"
# new_file = folder / "Ägerisee, Schweiz.jpg"
# old_file.rename(new_file)

# old_file = folder / "d98d37454bbda7e45887b764482edb422b24eaf35ac6738ef0d663f0f197453c.jpg"
# new_file = folder / "London in Luftaufnahme, England, UK.jpg"
# old_file.rename(new_file)

# old_file = folder / "London in Luftaufnahme, England, UK.jpg"
# new_file = folder / "London, Luftaufnahme der Tower Bridge, England, UK"
# old_file.rename(new_file)

# old_file = folder / "df367cf955beb6676a10a66b802669a85803c0015cb0fbf614b6bf36d0091886.jpg"
# new_file = folder / "Bay - Playa del Silencio in Cudillero, Asturien, Spanien.jpg"
# old_file.rename(new_file)

# old_file = folder / "e605dbbee3f37a10d2d110ccf608a16fa01aebec5f35dc5e735de39da6890001.jpg"
# new_file = folder / "Nationalpark Theodore-Roosevelt, Inselberg in Badland-Landschaft, North Dakota, USA.jpg"
# old_file.rename(new_file)

# + hidden=true
remove_non_wallpapers(folder_name)

# + [markdown] heading_collapsed=true hidden=true
# #### Show Wallpapers 20211217_1/

# + [markdown] hidden=true
# StackOverflow: [How to do reverse image search on google by uploading image url?](https://stackoverflow.com/questions/59176559/how-to-do-reverse-image-search-on-google-by-uploading-image-url)

# + hidden=true
folder_name = "20211217_1"
file_name_list = file_name_list = display_and_prepare_renaming_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

old_file = folder / "25e8925fb4ec185082b2d5364ff1ff8eb0ce89754b778c2eebf61e10c0f5e2cc.jpg"
new_file = folder / "New York City, NY, USA.jpg"
old_file.rename(new_file)

old_file = folder / "703a7716b10321e1c218dced95053070ba4ea7bd1a5968666b1196ddf9e53a04.jpg"
new_file = folder / "Scheveningen, Den Haag, Niederlande.jpg"
old_file.rename(new_file)

old_file = folder / "80719d8b250b430d5fa043164cbaf8e0c80d4493d1c94274171621592bdce083.jpg"
new_file = folder / "Coron Island, Palawan, Philippinen.jpg"
old_file.rename(new_file)

old_file = folder / "b5de60216621698a18af602cb3215d4046ed0515cdfe07599d7128952b336a11.jpg"
new_file = folder / "Miyakojima-Insel, Okinawa, Japan.jpg"
old_file.rename(new_file)

old_file = folder / "df367cf955beb6676a10a66b802669a85803c0015cb0fbf614b6bf36d0091886.jpg"
new_file = folder / "Bay - Playa del Silencio in Cudillero, Asturien, Spanien.jpg"
old_file.rename(new_file)

# + hidden=true
remove_non_wallpapers(folder_name)
# -

# ### 2022

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220101_1/

# + hidden=true
# file_name_list = display_and_prepare_renaming_wallpapers("20220101_1",True)
file_name_list = reduce_to_wallpapers("20220101_1",True)

# + hidden=true
from pathlib import Path

old_file = folder / "58fbc0f1cfe19d326edbf51b74c61995c3386dc92f4848bd73596097ef2051a1.jpg"
new_file = folder / "Naturpark - Valley of Fire, NV, USA.jpg"
old_file.rename(new_file)

old_file = folder / "2a2a3438ed1ff5525b8089ec285a7b30fa3135134f7547f9b55fdad87f9b62ce.jpg"
new_file = folder / "Nationalpark Los Glaciares, Berg Cerro Torre, Patagonien, Argentinien.jpg"
old_file.rename(new_file)

old_file = folder / "a46575de4ee21b793677cde31b37d0cd7eb6081a1dfa59b816ed8138c7fe58c0.jpg"
new_file = folder / "Nationalpark Amboró, Santa Cruz, Bolivien.jpg"
old_file.rename(new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220104_1

# + hidden=true
# file_name_list = display_and_prepare_renaming_wallpapers("20220104_1",True)
file_name_list = reduce_to_wallpapers("20220104_1",True)

# + hidden=true
from pathlib import Path

folder = Path(".") / "20220104_1"

old_file = folder / "0657a2d3fd21087aea9bf1831e13bde67796995742c70173d2d9c965b4965ad4.jpg"
new_file = folder / "Gemälde Canadian Rockies (Lake Louise) von Albert Bierstadt, The Metropolitan Museum of Art.jpg"
old_file.rename(new_file)

old_file = folder / "0fedc38e49a5796f5bfdbc7fde6553b944486b2b76e7a0f2043e489f98a2c72d.jpg"
new_file = folder / "Abtei Mont Saint-Michel, UNESCO-Welterbe, Normandie, Frankreich, Departement Manche.jpg"
old_file.rename(new_file)

old_file = folder / "119991f059b995449935e0bd4993f7edccd8a916900110f7c7e737bad1511c0e.jpg"
new_file = folder / "natural landscape.jpg"
old_file.rename(new_file)

old_file = folder / "2bfe9a4e72c787f17812dc49bb926f38ba3d20ccaad47c7422198e97fbdeaade.jpg"
new_file = folder / "Wasserfall - Saltos de Petrohué, Nationalpark Vicente Pérez Rosales, Región de los Lagos, Chile.jpg"
old_file.rename(new_file)

old_file = folder / "c5827f7a5f4a5ba9c10cba9ca949b2861e2961e5cdb21cfae9cf1b2fb68fbd13.jpg"
new_file = folder / "Banyak Inseln, tropischer Archipel nahe Sumatra, Aceh, Indonesien.jpg"
old_file.rename(new_file)

old_file = folder / "c75b76e8b7a1fca82786f9bb9723cbb616c00ea0ed7ec5bf88aba0e9f1e88ee5.jpg"
new_file = folder / "Wasserfall - Nationalpark Plitvicer Seen 1, Kroatien.jpg"
old_file.rename(new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220106/

# + hidden=true
# file_name_list = display_and_prepare_renaming_wallpapers("20220106_1",True)
file_name_list = reduce_to_wallpapers("20220106_1",True)

# + hidden=true
path = Path(".") / "20220106_1"
file_name_list = get_wallpapers(path,True)
print("filtered {}  wallpapers {}".format(len(file_name_list),file_name_list))
create_script_rename_wallpapers(file_name_list,path.name)

# + hidden=true
folder = Path(".") / "20220106_1"

old_file = folder / "1a9932db03aea52ac08cf20acb418d833508e4c714475c632e1391f84a2e8143.jpg"
new_file = folder / "Astronomie - Balken-Spiralgalaxie NGC 2835, Auge der Schlange.jpg"
old_file.rename(new_file)

old_file = folder / "1b73bbcc0b8b68102a602d55f543a4ee36c004a1a9dcd2f944b0381674d30a1e.jpg"
new_file = folder / "Weiße Wüste, Sahara, Ägypten.jpg"
old_file.rename(new_file)

old_file = folder / "fab2956f48be1814ceb39a0e7816f6a0d2362707a178b1b2860c175672c52e79.jpg"
new_file = folder / "Wasserfall - Nationalpark Plitvicer Seen 5, Kroatien.jpg"
old_file.rename(new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220109/

# + hidden=true
# file_name_list = display_and_prepare_renaming_wallpapers("20220109_1",True)
file_name_list = reduce_to_wallpapers("20220109_1",True)

# + hidden=true
from pathlib import Path

folder = Path(".") / "20220109_1"

old_file = folder / "21c716aacd02cc7c1f298324714343ae1a08e4c44220ea3c2c045c25e7e6730d.jpg"
new_file = folder / "Brücke - Pont Jacques-Cartier zwischen Montreal und Longueuil über den Sankt-Lorenz-Strom, Québec, Kanada.jpg"
old_file.rename(new_file)

old_file = folder / "441814265ba97f274c75253bcfa7af180ed558c51ce474c196c43ef5368f6536.jpg"
new_file = folder / "Naturpark Drei Zinnen mit Kriegstunnel, Sextner Dolomiten, Südtirol, Italien.jpg"
old_file.rename(new_file)

old_file = folder / "d5c2fcebbe32f15b4a2734b21a59d5eed92e055980f51b5c2b4def65cff29914.jpg"
new_file = folder / "Eisfeld Perito Moreno mit blauer Eislagune, Departement Lago Argentino, Argentinien.jpg"
old_file.rename(new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220111/

# + hidden=true
# file_name_list = display_and_prepare_renaming_wallpapers("20220111_1",True)
file_name_list = reduce_to_wallpapers("20220111_1",True)

# + hidden=true
from pathlib import Path

folder = Path(".") / "20220111_1"

old_file = folder / "Nationalpark - Künstlerische Muster bei New Blue Spring im Winter im Yellowstone-Nationalpark, Wyoming, USA.jpg"
new_file = folder / "New Blue Spring, artistic patterns in winter, Yellowstone National Park, Wyoming, USA.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220115_1/

# + hidden=true
folder_name = "20220115_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder = Path(".") / "20220115_1"

old_file = folder / "3383b3262748a5940c87f523f33d09c519ba1c62270990fe7e0f465ce16ebf23.jpg"
new_file = folder / "Manzanillo-Wildschutzgebiet, Limón, Costa Rica.jpg"
rename_file(old_file,new_file)

old_file = folder / "4d1a0d10b3c57e22cf264c330e2636028752cc77670ae7c2c6430dadb4587e81.jpg"
new_file = folder / "Berg Sinai - auch Jebel Musa, 2285 m, bei Sonnenaufgang, Sinai Halbinsel, Ägypten.jpg"
rename_file(old_file,new_file)

old_file = folder / "4e715737f0205d589a3c69f14a43eecdbb209f4fb85cc8b94425cbf67a887bbe.jpg"
new_file = folder / "Hawa Mahal, Palast Der Winde, Jaipur, Rajasthan, Indien.jpg"
rename_file(old_file,new_file)

old_file = folder / "7efd92b2b4a3c51c9617f632f7f8b2357b9bbe280ceb234407fb1ec3ca86d29c.jpg"
new_file = folder / "Nationalpark Sutjeska - Zelengora Berggipfel und Wiesen, Bosnien und Herzegowina.jpg"
rename_file(old_file,new_file)

old_file = folder / "b02c4d5cf33455e626fb0bb26a2ad3f7d816badd3cc7aa2afa8a572f1049c5ec.jpg"
new_file = folder / "Celeste-Fluss im Tenorio-Nationalpark, Costa Rica.jpg"
rename_file(old_file,new_file)

old_file = folder / "e705d87513642691227a3e873bd9cb95c0991af676d9677dc0f752f342898f90.jpg"
new_file = folder / "Pyramiden, Gizeh, Kairo, Ägypten.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220125_1/

# + hidden=true
folder_name = "20220125_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder = Path(".") / folder_name

old_file = folder / "21c716aacd02cc7c1f298324714343ae1a08e4c44220ea3c2c045c25e7e6730d.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "91c25cbdab597876f8556003fe7e3ff4295203be72ebcb50bec44638f304bcf5.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "c7b92a9b31fe16ecf9c63c5194430a10ef40f536eaa1a245e9e4466213ff5fea.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "cd4a6ee06e14572dcfba70e6c0e2b0a122c8ea30bc20afe3dd91b5d0cd3c29de.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "df2e8a3871930360adad925df3e25e36803ad135a93f5ff40050da7505a06543.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "f492102ba254bf96103ac449b62b9133409db418f657c9e710cb874f93aa0fd4.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220130_1/

# + hidden=true
folder_name = "20220130_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220130_1"
folder = Path(".") / folder_name

old_file = folder / "46edd46c145dbb53e9990ba7fa6dc58f92f30312919419fb5ee4f0f36e12a84c.jpg"
new_file = folder / "Savanne von Eichen, dehesa, La Serena, Badajoz, Extremadura, Spanien.jpg"
rename_file(old_file,new_file)

old_file = folder / "4ce05edae0003bf62e50c9e64f39709f6781bba60525e5ee058b51cbc12dc357.jpg"
new_file = folder / "Brücke - Rakotzbrücke im Rhododendronpark in Gablenz-Kromlau, Sachsen, Deutschland.jpg"
rename_file(old_file,new_file)

old_file = folder / "5e30e1aaf94465039ab1843ea1fbe9fb15b9230f4b3796c9deeed2cad6a9653a.jpg"
new_file = folder / "Adivino-Pyramide des Wahrsagers, Uxmal, Yucatán, México.jpg"
rename_file(old_file,new_file)

old_file = folder / "cd4a6ee06e14572dcfba70e6c0e2b0a122c8ea30bc20afe3dd91b5d0cd3c29de.jpg"
new_file = folder / "Pyramiden von Gizeh im Drohnenfoto, Kairo, Ägypten.jpg"
rename_file(old_file,new_file)

old_file = folder / "dc479424f1f2e36c58e0bb6022bb13968c3461ca3f08eb722ef81c9f6fe44470.jpg"
new_file = folder / "Toge, Tokamachi, Niigata 942-1351, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "f4d5d34c3a2f99e445adb70798ec962c2da20a433dafc5b46c3f9fcfbc61b658.jpg"
new_file = folder / "Sentinel vom Chapman's Peak Drive aus, Kap-Halbinsel, Südafrika.jpg"
rename_file(old_file,new_file)


# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220131_1/

# + hidden=true
folder_name = "20220131_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220131_1"
folder = Path(".") / folder_name

old_file = folder / "4ce05edae0003bf62e50c9e64f39709f6781bba60525e5ee058b51cbc12dc357.jpg"
new_file = folder / "Brücke - Rakotzbrücke im Rhododendronpark in Gablenz-Kromlau, Sachsen, Deutschland.jpg"
rename_file(old_file,new_file)

old_file = folder / "51fb60ba2a872abad6e58600bbd6fb92a679bb499c6c63c18de1b0b497f77de4.jpg"
new_file = folder / "Dead Horse Point State Park, Utah, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "5e30e1aaf94465039ab1843ea1fbe9fb15b9230f4b3796c9deeed2cad6a9653a.jpg"
new_file = folder / "Adivino-Pyramide des Wahrsagers, Uxmal, Yucatán, México.jpg"
rename_file(old_file,new_file)

old_file = folder / "dc479424f1f2e36c58e0bb6022bb13968c3461ca3f08eb722ef81c9f6fe44470.jpg"
new_file = folder / "Toge, Tokamachi, Niigata 942-1351, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "ec977735579517a88ee7b6a1e401d01f6448a87e4043fe5de530a1eca8bdbf5d.jpg"
new_file = folder / "Three Graces, Royal Liver Building, Port of Liverpool Building, and Cunard Building, Liverpool.jpg"
rename_file(old_file,new_file)

old_file = folder / "ff0d5590790d5e35777bdb64a33a1660aac572317c56e0abc1017bd6cf8b8401.jpg"
new_file = folder / "Champagner-Pool, Waikato 3073, Neuseeland.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220204_1/

# + hidden=true
folder_name = "20220204_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220204_1"
folder = Path(".") / folder_name

old_file = folder / "3099b135079ce604d9e6a49ffbc0d6381499e56e4172b9bbd4864810d61b47f9.jpg"
new_file = folder / "Berg Wildseeloder, Tirol, Österreich.jpg"
rename_file(old_file,new_file)

old_file = folder / "4ce05edae0003bf62e50c9e64f39709f6781bba60525e5ee058b51cbc12dc357.jpg"
new_file = folder / "Brücke - Rakotzbrücke im Rhododendronpark in Gablenz-Kromlau, Sachsen, Deutschland.jpg"
rename_file(old_file,new_file)

old_file = folder / "7c3547f0c66ca94af1f93a77f68b4a93a2de2f264951ba4c88dcef6384577235.jpg"
new_file = folder / "See Umm el Ma (Mutter des Wassers) in der Oase Awbari (Ubari), Wüste Sahara, Fezzan, Libyen.jpg"
rename_file(old_file,new_file)

old_file = folder / "8283fedb6ff838d61826f7ff3bff83fbfa8f6d46c18eccdf46585362bba2fb37.jpg"
new_file = folder / "Wasserfall - Präfektur Chiba, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "ae0ed9ef2158660f4f15134896f3b4123bb53b8643d6b62a12bfb30f46531a5c.jpg"
new_file = folder / "Machu Picchu, Ruinen der verlorenen antiken Inka-Stadt, Cusco, Peru.jpg"
rename_file(old_file,new_file)

old_file = folder / "d05d4e1145e29d12965970a94bbee9add7d21917202a779e72a5d38a12247e50.jpg"
new_file = folder / "Riverband Crnojevica, Vranjina-Hügel und Skutarisee, Montenegro.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220208_1/

# + hidden=true
folder_name = "20220208_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220208_1"
folder = Path(".") / folder_name

old_file = folder / "19d52c1968b7e5e04f045b945291411b442809df5929617138dd9229b9725571.jpg"
new_file = folder / "Nationalpark La Jacques-Cartier, Quai des Rabascas du Pont-Banc.jpg"
rename_file(old_file,new_file)

old_file = folder / "8867cae105f959d901370d260c3ad7370260c4c587154caa6afd4e0e17dc05cf.jpg"
new_file = folder / "Höhlen am Lake Superior, Munising, Michigan, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "c8e0c17bc12b547ceb53d1f784520e501e8ae82b8a60c7f57a738f210b81d25a.jpg"
new_file = folder / "Sahara, Dünen nahe Douz, Kebili, Tunesien.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220219_1/

# + hidden=true
folder_name = "20220219_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220219_1"
folder = Path(".") / folder_name

old_file = folder / "0f8b1f8528c1a4ce957aae8945b9b5c5defc536988aca1b610e040ad3cf1fac2.jpg"
new_file = folder / "Wasserfall - Bergoase Chebika, Tozeur, Tunesien.jpg"
rename_file(old_file,new_file)

old_file = folder / "2cd39e54cc21e01dd500062708f14fcad2045835f0f0a6b010d535bb99b41bc5.jpg"
new_file = folder / "Gottes Fenster im Blyde River Canyon, Mpumalanga, Südafrika.jpg"
rename_file(old_file,new_file)

old_file = folder / "67c30a33b180da21f987f55d70d78897171679454d4106951cbfde4abc478698.jpg"
new_file = folder / "Maharaja Sayajirao Universität Baroda, Fakultät der Künste. Indien.jpg"
rename_file(old_file,new_file)

old_file = folder / "96b1ad08d4cdf371c5fcf8dc48c8017550b1797905b9119b766d19674f0bfb75.jpg"
new_file = folder / "Semifonte-Kapelle und Ort Petrognano, Barberino Val d'Elsa, Toskana, Italien.jpg"
rename_file(old_file,new_file)

old_file = folder / "a86a6147c0e2429065620cedf97e8b619bdc452c83fa87a39fdb38f11f67b3fc.jpg"
new_file = folder / "Morning in Bolivia. Salar de Uyuni. Isla Incahuasi.jpg"
rename_file(old_file,new_file)

old_file = folder / "cd4a6ee06e14572dcfba70e6c0e2b0a122c8ea30bc20afe3dd91b5d0cd3c29de.jpg"
new_file = folder / "Pyramiden von Gizeh im Drohnenfoto, Kairo, Ägypten.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220223_1/

# + hidden=true
folder_name = "20220223_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220223_1"
folder = Path(".") / folder_name

# old_file = folder / "96c47efaf584419b7d4255ba627629ebc0a17fb07f529f6b253fcf5222e48b70.jpg"
# new_file = folder / "Bay - Scarborough Beach bei Sonnenuntergang, Südafrika.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "a0458a4a3e870c0bbbf89656a8e5a4bce857e7206bd38af17647ec88aa1b5f86.jpg"
# new_file = folder / "Nationalpark Borjomi Kharagauli, Georgien.jpg"
# rename_file(old_file,new_file)

old_file = folder / "dfabd215c3459636f4a46acf2f9083b1caf382bab445fe6f085734d4bd177d2d.jpg"
new_file = folder / "Nationalpark Mount Rainier, Washington, USA.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220225_1/

# + hidden=true
folder_name = "20220225_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220225_1"
folder = Path(".") / folder_name

old_file = folder / "01bbd2343fc40e19340b7895da541287568ea57d7af0235c80e52bbaf1686f18.jpg"
new_file = folder / "Pedra dos Tres Pontoes, Afonso Claudio, Espirito Santo State, Brazil.jpg"
rename_file(old_file,new_file)

old_file = folder / "553e4190c69295e069beaf22980683472198ba0df2c226a5b00eb7f31aa352d2.jpg"
new_file = folder / "Tempel des Poseidon am Kap Sounion in der Ägäis, Griechenland.jpg"
rename_file(old_file,new_file)

old_file = folder / "57989f37059745075c6af3ccf00fb3a99b8a1569642a90a814e6ce73a21c3b67.jpg"
new_file = folder / "Castillejas indivisas in Norman, Oklahoma, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "9773df6a44b8d498aedf474e289f896104300bd9b93ae21290ab1a2baadb40ef.jpg"
new_file = folder / "ESALQ, Öffentliche Landwirtschaftshochschule von oben in Piracicaba, Sao Paulo, Brasilien.jpg"
rename_file(old_file,new_file)

old_file = folder / "e542ce355cea271e2a0888bc4484fc16ae1e845fe412a37edaae2fe3b9916198.jpg"
new_file = folder / "Berg Salkantay über Tal entlang des Salkantay Trek nach Machu Picchu, Peru.jpg"
rename_file(old_file,new_file)

old_file = folder / "fe6885fa8a7d1d476c442e5031a26b090630a386c4996c62f742783e686ba65a.jpg"
new_file = folder / "Vulkan Toliman, See Atitlán, Guatemala, Mittelamerika.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220305_1/

# + hidden=true
folder_name = "20220305_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220305_1"
folder = Path(".") / folder_name

old_file = folder / "003a258b0e6ee032d340c1613728c0073ac31477dd3ddb71af269dd4389722ba.jpg"
new_file = folder / "Palmenstraße in Los Angeles, Kalifornien, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "476b505ef23873b0ace27a80ee545bafaa5c4b774864b2b7d052e2c4eed0e6f4.jpg"
new_file = folder / "Bay - Lloret de Mar, Girona, Spanien.jpg"
rename_file(old_file,new_file)

old_file = folder / "e6ffd802bd71331eeb293b4764338dbc1f53dc1a8f6177f8a12b0eeab74ab979.jpg"
new_file = folder / "Bergsee, Himalaya, Nepal.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220309_1/

# + hidden=true
folder_name = "20220309_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220309_1"
folder = Path(".") / folder_name

old_file = folder / "4795df8aa35fd72bfe9515897cdb2ec2a949fa747bebd14e5170ab1c9d5f208b.jpg"
new_file = folder / "Trevi-Brunnen zwischen der Via Poli und der Via della Stamperia, Trevi, Rom, Italien.jpg"
rename_file(old_file,new_file)

old_file = folder / "7abb99cd91337d29dc4d120a3eaf2f00feb305a94af82dd5e1f36af93835addc.jpg"
new_file = folder / "Mirissa, Matara, Southern Province, Sri Lanka.jpg"
rename_file(old_file,new_file)

old_file = folder / "87f516735a41d92f781c05ff00d58c9f46c85e2025b5c0a1a0068c4db6794a02.jpg"
new_file = folder / "Berg Fuji und Eiszapfen im Yachonomori Park, Präfektur Yamanashi, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "9cee40dd41de5642b90b8ca6bb688b713a0ce33c7215f32eac4e3f2f98bba1f5.jpg"
new_file = folder / "See Petén Itzá in El Ramate bei Sonnenuntergang, Guatemala.jpg"
rename_file(old_file,new_file)

old_file = folder / "aa08501898e7881f246682bfad1ccb362e2b3512a7f7c466b5e68db65fda390e.jpg"
new_file = folder / "Gemälde Canal Grande mit dem Campo della Carità, Venedig, Italien.jpg"
rename_file(old_file,new_file)

old_file = folder / "df2e8a3871930360adad925df3e25e36803ad135a93f5ff40050da7505a06543.jpg"
new_file = folder / "Walker Bay in Hermanus, Western Cape, Südafrika.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220313_1/

# + hidden=true
folder_name = "20220313_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220313_1"
folder = Path(".") / folder_name

# old_file = folder / "06803b3b389ff15011281266e3e3592024389b174dc23e26610be0e3e77a5bc3.jpg"
# new_file = folder / "Messner Mountain Museum, Località Monte Rite, Cibiana di Cadore (BL), Italia.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "0cda7e8d074bd7e3f9b2a323f42c6d25f6057385c80d48117c445c3e043b7f5b.jpg"
# new_file = folder / "Cana Island am Lake Michigan im Winter, Door County, Wisconsin, USA.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "1a810f7ee0ec4e465e1262f7b9c5b2feed8ea596863ed4cb8630ba0c524bd8b1.jpg"
# new_file = folder / "Resurrection Bay, Kenai Peninsula Borough, Yunan Alaska, USA.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "4795df8aa35fd72bfe9515897cdb2ec2a949fa747bebd14e5170ab1c9d5f208b.jpg"
# new_file = folder / "Trevi-Brunnen zwischen der Via Poli und der Via della Stamperia, Trevi, Rom, Italien.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "907aa61608b8067d95c823438c95f7d374a9c21389a87ef50c0534664d5ac224.jpg"
# new_file = folder / "Nationalforst Wǔlíngyuán mit Sonne, Zhāngjiājiè, Hunan, China.jpg"
# rename_file(old_file,new_file)

old_file = folder / "Kolosseum, Piazza del Colosseo 1, 00184 Roma RM, Italien.jpg"
new_file = folder / "Kolosseum, Piazza del Colosseo 1, Rom, Italien.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220320_1/

# + hidden=true
folder_name = "20220320_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220320_1"
folder = Path(".") / folder_name

old_file = folder / "2d8fb4e8f96f818445ae854e03441baa5ddb483f8670a110552ff4b98ef7293b.jpg"
new_file = folder / "Reine, Lofoten, Norwegen.jpg"
rename_file(old_file,new_file)

old_file = folder / "5c935167b6b598b6895dc6a381722d2d5721082a467d02253c3e42bc4288776f.jpg"
new_file = folder / "Sigiriya, Dambulla, Central Province, Sri Lanka.jpg"
rename_file(old_file,new_file)

old_file = folder / "7241495a4622193a61088f80db55329fcd379b75adf87bbcfb588aa2e4cc88af.jpg"
new_file = folder / "Fluss Gatesgarthdale Beck am Honister Pass, Lake District, Cumbria, England, UK.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220322_1/

# + hidden=true
folder_name = "20220322_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220322_1"
folder = Path(".") / folder_name

old_file = folder / "0e53671ab7c7003b5fe294ef8bcca8bd3de60bf2417d973671f9b97300d6aefe.jpg"
new_file = folder / "Glendurgan Garten, Cornwall, England, Vereinigtes Königreich.jpg"
rename_file(old_file,new_file)

old_file = folder / "26ba86ac7fc2e6986b583f7ca8d9b2309ce889d940cb159f6183e36da4becb3e.jpg"
new_file = folder / "Las Vegas, Nevada, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "3075826873a8a22ad529fb960bc9f7b6b085a6339551d3da3200b84f7d470e08.jpg"
new_file = folder / "Bellagio - Perle des Comer Sees, Italien.jpg"
rename_file(old_file,new_file)

old_file = folder / "4ce05edae0003bf62e50c9e64f39709f6781bba60525e5ee058b51cbc12dc357.jpg"
new_file = folder / "Brücke - Rakotzbrücke im Rhododendronpark in Gablenz-Kromlau, Sachsen, Deutschland.jpg"
rename_file(old_file,new_file)

old_file = folder / "783eb6c6adfa7c216fa61bffd13d03b5d3b0436b2dc05fbabd745b672746ad02.jpg"
new_file = folder / "Geiranger, Norway.jpg"
rename_file(old_file,new_file)

old_file = folder / "978eeaec931141614069921879fe80e7f9622b512614f1dcc5df3cef38673e7d.jpg"
new_file = folder / "Schloss Bran, Nationalpark Piatra Craiului, Ciocanu, Rumänien.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220326_1/

# + hidden=true
folder_name = "20220326_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220326_1"
folder = Path(".") / folder_name

old_file = folder / "5e30e1aaf94465039ab1843ea1fbe9fb15b9230f4b3796c9deeed2cad6a9653a.jpg"
new_file = folder / "Adivino-Pyramide des Wahrsagers, Uxmal, Yucatán, México.jpg"
rename_file(old_file,new_file)

old_file = folder / "86db543eee5587d785b09ebad71d4f3553b62f93c70cc7bd705cdcf7f608ff92.jpg"
new_file = folder / "Lí-Fluss (漓江 Lí Jiāng) und Karst Berge nahe der antiken Stadt 兴坪 Xìngpíng, 阳朔 Yángshuò, 桂林 Guìlín, 廣西 Guǎngxī, China.jpg"
rename_file(old_file,new_file)

old_file = folder / "e9e0e1c64c91871cebc0bc202e72165577a7cb2ef6fff6a20ccd6c2897dbe478.jpg"
new_file = folder / "Echo Parksee und die Skyline von Los Angeles.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220328_1/

# + hidden=true
folder_name = "20220328_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220328_1"
folder = Path(".") / folder_name

old_file = folder / "6ebcf25540bc0981cc6feb855acca0ef89305f7d9a66ed1042de5ad9e3825c78.jpg"
new_file = folder / "Cap Blanc-Nez, Escalles, Nord-Pas-de-Calais, Frankreich.jpg"
rename_file(old_file,new_file)

old_file = folder / "7cd55949d47dc344c096f437fd33970bc658d5b63d937e9d937cf8adb99bf9c1.jpg"
new_file = folder / "Granite Island, Encounter Bay, South Australia, Australien.jpg"
rename_file(old_file,new_file)

old_file = folder / "e0c9bdba2087a826a465f7fe39edb6366fb0252b1304ae875009dc61c98f2a4b.jpg"
new_file = folder / "Rotes Haus über Sognefjord, Norwegen.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220330_1/

# + hidden=true
folder_name = "20220330_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220330_1"
folder = Path(".") / folder_name

old_file = folder / "2d8fb4e8f96f818445ae854e03441baa5ddb483f8670a110552ff4b98ef7293b.jpg"
new_file = folder / "Reine, Lofoten, Norwegen.jpg"
rename_file(old_file,new_file)

old_file = folder / "75eae37de95f53598a6d3e897a793eb9e3bdf63ce3af5bdd5d98580f3caee0a3.jpg"
new_file = folder / "Nationalpark Sehlabathebe, Ostlesotho.jpg"
rename_file(old_file,new_file)

old_file = folder / "7fde0b52997d40da43965e41efa104d16ce499573748cb4efe1be2e0b63b8eae.jpg"
new_file = folder / "Ilulissat-Eisfjord (Ilulissat Kangerlua) an der Diskobucht, Grönland.jpg"
rename_file(old_file,new_file)

old_file = folder / "9cc8daa8e6f98cf36b1acb3e13dfce0190de70f859ee54fc9b51e8b29d1d7470.jpg"
new_file = folder / "Nationalpark Big-Bend, Texas, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "b03ad6f62837de6b3f6096d4d9a0f114a40e9683eef976e3bea1fdcbe4cf13c7.jpg"
new_file = folder / "Lí-Fluss (漓江 Lí Jiāng) mit Karstbergen, Guǎngxī 广西, China.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220405_1/

# + hidden=true
folder_name = "20220405_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220405_1"
folder = Path(".") / folder_name

old_file = folder / "009d5eed55a0e21f305533f742a8b8b1824622e8804e91902ba0d2f13c6dc633.jpg"
new_file = folder / "Nationalpark Banff, Moraine Lake mit Kanus, Alberta, Kanada.jpg"
rename_file(old_file,new_file)

old_file = folder / "60b8d703fad24d59b56bb85c30fb5a8b9c938626ce049b24cb188f53214bca8d.jpg"
new_file = folder / "Nationalpark Voyageurs, Ash River Visitor Center, Minnesota, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "7c19709d159a9f5fb768f4b489efdb496c9238021c017dec3386004b5eba6c56.jpg"
new_file = folder / "naturpark sintra cascais azeNaturpark Sintra-Cascais, Azenhas Do Mar, Costa de Lisboa, Portugal.jpg"
rename_file(old_file,new_file)

old_file = folder / "c1eea3db0741f1d83a08c08a4fb52e2af3be616b82cd705e0a58f91ff9104ac0.jpg"
new_file = folder / "Bay - Horseshoe Bay Beach, Southhampton Parish, Bermuda.jpg"
rename_file(old_file,new_file)

old_file = folder / "c7b92a9b31fe16ecf9c63c5194430a10ef40f536eaa1a245e9e4466213ff5fea.jpg"
new_file = folder / "Vestrahorn Beach, Batman Mountain, Island.jpg"
rename_file(old_file,new_file)

old_file = folder / "df0f784e155248b805063c00ad4c0289bfd829a6bcedeeb2646c951e7c4ca9c3.jpg"
new_file = folder / "Hana, Maui, Hawaii, USA.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220407_1/

# + hidden=true
folder_name = "20220407_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220407_1"
folder = Path(".") / folder_name

old_file = folder / "10452b84469d1ee9524c4af4af705fa15caf1e7ca76ad98c0f42b0561ef58ad5.jpg"
new_file = folder / "Leuchtturm Farol da Barra in der Forte de Santo Antonio da Barra, UNESCO-Weltkulturerbe, Salvador, Bahia, Brasilien.jpg"
rename_file(old_file,new_file)

old_file = folder / "45e010735d5dff527a5d5f18914d6233762cc3796585a96d0219977d14c087f7.jpg"
new_file = folder / "Ounianga Kebir, Sahara, Tschad.jpg"
rename_file(old_file,new_file)

old_file = folder / "91c25cbdab597876f8556003fe7e3ff4295203be72ebcb50bec44638f304bcf5.jpg"
new_file = folder / "Warebeth Strand, Orkney-Inseln, Schottland, Großbritannien.jpg"
rename_file(old_file,new_file)

old_file = folder / "b02c4d5cf33455e626fb0bb26a2ad3f7d816badd3cc7aa2afa8a572f1049c5ec.jpg"
new_file = folder / "Wasserfall - Celeste-Fluss im Tenorio-Nationalpark, Costa Rica.jpg"
rename_file(old_file,new_file)

old_file = folder / "b4e2044926a88304da0221e66ae919c015684a901b55852be8c065a366df1136.jpg"
new_file = folder / "Tauchgondel Zingst.jpg"
rename_file(old_file,new_file)

old_file = folder / "db87f474f06f2d4eee68ee2dbbc50d3835e16553a5aa0a2a5ec6969db38fc824.jpg"
new_file = folder / "Aiguilles Rouges bei Chamonix, Haute Savoie, Frankreich.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220410_1/

# + hidden=true
folder_name = "20220410_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220410_1"
folder = Path(".") / folder_name

old_file = folder / "Berg Huáng Shān 黃山, Ānhuī 安徽, China.jpg"
new_file = folder / "Vulkan Toliman, See Atitlán, Guatemala, Mittelamerika.jpg"
rename_file(old_file,new_file)

old_file = folder / "TTTTT.jpg"
new_file = folder / "Berg Huáng Shān 黃山, Ānhuī 安徽, China.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220414_1/

# + hidden=true
folder_name = "20220414_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220414_1"
folder = Path(".") / folder_name

# old_file = folder / "06803b3b389ff15011281266e3e3592024389b174dc23e26610be0e3e77a5bc3.jpg"
# new_file = folder / "Messner Mountain Museum, Località Monte Rite, Cibiana di Cadore (BL), Italia.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "37a5dc662a21e2e4f39235304b188e73bc2e198a60d3cbaa712dc378bc941121.jpg"
# new_file = folder / "Claiborne Pell Newport Bridge view with rocky seascape at sunrise, Jamestown, Rhode Island, USA.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "7d43f1ede76e6b02d7c0a388e951c677a2a28d0635fc8d8bb68d9b6f46bbf9f7.jpg"
# new_file = folder / "Red Rock Canyon National Conservation Area, Nevada, USA.jpg"
# rename_file(old_file,new_file)

old_file = folder / "Claiborne Pell Newport Bridge view with rocky seascape at sunrise, Jamestown, Rhode Island, USA.jpg"
new_file = folder / "Brücke - Claiborne Pell Newport Bridge view with rocky seascape at sunrise, Jamestown, Rhode Island, USA.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220419_1/

# + hidden=true
folder_name = "20220419_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220419_1"
folder = Path(".") / folder_name

old_file = folder / "26ba86ac7fc2e6986b583f7ca8d9b2309ce889d940cb159f6183e36da4becb3e.jpg"
new_file = folder / "Las Vegas, Nevada, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "436f5c45947ff8c3f932bbb0ee262ec49d47fe9d2cb5de40cb4df042cdee6ca3.jpg"
new_file = folder / "Sanipass, Drakensberge, Südafrika.jpg"
rename_file(old_file,new_file)

old_file = folder / "60b8d703fad24d59b56bb85c30fb5a8b9c938626ce049b24cb188f53214bca8d.jpg"
new_file = folder / "Nationalpark Voyageurs mit Polarlichters, Ash River Visitor Center, Minnesota, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "9e02f2ca3fd46bae0bdb74e07237864d37ec6191538af955cdf1adec6fa899a2.jpg"
new_file = folder / "Wasserfall - Victoriafälle.jpg"
rename_file(old_file,new_file)

old_file = folder / "aab37e73041d6c597e769472059550be53961bbdbf889a12cdf454a186aedd67.jpg"
new_file = folder / "Bonsai Rock, Lake Tahoe, Nevada, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "d3eedb83164482c35b9bf5057a67514a6d30ccc1c43cadacc08c0526ac994779.jpg"
new_file = folder / "Nationalpark Canaima mit Tafelbergen und Carrao-Fluss bei Ucaima, Venezuela.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220422_1/

# + hidden=true
folder_name = "20220422_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220422_1"
folder = Path(".") / folder_name

old_file = folder / "15b8a714d49c6315f87356cc8275654f989246672131ab942ff6ed62fc8003e4.jpg"
new_file = folder / "Nationalpark La-Maddalena-Archipel, Budelli, Sardinien, Italien.jpg"
rename_file(old_file,new_file)

old_file = folder / "60b8d703fad24d59b56bb85c30fb5a8b9c938626ce049b24cb188f53214bca8d.jpg"
new_file = folder / "Nationalpark Voyageurs mit Polarlichters, Ash River Visitor Center, Minnesota, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "e1eb6c54db7691f71a594d7aeea5051ed3225a1933b1a7b83999c9f14422adeb.jpg"
new_file = folder / "Kanincheneulen, Sublette County, Wyoming, USA.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220423_1/

# + hidden=true
folder_name = "20220423_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220423_1"
folder = Path(".") / folder_name

old_file = folder / "15b8a714d49c6315f87356cc8275654f989246672131ab942ff6ed62fc8003e4.jpg"
new_file = folder / "Nationalpark La-Maddalena-Archipel, Budelli, Sardinien, Italien.jpg"
rename_file(old_file,new_file)

old_file = folder / "4d1d833aad4e8f7c51a1a2041d41d59a8fba9b7cb2f825df48b5b944670a9852.jpg"
new_file = folder / "Goldstrand (Slatni Pjasazi), Bulgarien.jpg"
rename_file(old_file,new_file)

old_file = folder / "60b8d703fad24d59b56bb85c30fb5a8b9c938626ce049b24cb188f53214bca8d.jpg"
new_file = folder / "Nationalpark Voyageurs mit Polarlichters, Ash River Visitor Center, Minnesota, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "a7909d29c491c9b53a238d672aa8796da835d55d0813b2ae3a1ab1421ddd780c.jpg"
new_file = folder / "Blaue Stunde, Brücken, Moldau, Prag, Tschechien.jpg"
rename_file(old_file,new_file)

old_file = folder / "e1eb6c54db7691f71a594d7aeea5051ed3225a1933b1a7b83999c9f14422adeb.jpg"
new_file = folder / "Kanincheneulen, Sublette County, Wyoming, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "f3de784698c40b7e1211205816e521bf637defbba3c40b5e57e351174839ad0d.jpg"
new_file = folder / "Hitachi-Küstenpar, Nemophila-Blumen, Hitachinaka, Präfektur Ibaraki, Japan.jpg"
rename_file(old_file,new_file)

# + hidden=true
from pathlib import Path

folder_name = "20220423_1"
folder = Path(".") / folder_name

old_file = folder / "Hitachi-Küstenpar, Nemophila-Blumen, Hitachinaka, Präfektur Ibaraki, Japan.jpg"
new_file = folder / "Hitachi-Küstenpark, Nemophila-Blumen, Hitachinaka, Präfektur Ibaraki, Japan.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220424_1/

# + hidden=true
folder_name = "20220424_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220424_1"
folder = Path(".") / folder_name

old_file = folder / "3c3d5c711a4ab4b64377f018e9cae292324d7123598ca2e83098bda2f3937aea.jpg"
new_file = folder / "Nationalpark Zhāngyè (张掖) Geopark mit Dānxiá -Landschaften (丹霞), Gānsù (甘肃), China.jpg"
rename_file(old_file,new_file)

old_file = folder / "4d1d833aad4e8f7c51a1a2041d41d59a8fba9b7cb2f825df48b5b944670a9852.jpg"
new_file = folder / "Goldstrand (Slatni Pjasazi), Bulgarien.jpg"
rename_file(old_file,new_file)

old_file = folder / "8ca90ab686003c2c8d43020ebea309d60dd333bbc2f28cc3f0eff9221e580d6b.jpg"
new_file = folder / "Bay - Playa del Silencio, Cudillero, Asturien, Spanien.jpg"
rename_file(old_file,new_file)

old_file = folder / "9925418eb05335c1bcb75729faffe1be92f46bdbd33c9a3cbf63b12d6f630fdd.jpg"
new_file = folder / "Bay - Cabo de la Vela, Halbinsel Guajira, La Guajira, Riohacha, Kolumbien.jpg"
rename_file(old_file,new_file)

old_file = folder / "a7909d29c491c9b53a238d672aa8796da835d55d0813b2ae3a1ab1421ddd780c.jpg"
new_file = folder / "Blaue Stunde, Brücken, Moldau, Prag, Tschechien.jpg"
rename_file(old_file,new_file)

old_file = folder / "f3de784698c40b7e1211205816e521bf637defbba3c40b5e57e351174839ad0d.jpg"
new_file = folder / "Hitachi-Küstenpar, Nemophila-Blumen, Hitachinaka, Präfektur Ibaraki, Japan.jpg"
rename_file(old_file,new_file)

# + hidden=true
from pathlib import Path

folder_name = "20220423_1"
folder = Path(".") / folder_name

old_file = folder / "Hitachi-Küstenpar, Nemophila-Blumen, Hitachinaka, Präfektur Ibaraki, Japan.jpg"
new_file = folder / "Hitachi-Küstenpark, Nemophila-Blumen, Hitachinaka, Präfektur Ibaraki, Japan.jpg"
rename_file(old_file,new_file)

# + [markdown] heading_collapsed=true
# #### Show Wallpapers in 20220426_1/

# + hidden=true
folder_name = "20220426_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# + hidden=true
from pathlib import Path

folder_name = "20220426_1"
folder = Path(".") / folder_name

old_file = folder / "00eaa39532c263e12111cfa8ca334140afc9d5893f3da63443fbbabbabbd532f.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "097b0c6a161b2fdb42bbfbae7c0bf62936752cc0f501ab051e566e63fb078e8f.jpg"
new_file = folder / "Nationalpark Plitvicer Seen 4, Krotien.jpg"
rename_file(old_file,new_file)

old_file = folder / "10452b84469d1ee9524c4af4af705fa15caf1e7ca76ad98c0f42b0561ef58ad5.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "1a9932db03aea52ac08cf20acb418d833508e4c714475c632e1391f84a2e8143.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "c6f2f450575eb147e82b6e622daee2e7c0bc8035b44eaeecab84a7764d4aa544.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)
# -

# #### Show Wallpapers in 20220501_1/

folder_name = "20220501_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# +
from pathlib import Path

folder_name = "20220501_1"
folder = Path(".") / folder_name

old_file = folder / "10452b84469d1ee9524c4af4af705fa15caf1e7ca76ad98c0f42b0561ef58ad5.jpg"
new_file = folder / "Leuchtturm Farol da Barra in der Forte de Santo Antonio da Barra, UNESCO-Weltkulturerbe, Salvador, Bahia, Brasilien.jpg"
rename_file(old_file,new_file)

old_file = folder / "45a1cdade789a5162de9527a4eec2bb9f51a82f4b51f73cda21b28c4e3e5d019.jpg"
new_file = folder / "Bay - Scarborough Beach bei Sonnenuntergang, Südafrika.jpg"
rename_file(old_file,new_file)

# old_file = folder / "52d30ac6a10858d893b1eb96a046a0e8cbdaa979fffa20cbbc1ffd3e11d7a238.jpg"
# new_file = folder / "TTTTT.jpg"
# rename_file(old_file,new_file)

old_file = folder / "55ce5c24329914cff572756155402cc09c5f8d5e451b8ebfbcf16a964f41c891.jpg"
new_file = folder / "El Nido, Insel Matinloc, Palawan, Philippinen.jpg"
rename_file(old_file,new_file)

old_file = folder / "81713eeb901c71dff6836ed1076ba01e9aad6470cb1b42435fa1e27c966cf9f1.jpg"
new_file = folder / "Bay - Ha Long Bucht mit Kalksteinsäulen, Provinz Quang Ninh, Vietnam.jpg"
rename_file(old_file,new_file)

old_file = folder / "d44afcaaf387816c4c2e3c955301b184fb70f73a17a6941bb7cea325210efd95.jpg"
new_file = folder / "Megalithen auf der Argimusco-Hochebene, Montalbano Elicona, Sizilien, Italien.jpg"
rename_file(old_file,new_file)
# -

# #### Show Wallpapers in 20220508_1/

folder_name = "20220508_1"
file_name_list = reduce_to_wallpapers(folder_name,True)

# +
from pathlib import Path

folder_name = "20220508_1"
folder = Path(".") / folder_name

old_file = folder / "03524bd7ed240954228c0bb4d55046c3861a758b5bd74a2a7e738e6181d04312.jpg"
new_file = folder / "Gemälde von Thomas Cole, Blick auf die Catskill Mountains im Bundesstaat New York im Frühherbst - 1837, Hudson River School.jpg"
rename_file(old_file,new_file)

old_file = folder / "4133576b5256cbd8d1e3c801445f0fc7dae14e621eece9f9c4521349dc3e614e.jpg"
new_file = folder / "Berg Zaō, Präfektur Yamagata, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "4e0cc388b882a2cef84c760282e6c4f5db02eaea8716e5bcf9a2d464ddf86662.jpg"
new_file = folder / "Reine, Lofoten, Norwegen.jpg"
rename_file(old_file,new_file)

old_file = folder / "7ada986b8aaa8d7099b8be947eb8f2f4fd21c047451f5dbf5a2e54775c30003b.jpg"
new_file = folder / "Allrad-Fahrzeuge auf unbefestigter Straße im Dschungel bei Korhogo, Elfenbeinküste.jpg"
rename_file(old_file,new_file)

old_file = folder / "8283fedb6ff838d61826f7ff3bff83fbfa8f6d46c18eccdf46585362bba2fb37.jpg"
new_file = folder / "Wasserfall - Präfektur Chiba, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "a5edc58b00c3c37eb92b2eb66446e22593efa83e1c275c0e0de3ba4148f287f8.jpg"
new_file = folder / "Hitachi Seaside Park mit kochias, Japan.jpg"
rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
# -

# ## Final Destination Collection/

# ### Codebase 3-1 (Merge)

# +
from pathlib import Path
import shutil

def copy_wallpapers(source,target):
    '''Copy all files contained in list `source` to `target`
    
    Parameters:
    source   (list of strings)    list of relative path names of files to be copied
    target   (Path)               relative path to folder to copy files to
    '''
    for path in [Path(name) for name in source]:
        if path.is_file():
            if not((target / path.name).is_file()):
                shutil.copy(path, target)
                print(f'[ OK ] Wallpaper "{str(target / path.name)}" collected')
            else:
                print(f'[FAIL] Wallpaper "{str(target / path.name)}" already exists')



# -

# ### Merge Intermediate Collection

# The function `copy_wallpapers(source,target)` is to be applied to `file_name_list` so let's check if this list has been properly composed.
# * Otherwiese, run `display_and_prepare_renaming_wallpapers()` on the intermediate collection under consideration above.

file_name_list

copy_wallpapers(file_name_list, Path(".") / "Collection")

# ### Show Wallpapers in Collection/

# display_and_prepare_renaming_wallpapers("Collection")
reduce_to_wallpapers("Collection",False)

remove_non_wallpapers("Collection")

# +
from pathlib import Path

folder = Path(".") / "Collection"

old_file = folder / "Claiborne Pell Newport Bridge view with rocky seascape at sunrise, Jamestown, Rhode Island, USA.jpg"
new_file = folder / "Brücke - Claiborne Pell Newport Bridge view with rocky seascape at sunrise, Jamestown, Rhode Island, USA.jpg"
rename_file(old_file,new_file)



# +
from pathlib import Path

folder = Path(".") / "Collection"

path = folder / "Robberg-Halbinsel, Südafrika.jpg"
path.unlink()

# -

# ## TODO: Find Locations Parsing Results of Google Reverse Image Search

# !pip install googlesearch-python

from googlesearch import search
####
# googlesearch.search(str: term, int: num_results=10, str: lang="en") -> list
#   rf. https://pypi.org/project/googlesearch-python/
#   and https://github.com/Nv7-GitHub/googlesearch
####
search("Google") # Returns the list of first page results of searching the keyword "Google" with Google

# List results of the Google search specified by the link given by variable `url`:

# +
from bs4 import BeautifulSoup
from requests import get

def location_of_image(url):
    '''
    Get the results of the Google Reverse Image Search given by `url` and 
    try to determine where the photo was taken
    '''
    usr_agent = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      # 'Chrome/61.0.3163.100 Safari/537.36'
    }
    def fetch_results(search_url):
        response = get(search_url, headers=usr_agent)
        response.raise_for_status()
        return response.text

    def parse_results(raw_html):
        soup = BeautifulSoup(raw_html, 'html.parser')
        result_block = soup.find_all('div', attrs={'class': 'g'})
        # FIXME: len(result_block)=0
        print(f'{len(result_block)=}')
        for result in result_block:
            link = result.find('a', href=True)
            title = result.find('h3')
            text = link.get_text()
            print("Extracted {}".format(text))
            if link and title:
                yield text[:text.index("https://")] # + " -> " + link['href']
    
    # return("location_of_image({})".format(url))
    
    html = fetch_results(url)
    results = list(parse_results(html))
    # results = ["links","from","Google"]
    results = ''.join([ "<li>{}</li>".format(elem) for elem in results ])
    return ''.join(['<ol>',results,'</ol>'])


# to search
url = "http://www.google.com/search?tbs=sbi:AMhZZiur1dWe7P_1e3XOQOu6LqpKWxDu7IITdc4LKlgmF2bbOpB4L-WVRz1nGcel_1s8n7U7_18qUWsGgHXvjUj_15wctPXJzR2v1D35RaofRc8GIzawOL2HuMcSL2mydMCtQzsnmMI0ZyMdne-QSIPrcc8AWqh8rzIX2THdKQTV-_1PEYsjUh66HOIj3odw4DZVIS4YoFImy_1DxNCJ1we26UFZDAkRS-4UMQLMMjPGKJ8VgHjfnpbRkJzRPjnSbv3QWwsEymnfAe3faR4kw91j2h6K2ZhspXqXVlnth9Ki-daDeeHm5WVIv_1Goj2o-lBCrWCg2eufMOmE2pM6BEK3Yyb7IvC05nKNDiRww"
print(f'{url=}')

location_of_image(url)

# +
from bs4 import BeautifulSoup
from requests import get

def search_url(passed_url):
    usr_agent = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      # 'Chrome/61.0.3163.100 Safari/537.36'
    }
    def fetch_results(raw_url):
        # escaped_search_term = search_term.replace(' ', '+')
        # google_url = 'https://www.google.com/search?q={}&num={}&hl={}'.format(escaped_search_term, number_results+1,language_code)
        
        print(fetch_results.__name__+": raw_url='%s'" % raw_url)
        response = get(raw_url, headers=usr_agent)    
        response.raise_for_status()

        # print(fetch_results.__name__+": response.text='%s'" % response.text)
        return response.text
    def parse_results(raw_html):
        soup = BeautifulSoup(raw_html, 'html.parser')
        result_block = soup.find_all('div', attrs={'class': 'g'})
        show_enter_loop = True
        for result in result_block:
            link = result.find('a', href=True)
            title = result.find('h3')
            text = link.get_text()
            print("Extracted {}".format(text))
            if link and title:
                # yield text[:text.index("https://")] + " -> " + link['href']
                yield text[:text.index("https://")]

    html = fetch_results(passed_url)
    return list(parse_results(html))

# to search
url = "http://www.google.com/search?tbs=sbi:AMhZZiur1dWe7P_1e3XOQOu6LqpKWxDu7IITdc4LKlgmF2bbOpB4L-WVRz1nGcel_1s8n7U7_18qUWsGgHXvjUj_15wctPXJzR2v1D35RaofRc8GIzawOL2HuMcSL2mydMCtQzsnmMI0ZyMdne-QSIPrcc8AWqh8rzIX2THdKQTV-_1PEYsjUh66HOIj3odw4DZVIS4YoFImy_1DxNCJ1we26UFZDAkRS-4UMQLMMjPGKJ8VgHjfnpbRkJzRPjnSbv3QWwsEymnfAe3faR4kw91j2h6K2ZhspXqXVlnth9Ki-daDeeHm5WVIv_1Goj2o-lBCrWCg2eufMOmE2pM6BEK3Yyb7IvC05nKNDiRww"
print(url)

for j in search_url(url):
    print(j)
# -

# List results of searching the string in variable `query` with Google:

# +
from bs4 import BeautifulSoup
from requests import get

def search(term, num_results=10, lang="en", proxy=None):
    usr_agent = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/61.0.3163.100 Safari/537.36'}

    def fetch_results(search_term, number_results, language_code):
        escaped_search_term = search_term.replace(' ', '+')

        google_url = 'https://www.google.com/search?q={}&num={}&hl={}'.format(escaped_search_term, number_results+1,
                                                                              language_code)
        proxies = None
        if proxy:
            if proxy[:5]=="https":
                proxies = {"https":proxy} 
            else:
                proxies = {"http":proxy}
        
        response = get(google_url, headers=usr_agent, proxies=proxies)    
        response.raise_for_status()

        return response.text

    def parse_results(raw_html):
        soup = BeautifulSoup(raw_html, 'html.parser')
        result_block = soup.find_all('div', attrs={'class': 'g'})
        for result in result_block:
            link = result.find('a', href=True)
            title = result.find('h3')
            text = link.get_text()
            if link and title:
                yield text[:text.index("https://")] + " -> " + link['href']

    html = fetch_results(term, num_results, lang)
    return list(parse_results(html))

# to search
query = "Geeksforgeeks"

for j in search(query, num_results=10):
    print(j)

# +
try:
    from googlesearch import search
except ImportError:
    print("No module named 'google' found")
 
# to search
query = "Geeksforgeeks"
# query = "A computer science portal"

# googlesearch.search(str: term, int: num_results=10, str: lang="en") -> list

for j in search(query, num_results=10):
    print(j)

# +
from googlesearch import search
import urllib
from bs4 import BeautifulSoup

def google_scrape(url):
    thepage = urllib.urlopen(url)
    soup = BeautifulSoup(thepage, "html.parser")
    return soup.title.text

i = 1
query = 'search this'
for url in search(query, stop=10):
    a = google_scrape(url)
    print(str(i) + ". " + a)
    print(url)
    print(" ")
    i += 1

# -

import urllib
import json as m_json
query = input ( 'Query: ' )
query = urllib.parse.urlencode ( { 'q' : query } )
response = urllib.request.urlopen ( 'https://www.google.com/search?&q=Forest' + query ).read()
json = m_json.loads ( response )
results = json [ 'responseData' ] [ 'results' ]
for result in results:
    title = result['title']
    url = result['url']   # was URL in the original and that threw a name error exception
    print ( title + '; ' + url )
