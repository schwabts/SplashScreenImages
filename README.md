---
jupyter:
  jupytext:
    encoding: '# -*- coding: utf-8 -*-'
    formats: ipynb,py,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.6
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Splash Screen Images


## Class Diagrams

```python
!pip install --q pylint --user
```

[pyreverse (1) - Linux Man Pages - SysTutorials](https://www.systutorials.com/docs/linux/man/1-pyreverse/)

```python
!pip install pydot
```

```python
!pyreverse -o dot -pdatetime datetime
```

```python
import pydot

print("Creating graph ...")
(graph,) = pydot.graph_from_dot_file('classes_datetime.dot')
print("Creating PNG ...")
graph.write_png('classes_datetime.png')
print("done")
```

<img src="classes_datetime.png" />


## Create New Collection Collecting Wallpapers From System


### UML Class Diagrams

```python
!pyreverse -o dot -pSplashScreenCollector SplashScreenCollector
```

```python
import pydot

print("Creating graph ...")
(graph,) = pydot.graph_from_dot_file('classes_SplashScreenCollector.dot')
print("Creating PNG ...")
graph.write_png('classes_SplashScreenCollector.png')
print("done")
```

## Show Wallpapers Already Collected


### Codebase 2-1 (Show)

```python
from datetime import date, timedelta
from pathlib import Path
import shutil
import filecmp
import re

from IPython.display import display, HTML, Image
import pandas as pd
from PIL import Image
from bs4 import BeautifulSoup
import requests 

# TODO: The Windows System folder with wallpapers is also to be handled
#       by SplashScreenCollection as well as the final folder Collection/
class SplashScreenCollection():
    ''' Manage a collection of Splash Screen Images in a folder by allowing for removal
        of non wallpaper files, clickable wallpaper display by HTML strings in boxes'
        results pointing the Google Reverse Image Search results, and creating commands
        for giving the wallpaper files meaningful names instead of Windows' hash keys.
    '''
    def __init__(self,path_list=None):
        ''' Set attribute  `wallpaper_path_list` to the path where wallpapers are stored
            and `src_file_list` to the list of names of all files in that folder
            
            Parameters:
            path_list                   list of path names of wallpapers to initialise the collection with
        '''
        self.wallpaper_path_list = path_list
    
    def set_path_name_timestamp(self):
        ''' Set attribute `folder_name` to the a folder with the name "YYYYMMDD_i" in the current directory
            which does not yet exist using the first possible natural number i for this.

            Return Path() object of the path to this folder.
            
            Return:
            result of self.set_path_name(), i.e.
            False                       because path_name is created such that it does not exist yet
                                        but a folder with this name is created anyway
            
            Note:
            For Windows batch a solution of finding unique names was given in
            [How do I increment a folder name using Windows batch?](https://stackoverflow.com/questions/13328421/how-do-i-increment-a-folder-name-using-windows-batch).
        '''
        self.today = date.today()
        self.month = self.today.strftime("%B")
        prefix = self.today.strftime("%Y%m%d") + "_"
        # count entries of current folder starting with "YYYYMMDD_" representing today
        list_prefixes_in_current_folder = list(Path(".").glob(prefix+"*"))  # Path().glob is iterable but no list
        running_index = len(list_prefixes_in_current_folder)
        # increment suffix for getting a new name
        running_index+=1
        return(self.set_path_name("{}{}".format(prefix,running_index)),False)
    
    def set_path_name(self,path_name,missing_ok=False):
        ''' Set attribute `path_name` to the passed parameter and the attribute `path` to a
            Path() object of the path to a folder with that name in the current directory.
            
            Parameters:
            path_name                   name of folder (usually matching "YYYYMMDD_i") to search wallpapers in
            missing_ok                  allow for setting pathes that do not exist, default: False,
                                            i.e. create a folder with name `path_name` unless it exists
            
            Return:
            True                        if such a folder already exists in current directory,
                                        otherwise False after creating the folder
        '''
        self.path_name = path_name
        self.path = Path(".") / self.path_name
        if self.path.is_dir():
            print(f'{self.set_path_name.__name__}: Set to collection {self.path_name=}')
            self.path_exists = True
        else:
            print(f'{self.set_path_name.__name__}: Collection {self.path_name=} not found!')
            self.path_exists = False
        if self.path_exists:
            self.set_wallpaper_path_list(False)
        else:
            if not missing_ok:
                self.create_path()
        return(self.path_exists)

    def create_path(self):
        ''' Create the (local) folder `self.path` the object self representing
            
            Return:
            False                       if a non-empty folder whose path is `self.path` exists
        '''
        if self.path.exists():
            # local target folder exists
            if any(self.path.iterdir()):
                # local folder path is not empty
                self.path_empty = False
                print(f"[FAIL] {self.path=} exists and is not empty")
                return(False)
            else:
                # local target folder is empty
                self.path_empty = True
                print(f"[ OK ] {self.path=} exists but is empty")
            return(True)
        else:
            print(f"[ OK ] {self.path=} is missing")
            print(f'creating {self.path}/')
            self.path.mkdir(parents=False, exist_ok=False)
            self.path_exists = True
            self.path_empty = True
            return(True)
    
    def from_system(self,username):
        ''' Create new collection of wallpapers currently stored in the systems folder
            
            Parameters:
            None
        '''
        self.copy_files_from_system(username)
        self.append_extension("jpg")
        self.set_wallpaper_path_list(True)
        self.reduce()
        self.rename_collected_wallpapers()
        print(self.print_subsection_for_folder())
    
    def copy_files_from_system(self,username):
        ''' Copy files from source to target

            No longer needed Parameters:
            username    (string)        Name of user in whose windows proile to look for wallpapers
        '''
        from_path = Path("/") / "Users" / username / \
            "AppData" / "Local" / "Packages" / \
            "Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy" / \
            "LocalState" / "Assets"
        print(f"copying from {str(from_path)=}")
        for p in from_path.iterdir():
            if p.is_file():
                if shutil.copy(p, self.path):
                    print(f"[ OK ] copied {str(p)=} to {str(self.path)}/")
                else:
                    print(f"[FAIL] unable to copy {str(p)=}")

    def append_extension(self,extension):
        ''' Add the string `extension` to the names of all files in folder `self.path`

            Parameters:
            extension   (string)        extension to add to all files in the collection
        '''
        print(f'exec append_extension("{extension}") ...')
        for p in self.path.iterdir():
            if p.is_file():
                new_name = p.stem + "." + extension
                if p.rename(Path(p.parent) / new_name):
                    print(f"[ OK ] {p.stem} -> {new_name}")
                else:
                    print(f"[FAIL] unable to rename {str(p)=}")
        print(f'done append_extension("{extension} ...")')
    
    def set_wallpaper_path_list(self,check_size=False):
        ''' Set attribute `wallpaper_path_list` to the list of path names of wallpapers in `self.path`
            
            if `check_size` is True then get the the maxima of
                of all widths  of all images in `self.path` whose names end on ".jpg" and
                of all heights of all images in `self.path` whose names end on ".jpg"
            and omit all path names if they do not belong to an image with these dimensions.
            
            Parameters:
            check_size                  True:  Consider only the images with greatest dimensions
                                        False: Take all images (dafault)
        '''
        path_list = [path for path in self.path.iterdir()]
        # print(f'    | {path_list=}')
        if check_size:
            width_list = [self.catch(path,0) for path in path_list if str(path).endswith(".jpg")]
            if len(width_list) == 0:
                # print("    | No images with positive width in {}".format(folder.name))
                return []
            width = max(width_list)
            # print(f'    | {width=}')

            height_list = [self.catch(path,1) for path in path_list if str(path).endswith(".jpg") and self.catch(path,0)==width]
            if len(height_list) == 0:
                # print("    | No images with positive height in {}".format(folder.name))
                return []
            height = max(height_list)
            # print(f'    | {height=}')
        
        result = []
        if check_size:
            self.wallpaper_path_list = [str(path) for path in path_list if self.do_check_size(path,width,height)]
            # print(f'    | {len(self.wallpaper_path_list)=} wallpapers of size {width}x{height}')
            # print(f'    | {self.wallpaper_path_list=}')
        else:
            self.wallpaper_path_list = [str(path) for path in path_list]
            # print(f'    | {len(self.wallpaper_path_list)=} wallpapers')
            # print(f'    | {self.wallpaper_path_list=}')
        print(f'set_wallpaper_path_list: {len(self.wallpaper_path_list)}/{len(path_list)} files in {str(self.path)}/ meet the conditions of wallpapers')
        return self.wallpaper_path_list
    
    def catch(self,path,a):
        try:
            return(Image.open(str(path)).size[a])
        except Exception as e:
            return 0

    def do_check_size(self,path,width,height):
        ''' Return True if `path` is the path name to an image file with a wallpaper
            Images are considered wallpapers if their size is `width` x `height`
            
            Parameters:
            path                        Path() object of folder get wallpapers from
            width                       Width an image must have for being able to be a wallpaper
            height                      Height an image must have for being able to be a wallpaper
        '''
        if not str(path).endswith(".jpg"):
            return(False)
        if self.catch(path,0) != width:
            return(False)
        if self.catch(path,1) != height:
            return(False)
        return(True)
    
    def print(self):
        print(self.wallpaper_path_list)
    
    def reduce(self): # ,path_list):
        ''' Remove all files from folder `self.path` that are not contained in the
            list `self.wallpaper_path_list` of names of pathes to wallpapers
        
            No longer needed Parameters:
            path_list                   list of path names of the files which must not be deleted
            folder                      Path() object of folder get wallpapers from
        '''
        print(f'exec reduce() ...')
        for path in self.path.iterdir():
            if str(path) in self.wallpaper_path_list:
                print("keeping {} in {}/".format(str(path),self.path_name))
            else:
                print("removing {} from {}/".format(str(path),self.path_name))
                path.unlink()
        print(f'done reduce()')
    
    def rename_collected_wallpapers(self):
        ''' Check if files copied from system already were stored in the final collection
            If so rename them such that they have the same name as in the final collection
        '''
        print(f'exec rename_collected_wallpapers() ...')
        collection_path = Path('.') / 'Collection'
        wallpapers_found = 0                                      # TODO: [False for p in self.wallpaper_path_list]
        for path in collection_path.iterdir():
            for i in  range(len(self.wallpaper_path_list)):
                path_str = self.wallpaper_path_list[i]
                if filecmp.cmp(str(path),path_str, shallow=False):
                    print("rename {} -> {}".format(Path(path_str).name,path.name))
                    new_path = Path(path_str).parent / path.name
                    Path(path_str).rename(new_path)
                    self.wallpaper_path_list[i] = str(new_path)
                    wallpapers_found += 1                         # TODO: wallpapers_found[i] = True
                    break
            if wallpapers_found == len(self.wallpaper_path_list): # TODO: if all(wallpapers_found):
                break
        print(f'done rename_collected_wallpapers()')
    
    ### all done for from_system() ###
    
    def from_collection(self,collection):
        ''' Copy all the wallpaper files in SplashScreenCollection() object `collection`
            into the folder with path `self.path`
            
            Parameters:
            collection                  SplashScreenCollection() object with images to import
        '''
        for w in collection:
            destination.append(w)
        print(self.print_script_renaming_wallpapers())
    
    ### make SplashScreenCollection be an iterator:
    
    def __len__(self):
        try:
            return len(self.wallpaper_path_list)
        except AttributeError:
            return -1
    
    def append(self,path_str):
        ''' Add an item to the end of the list stored as attribute `wallpaper_path_list`, i.e.
            Import the wallpaper in file to which Path() object `path` points to.
            Equivalent to a[len(a):] = [path].

            Parameters:
            path                        path to wallpaper file to be added to this collection
        '''
        path = Path(path_str)
        if (self.path / path.name).is_file():
            print(f'[FAIL] Wallpaper "{str(self.path / path.name)}" already exists')
        else:
            shutil.copy(path, self.path)
            self.wallpaper_path_list.append(path)
            print(f'[ OK ] Wallpaper "{str(self.path / path.name)}" collected')

    # TODO: Delegate the following methods to self.wallpaper_path_list,
    #       rf. [Data Structures](https://docs.python.org/3/tutorial/datastructures.html)
    def extend(self,iterable):
        ''' Extend the list by appending all the items from the iterable.
            Equivalent to a[len(a):] = iterable.
        '''
        self.wallpaper_path_list.extend(iterable)
        pass

    def insert(self, i, x):
        ''' Insert an item at a given position.
            The first argument is the index of the element before which to insert,
            so  a.insert(0, x) inserts at the front of the list,
            and a.insert(len(a), x) is equivalent to a.append(x).
        '''
        self.wallpaper_path_list.insert(i, x)
        pass

    def remove(self,x):
        ''' Remove the first item from the list whose value is equal to x.
            It raises a `ValueError` if there is no such item.
        '''
        self.wallpaper_path_list.remove(x)
        pass

    # def pop(self,[i]):
    #     ''' Remove the item at the given position in the list, and return it.
    #         If no index is specified, a.pop() removes and returns the last item in the list.
    #         (The square brackets around the i in the method signature denote that the parameter is optional,
    #          not that you should type square brackets at that position.
    #          You will see this notation frequently in the Python Library Reference.)
    #     '''
    #     self.wallpaper_path_list.pop(x)
    #     pass

    def clear(self):
        ''' Remove all items from the list. Equivalent to del a[:].
        '''
        self.wallpaper_path_list.clear(x)
        pass

    #def index(self,x[, start[, end]]):
    #    ''' Return zero-based index in the list of the first item whose value is equal to x.
    #        Raises a ValueError if there is no such item.
    #
    #        The optional arguments start and end are interpreted as in the slice notation and
    #        are used to limit the search to a particular subsequence of the list.
    #        The returned index is computed relative to the beginning of the full sequence
    #        rather than the start argument.
    #    '''
    #    self.wallpaper_path_list.index(x)
    #    pass

    def count(self,x):
        ''' Return the number of times x appears in the list.
        '''
        self.wallpaper_path_list.count(x)
        pass

    def sort(self,*, key=None, reverse=False):
        ''' Sort the items of the list in place
            (the arguments can be used for sort customization, see sorted() for their explanation).
        '''
        self.wallpaper_path_list.sort(x)
        pass

    def reverse(self):
        ''' Reverse the elements of the list in place.
        '''
        self.wallpaper_path_list.reverse(x)
        pass

    def copy(self):
        '''Return a shallow copy of the list. Equivalent to a[:].
        '''
        self.wallpaper_path_list.copy(x)
        pass
    
    def get_value(self, index):
        len_ = len(self)
        if len_ == -1:
            raise AttributeError
        if index < 0 or index >= len_:
            raise IndexError('list index out of range')
        return self.wallpaper_path_list[index]
    
    def __getitem__(self, key):
        ''' Return item at position `key` by delegation to internal method `get_value` for
            the support of slicing

            Parameters:
            key                         index for accessing `self.wallpaper_path_list`
        '''
        if isinstance(key, slice):
            start, stop, step = key.indices(len(self))
            return SplashScreenCollection([self[i] for i in range(start, stop, step)])
        elif isinstance(key, int):
            return self.get_value(key)
        elif isinstance(key, tuple):
            raise NotImplementedError('Tuple as index')
        else:
            raise TypeError('Invalid argument type: {}'.format(type(key)))
    
    def __iter__(self):
        return self.SplashScreenCollectionIterator(self)
    
    class  SplashScreenCollectionIterator():
        def __init__(self, iterable):
            self.__iterable = iterable
            self.__index = 0
        
        def __iter__(self):
            return self
        
        def __next__(self):
            if self.__index >= len(self.__iterable):
                raise StopIteration
            # return the next path (as string) / the name of the next path
            path_name = self.__iterable.wallpaper_path_list[self.__index]
            self.__index += 1
            return path_name
    
    def add_spaces(self,text,indent=2):
        return('  ' * indent + text)
    
    def add_newline(self,text,indent=2):
        return(self.add_spaces(text,indent) + '\n')
    
    def print_subsection_for_folder(self):
        text = self.add_newline(f'#### Q{((self.today.month-1) // 4) + 1}/', indent = 0)
        text += self.add_newline(f'##### {self.month}/', indent = 1)
        text += self.add_newline(f'###### Show Wallpapers in {self.path}/')
        text += self.add_newline(f'collection = SplashScreenCollection()')
        text += self.add_newline(f'folder_name = "{self.path}"')
        text += self.add_newline("if collection.set_path_name(folder_name,True):")
        text += self.add_newline("    print(f'[ OK ] {folder_name=} exists')")
        # No need to generate invoking `self.reduce()` since it is called by `self.from_system(username)`
        # if not str(self.path).startswith( 'Collection' ):
        #     text += self.add_newline("    collection.reduce()")
        text += self.add_newline("    collection.show()")
        text += self.add_newline("else:")
        text += self.add_newline("    print(f'[FAIL] {folder_name=} not found')")
        text += self.add_newline(f'###### renaming')
        print("\n"+text)
    
    def print_script_renaming_wallpapers(self):
        ''' Output a script renaming the files given by the list file_list all of which
            need to be in the folder with name folder_name in the current directory.

            No longer needed Parameters (-> self.wallpaper_path_list, self.rename_file() renames in self.path):
            file_list                   list of files to be renamed in folder with name folder_name
            folder_name                 name of folder in current directory
        '''
        print(f'\nexec print_script_renaming_wallpapers() ...\n')
        text = ""
        for path_name in self.wallpaper_path_list:
            old_name = self.remove_prefix(path_name,self.path_name+'\\')
            comment_prefix = ""
            # TODO: comment out the statements for wallpapers that already have been renamed,
            #       i. e. for those values of Path(path_name).stem whose filename without the path is a hash key,
            #       i. e. for those values of old_name that are a hash key plus extension ".jpg"
            #       rf. [How to get only the name of the path with python?](https://stackoverflow.com/questions/50876840/how-to-get-only-the-name-of-the-path-with-python)
            # if old_name minus extension is an unrenamed hash key:
            #     comment_prefix = "# "
            if not re.search(r'^[0-9a-z]+.jpg', old_name): # old_name minus extension is an unrenamed hash key:
                comment_prefix = "# "
            text += self.add_newline(f'{comment_prefix}old_file = "{old_name}"',False)
            text += self.add_newline(f'{comment_prefix}new_file = "TTTTT.jpg"',False)
            text += self.add_newline(f"{comment_prefix}collection.rename_file(old_file,new_file)\n",False)
        print("\n"+text+"\n")
        print("print(\"\\nDo NOT forget to reload collection!\")")
    
    def show(self):
        ''' Search wallpapers in folder with name folder_name and show them in a dataframe
            with links to results of respective Google Reverse Image Searches.

            No longer needed Parameters (->self.wallpaper_path_list, self.display() uses self.path_name):
            folder_name                 name of folder to search wallpapers in
            check_size                  True:  Consider only the images with greatest dimensions
                                        False: Take all images (dafault)
        '''
        self.display()
        if len(self.wallpaper_path_list)>0:
            self.print_script_renaming_wallpapers()

    def display(self):
        ''' For each file in the folder passed as self.path of class Path two rows of
            a dataframe with the quoted and unquoted name of the path to the file are
            created.
            Formatting with the function `clickable_image_html()` is applied when
            converting to html the result of which is passed to display(HTML())
        
            No longer needed Parameters:
            file_list     -             list of pathes to the wallpapers to build a dataframe with
            folder_name   -             name of folder with the wallpapers,
        '''
        wallpaper_column='Wallpapers in {}/'.format(str(self.path_name))
        quoted_file_list = [ f'<{str(path)}>' for path in self.wallpaper_path_list]
        # DONE: insert quote entries of file_list at rows with indices 1+(2)
        files = [None]*(len(self.wallpaper_path_list)+len(quoted_file_list))
        files[::2] = quoted_file_list
        files[1::2] = self.wallpaper_path_list
        # print(f'{files=}') # OK
        df = pd.DataFrame( files, columns = [wallpaper_column] )
        # OR TODO: quote entries of file_list and insert them at rows with indices 1+(2)
        
        # TODO: Maybe this won't work with clickable_image_html() being a method!
        format_dict = { wallpaper_column: self.clickable_image_html }
        html_str=df.to_html(escape=False, formatters=format_dict)
        display(HTML(html_str))

    def exists(self):
        ''' Check if `self.path` is the path to a folder which exists
            
            Return:
            True                        if `self.path` exists, otherwise False
        '''
        if hasattr(a, 'path'):
            if isinstance(self.path, Path):
                return(self.path.is_dir())
        return(False)

    def remove_prefix(self,text,prefix):
        text_str = str(text)
        if text_str.startswith(prefix):
            return text_str[len(prefix):]
        return text_str  # or whatever
    
    def path_to_image_html(self,path):
        ''' This function essentially converts a path to a local image to
            '<img src="'+ path + '" />' format. And one can put any
            formatting adjustments to control the height, aspect ratio, size etc.
            within as in the below example.

            Parameters:
            path                        Path() object of local image
        '''
        # remove_prefix(str(path), str(Path.cwd())+'\\')
        result = '<img src="'+ str(path) + '" />'
        print(f'path_to_image_html({path=}):\n    {result}')
        return result

    def clickable_image_html(self,path):
        ''' Convert path to a local image to the image tag returned by
            path_to_image_html() inside an <a href...> tag pointing to
            a link to the results of a Google Reverse Image Search
            obtained as answer to a post request.

            Parameters:
            path                        Path() object of local image
        '''
        # DONE: check for quoted entries instead of for extension ".jpg"
        if str(path).startswith("<") and str(path).endswith(">"):
            # DONE: remove quotes from path if path is quoted
            return str(path).lstrip("<").rstrip(">")
        else:
            image_html_str = self.path_to_image_html(path)
            filePath = fr"{str(path)}"
            searchUrl = 'http://www.google.com/searchbyimage/upload'
            multipart = {'encoded_image': (filePath, open(filePath, 'rb')), 'image_content': ''}
            response = requests.post(searchUrl, files=multipart, allow_redirects=False)
            fetchUrl = response.headers['Location']
            # location = location_of_image(fetchUrl)
            return '<a href="{}">{}</a><br>'.format(fetchUrl,image_html_str) # "...{}".format(...,location)
    
    def rename_file(self,old_name,new_name):
        ''' Rename file with name old_file into new_file

            Parameters:
            old_name   (string)         old name of file
            new_name   (string)         new name of file
        '''
        try:
            old_path = self.path / old_name
            new_path = self.path / new_name
            old_path.rename(new_path)
            print("{}\n  -> {}".format(old_name,new_name))
        except Exception as e:
            # print("{e}: unable to rename {old_file}")
            print(e)
    
    def location_of_image(url):
        ''' Parse the results of the Google Reverse Image Search given by `url` and 
            try to determine where the photo was taken

            NICE TO HAVE, NOT WORKING YET

            Parameters:
            url             -           link to  the results page of a Google Reverse Image Search
        '''
        usr_agent = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          # 'Chrome/61.0.3163.100 Safari/537.36'
        }
        def fetch_results(search_url):
            response = requests.get(search_url, headers=usr_agent)
            response.raise_for_status()
            return response.text

        def parse_results(raw_html):
            soup = BeautifulSoup(raw_html, 'html.parser')
            result_block = soup.find_all('div', attrs={'class': 'g'})
            for result in result_block:
                link = result.find('a', href=True)
                title = result.find('h3')
                text = link.get_text()
                if link and title:
                    yield text[:text.index("https://")] # + " -> " + link['href']

        # return("location_of_image({})".format(url))

        html = fetch_results(url)
        results = list(parse_results(html))
        # results = ["links","from","Google"]
        results = [ "<li>{}</li>".format(elem) for elem in results ]
        return ''.join(['<ol>',''.join(results),'</ol>'])



```

We would **prefer** to return an interactive dataframe as described in
* [Creating Interactive Data Tables in Plotly Dash | by Akash Kaul](https://towardsdatascience.com/creating-interactive-data-tables-in-plotly-dash-6d371de0942b)


### Usage


#### `collection.from_system()` replaces former `collector.collect()`

```python
collection = SplashScreenCollection() # SplashScreenCollection(True)
folder_name = collection.set_path_name_timestamp()
collection.from_system("schwa")
```


#### Show and prepare renaming wallpapers in a collection

```python
collection = SplashScreenCollection()
folder_name = "20220607_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python
old_file = "03524bd7ed240954228c0bb4d55046c3861a758b5bd74a2a7e738e6181d04312.jpg"
new_file = "Gemälde von Thomas Cole, Blick auf die Catskill Mountains im Bundesstaat New York im Frühherbst - 1837, Hudson River School.jpg"
collection.rename_file(old_file,new_file)

old_file = "41793c049cb597aab5239c999d0958179eb1f82559061c81da316de678b60f7f.jpg"
new_file = "Costa Vedere, Madagascar.jpg"
collection.rename_file(old_file,new_file)

old_file = "45a1cdade789a5162de9527a4eec2bb9f51a82f4b51f73cda21b28c4e3e5d019.jpg"
new_file = "Bay - Scarborough Beach bei Sonnenuntergang, Südafrika.jpg"
collection.rename_file(old_file,new_file)

old_file = "8b7a9508c4aecbe67fb699a2c145f3ab86e92000db5b769a092ffbaf7dc26a91.jpg"
new_file = "Berg Wildseeloder, Tirol, Österreich.jpg"
collection.rename_file(old_file,new_file)

old_file = "aab37e73041d6c597e769472059550be53961bbdbf889a12cdf454a186aedd67.jpg"
new_file = "Bonsai Rock, Lake Tahoe, Nevada, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "e8f91a5f0327ecaec496ede1d22ee7cc3875c2bc0fa4ea06914b016f8fa4481a.jpg"
new_file = "Rosapelikane, Namibia.jpg"
collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

#### Populating wallpaper collections with other collections replaces the function `copy_wallpapers(source,target)`

```python
print(f'{collection.path_name=}')
```

```python
collection.show()
```

```python
collection = SplashScreenCollection()
collection.set_path_name("20220615_1")
collection.print()
```

```python
destination = SplashScreenCollection(False)
destination.set_path_name("Collection")
destination.from_collection(collection)
```


#### TODO:  **Create and Display Dataframe With  Images and Links to Results of Google Reverse Image Searches**


Using the package
[Google-Images-Search - PyPI](https://pypi.org/project/Google-Images-Search/)
requires setting up a Google developers account with
* a project
* enabled Google Custom Search API, and
* generated API key credentials

but does not facilitate **Reverse** Image Searches.


The function `clickable_image_html(path)` below implements the **reverse image search workflow** as shown in the answer
> [Google reverse image search using POST request](https://stackoverflow.com/questions/23270175/google-reverse-image-search-using-post-request)

on Stack Overflow.


After having found the link to Google Reverse Image Search Results these results should be evaluated in order to find a reasonable description for each image that it is to be proposed to be renamed to.


### 2021

<!-- #region heading_collapsed=true -->
#### Show Wallpapers in 20211214_1/
<!-- #endregion -->

```python hidden=true
file_name_list = display_and_prepare_renaming_wallpapers("20211214_1",True)
```

<!-- #region heading_collapsed=true -->
#### Show Wallpapers in 20211214_2/
<!-- #endregion -->

```python hidden=true
folder_name = "20211214_2"
file_name_list = display_and_prepare_renaming_wallpapers(folder_name,True)
```

```python hidden=true
from pathlib import Path

folder = Path(".") / "20211214_2"

old_file = folder / "42ed0cdf80111860edb0fd0ebedc4d61ab0bbab864bd8fbe52ed44a027f7850c.jpg"
new_file = folder / "Naturpark bayerische Rhön mit Lupinenfeld bei Sonnenaufgang, Deutschland.jpg"
old_file.rename(new_file)

old_file = folder / "46c4dd0006b8e42cafd420164c7f4186b6595d3b87bea683c30ab82065b356f9.jpg"
new_file = folder / "Lanikai Beach in Kailua mit Moku Nui und Moku Iki, Oahu, HI, USA.jpg"
old_file.rename(new_file)

old_file = folder / "cf2067f09d5f988f2e5f3477ad4c79889b2e49cff9edaa09c904c3f17ef9b949.jpg"
new_file = folder / "Nationalpark Virgin-Islands.jpg"
old_file.rename(new_file)

old_file = folder / "d98d37454bbda7e45887b764482edb422b24eaf35ac6738ef0d663f0f197453c.jpg"
new_file = folder / "London, Luftaufnahme der Tower Bridge, England, UK.jpg"
old_file.rename(new_file)

old_file = folder / "daa75bc777aec60877cef680d76ad29a48bd8c4c596597c811a612da6e182323.jpg"
new_file = folder / "Los Angeles, Palm Trees Street zur Innenstadt, CA, USA.jpg"
old_file.rename(new_file)

old_file = folder / "e705d87513642691227a3e873bd9cb95c0991af676d9677dc0f752f342898f90.jpg"
new_file = folder / "Pyramiden von Gizeh, Kairo, Ägypten.jpg"
old_file.rename(new_file)
```

```python hidden=true
remove_non_wallpapers("20211214_2",file_name_list)
# ?remove_non_wallpapers
```

<!-- #region heading_collapsed=true -->
#### Show Wallpapers in 20211216_1/
<!-- #endregion -->

```python hidden=true
folder_name = "20211216_1"
file_name_list = display_and_prepare_renaming_wallpapers(folder_name,True)
```

```python hidden=true
from pathlib import Path

folder = Path(".") / folder_name

# old_file = folder / "2d697ab2e3cb6a9b93651304b8933e3a9c936fc969d00e45ffe4f7accd5eb1e2.jpg"
# new_file = folder / "Burg Berlanga de Duero, Provinz Soria, Kastilien und Leon, Spanien.jpg"
# old_file.rename(new_file)

# old_file = folder / "703a7716b10321e1c218dced95053070ba4ea7bd1a5968666b1196ddf9e53a04.jpg"
# new_file = folder / "Scheveningen, Den Haag, Niederlande.jpg"
# old_file.rename(new_file)

# old_file = folder / "cf7fbfa5f6d2ff49c06f62f31b580689b9406adc040950443685384c47bf9753.jpg"
# new_file = folder / "Ägerisee, Schweiz.jpg"
# old_file.rename(new_file)

# old_file = folder / "d98d37454bbda7e45887b764482edb422b24eaf35ac6738ef0d663f0f197453c.jpg"
# new_file = folder / "London in Luftaufnahme, England, UK.jpg"
# old_file.rename(new_file)

# old_file = folder / "London in Luftaufnahme, England, UK.jpg"
# new_file = folder / "London, Luftaufnahme der Tower Bridge, England, UK"
# old_file.rename(new_file)

# old_file = folder / "df367cf955beb6676a10a66b802669a85803c0015cb0fbf614b6bf36d0091886.jpg"
# new_file = folder / "Bay - Playa del Silencio in Cudillero, Asturien, Spanien.jpg"
# old_file.rename(new_file)

# old_file = folder / "e605dbbee3f37a10d2d110ccf608a16fa01aebec5f35dc5e735de39da6890001.jpg"
# new_file = folder / "Nationalpark Theodore-Roosevelt, Inselberg in Badland-Landschaft, North Dakota, USA.jpg"
# old_file.rename(new_file)
```

```python hidden=true
remove_non_wallpapers(folder_name)
```

<!-- #region heading_collapsed=true -->
#### Show Wallpapers 20211217_1/
<!-- #endregion -->

<!-- #region hidden=true -->
StackOverflow: [How to do reverse image search on google by uploading image url?](https://stackoverflow.com/questions/59176559/how-to-do-reverse-image-search-on-google-by-uploading-image-url)
<!-- #endregion -->

```python hidden=true
folder_name = "20211217_1"
file_name_list = file_name_list = display_and_prepare_renaming_wallpapers(folder_name,True)
```

```python hidden=true
from pathlib import Path

old_file = folder / "25e8925fb4ec185082b2d5364ff1ff8eb0ce89754b778c2eebf61e10c0f5e2cc.jpg"
new_file = folder / "New York City, NY, USA.jpg"
old_file.rename(new_file)

old_file = folder / "703a7716b10321e1c218dced95053070ba4ea7bd1a5968666b1196ddf9e53a04.jpg"
new_file = folder / "Scheveningen, Den Haag, Niederlande.jpg"
old_file.rename(new_file)

old_file = folder / "80719d8b250b430d5fa043164cbaf8e0c80d4493d1c94274171621592bdce083.jpg"
new_file = folder / "Coron Island, Palawan, Philippinen.jpg"
old_file.rename(new_file)

old_file = folder / "b5de60216621698a18af602cb3215d4046ed0515cdfe07599d7128952b336a11.jpg"
new_file = folder / "Miyakojima-Insel, Okinawa, Japan.jpg"
old_file.rename(new_file)

old_file = folder / "df367cf955beb6676a10a66b802669a85803c0015cb0fbf614b6bf36d0091886.jpg"
new_file = folder / "Bay - Playa del Silencio in Cudillero, Asturien, Spanien.jpg"
old_file.rename(new_file)
```

```python hidden=true
remove_non_wallpapers(folder_name)
```

<!-- #region heading_collapsed=true -->
### 2022
<!-- #endregion -->

<!-- #region hidden=true -->
#### January
<!-- #endregion -->

<!-- #region hidden=true -->
##### Show Wallpapers in 20220101_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220101_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

old_file = folder / "58fbc0f1cfe19d326edbf51b74c61995c3386dc92f4848bd73596097ef2051a1.jpg"
new_file = folder / "Naturpark - Valley of Fire, NV, USA.jpg"
old_file.rename(new_file)

old_file = folder / "2a2a3438ed1ff5525b8089ec285a7b30fa3135134f7547f9b55fdad87f9b62ce.jpg"
new_file = folder / "Nationalpark Los Glaciares, Berg Cerro Torre, Patagonien, Argentinien.jpg"
old_file.rename(new_file)

old_file = folder / "a46575de4ee21b793677cde31b37d0cd7eb6081a1dfa59b816ed8138c7fe58c0.jpg"
new_file = folder / "Nationalpark Amboró, Santa Cruz, Bolivien.jpg"
old_file.rename(new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220104_1
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220104_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder = Path(".") / "20220104_1"

old_file = folder / "0657a2d3fd21087aea9bf1831e13bde67796995742c70173d2d9c965b4965ad4.jpg"
new_file = folder / "Gemälde Canadian Rockies (Lake Louise) von Albert Bierstadt, The Metropolitan Museum of Art.jpg"
old_file.rename(new_file)

old_file = folder / "0fedc38e49a5796f5bfdbc7fde6553b944486b2b76e7a0f2043e489f98a2c72d.jpg"
new_file = folder / "Abtei Mont Saint-Michel, UNESCO-Welterbe, Normandie, Frankreich, Departement Manche.jpg"
old_file.rename(new_file)

old_file = folder / "119991f059b995449935e0bd4993f7edccd8a916900110f7c7e737bad1511c0e.jpg"
new_file = folder / "natural landscape.jpg"
old_file.rename(new_file)

old_file = folder / "2bfe9a4e72c787f17812dc49bb926f38ba3d20ccaad47c7422198e97fbdeaade.jpg"
new_file = folder / "Wasserfall - Saltos de Petrohué, Nationalpark Vicente Pérez Rosales, Región de los Lagos, Chile.jpg"
old_file.rename(new_file)

old_file = folder / "c5827f7a5f4a5ba9c10cba9ca949b2861e2961e5cdb21cfae9cf1b2fb68fbd13.jpg"
new_file = folder / "Banyak Inseln, tropischer Archipel nahe Sumatra, Aceh, Indonesien.jpg"
old_file.rename(new_file)

old_file = folder / "c75b76e8b7a1fca82786f9bb9723cbb616c00ea0ed7ec5bf88aba0e9f1e88ee5.jpg"
new_file = folder / "Wasserfall - Nationalpark Plitvicer Seen 1, Kroatien.jpg"
old_file.rename(new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220106/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220106_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
path = Path(".") / "20220106_1"
file_name_list = get_wallpapers(path,True)
print("filtered {}  wallpapers {}".format(len(file_name_list),file_name_list))
create_script_rename_wallpapers(file_name_list,path.name)
```

```python hidden=true
folder = Path(".") / "20220106_1"

old_file = folder / "1a9932db03aea52ac08cf20acb418d833508e4c714475c632e1391f84a2e8143.jpg"
new_file = folder / "Astronomie - Balken-Spiralgalaxie NGC 2835, Auge der Schlange.jpg"
old_file.rename(new_file)

old_file = folder / "1b73bbcc0b8b68102a602d55f543a4ee36c004a1a9dcd2f944b0381674d30a1e.jpg"
new_file = folder / "Weiße Wüste, Sahara, Ägypten.jpg"
old_file.rename(new_file)

old_file = folder / "fab2956f48be1814ceb39a0e7816f6a0d2362707a178b1b2860c175672c52e79.jpg"
new_file = folder / "Wasserfall - Nationalpark Plitvicer Seen 5, Kroatien.jpg"
old_file.rename(new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220109/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220109_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder = Path(".") / "20220109_1"

old_file = folder / "21c716aacd02cc7c1f298324714343ae1a08e4c44220ea3c2c045c25e7e6730d.jpg"
new_file = folder / "Brücke - Pont Jacques-Cartier zwischen Montreal und Longueuil über den Sankt-Lorenz-Strom, Québec, Kanada.jpg"
old_file.rename(new_file)

old_file = folder / "441814265ba97f274c75253bcfa7af180ed558c51ce474c196c43ef5368f6536.jpg"
new_file = folder / "Naturpark Drei Zinnen mit Kriegstunnel, Sextner Dolomiten, Südtirol, Italien.jpg"
old_file.rename(new_file)

old_file = folder / "d5c2fcebbe32f15b4a2734b21a59d5eed92e055980f51b5c2b4def65cff29914.jpg"
new_file = folder / "Eisfeld Perito Moreno mit blauer Eislagune, Departement Lago Argentino, Argentinien.jpg"
old_file.rename(new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220111/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220111_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder = Path(".") / "20220111_1"

old_file = folder / "Nationalpark - Künstlerische Muster bei New Blue Spring im Winter im Yellowstone-Nationalpark, Wyoming, USA.jpg"
new_file = folder / "New Blue Spring, artistic patterns in winter, Yellowstone National Park, Wyoming, USA.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220115_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220115_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder = Path(".") / "20220115_1"

old_file = folder / "3383b3262748a5940c87f523f33d09c519ba1c62270990fe7e0f465ce16ebf23.jpg"
new_file = folder / "Manzanillo-Wildschutzgebiet, Limón, Costa Rica.jpg"
rename_file(old_file,new_file)

old_file = folder / "4d1a0d10b3c57e22cf264c330e2636028752cc77670ae7c2c6430dadb4587e81.jpg"
new_file = folder / "Berg Sinai - auch Jebel Musa, 2285 m, bei Sonnenaufgang, Sinai Halbinsel, Ägypten.jpg"
rename_file(old_file,new_file)

old_file = folder / "4e715737f0205d589a3c69f14a43eecdbb209f4fb85cc8b94425cbf67a887bbe.jpg"
new_file = folder / "Hawa Mahal, Palast Der Winde, Jaipur, Rajasthan, Indien.jpg"
rename_file(old_file,new_file)

old_file = folder / "7efd92b2b4a3c51c9617f632f7f8b2357b9bbe280ceb234407fb1ec3ca86d29c.jpg"
new_file = folder / "Nationalpark Sutjeska - Zelengora Berggipfel und Wiesen, Bosnien und Herzegowina.jpg"
rename_file(old_file,new_file)

old_file = folder / "b02c4d5cf33455e626fb0bb26a2ad3f7d816badd3cc7aa2afa8a572f1049c5ec.jpg"
new_file = folder / "Celeste-Fluss im Tenorio-Nationalpark, Costa Rica.jpg"
rename_file(old_file,new_file)

old_file = folder / "e705d87513642691227a3e873bd9cb95c0991af676d9677dc0f752f342898f90.jpg"
new_file = folder / "Pyramiden, Gizeh, Kairo, Ägypten.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220125_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220125_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder = Path(".") / folder_name

old_file = folder / "21c716aacd02cc7c1f298324714343ae1a08e4c44220ea3c2c045c25e7e6730d.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "91c25cbdab597876f8556003fe7e3ff4295203be72ebcb50bec44638f304bcf5.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "c7b92a9b31fe16ecf9c63c5194430a10ef40f536eaa1a245e9e4466213ff5fea.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "cd4a6ee06e14572dcfba70e6c0e2b0a122c8ea30bc20afe3dd91b5d0cd3c29de.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "df2e8a3871930360adad925df3e25e36803ad135a93f5ff40050da7505a06543.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "f492102ba254bf96103ac449b62b9133409db418f657c9e710cb874f93aa0fd4.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220130_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220130_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220130_1"
folder = Path(".") / folder_name

old_file = folder / "46edd46c145dbb53e9990ba7fa6dc58f92f30312919419fb5ee4f0f36e12a84c.jpg"
new_file = folder / "Savanne von Eichen, dehesa, La Serena, Badajoz, Extremadura, Spanien.jpg"
rename_file(old_file,new_file)

old_file = folder / "4ce05edae0003bf62e50c9e64f39709f6781bba60525e5ee058b51cbc12dc357.jpg"
new_file = folder / "Brücke - Rakotzbrücke im Rhododendronpark in Gablenz-Kromlau, Sachsen, Deutschland.jpg"
rename_file(old_file,new_file)

old_file = folder / "5e30e1aaf94465039ab1843ea1fbe9fb15b9230f4b3796c9deeed2cad6a9653a.jpg"
new_file = folder / "Adivino-Pyramide des Wahrsagers, Uxmal, Yucatán, México.jpg"
rename_file(old_file,new_file)

old_file = folder / "cd4a6ee06e14572dcfba70e6c0e2b0a122c8ea30bc20afe3dd91b5d0cd3c29de.jpg"
new_file = folder / "Pyramiden von Gizeh im Drohnenfoto, Kairo, Ägypten.jpg"
rename_file(old_file,new_file)

old_file = folder / "dc479424f1f2e36c58e0bb6022bb13968c3461ca3f08eb722ef81c9f6fe44470.jpg"
new_file = folder / "Toge, Tokamachi, Niigata 942-1351, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "f4d5d34c3a2f99e445adb70798ec962c2da20a433dafc5b46c3f9fcfbc61b658.jpg"
new_file = folder / "Sentinel vom Chapman's Peak Drive aus, Kap-Halbinsel, Südafrika.jpg"
rename_file(old_file,new_file)
```


<!-- #region hidden=true -->
##### Show Wallpapers in 20220131_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220131_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220131_1"
folder = Path(".") / folder_name

old_file = folder / "4ce05edae0003bf62e50c9e64f39709f6781bba60525e5ee058b51cbc12dc357.jpg"
new_file = folder / "Brücke - Rakotzbrücke im Rhododendronpark in Gablenz-Kromlau, Sachsen, Deutschland.jpg"
rename_file(old_file,new_file)

old_file = folder / "51fb60ba2a872abad6e58600bbd6fb92a679bb499c6c63c18de1b0b497f77de4.jpg"
new_file = folder / "Dead Horse Point State Park, Utah, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "5e30e1aaf94465039ab1843ea1fbe9fb15b9230f4b3796c9deeed2cad6a9653a.jpg"
new_file = folder / "Adivino-Pyramide des Wahrsagers, Uxmal, Yucatán, México.jpg"
rename_file(old_file,new_file)

old_file = folder / "dc479424f1f2e36c58e0bb6022bb13968c3461ca3f08eb722ef81c9f6fe44470.jpg"
new_file = folder / "Toge, Tokamachi, Niigata 942-1351, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "ec977735579517a88ee7b6a1e401d01f6448a87e4043fe5de530a1eca8bdbf5d.jpg"
new_file = folder / "Three Graces, Royal Liver Building, Port of Liverpool Building, and Cunard Building, Liverpool.jpg"
rename_file(old_file,new_file)

old_file = folder / "ff0d5590790d5e35777bdb64a33a1660aac572317c56e0abc1017bd6cf8b8401.jpg"
new_file = folder / "Champagner-Pool, Waikato 3073, Neuseeland.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
#### February
<!-- #endregion -->

<!-- #region hidden=true -->
##### Show Wallpapers in 20220204_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220204_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220204_1"
folder = Path(".") / folder_name

old_file = folder / "3099b135079ce604d9e6a49ffbc0d6381499e56e4172b9bbd4864810d61b47f9.jpg"
new_file = folder / "Berg Wildseeloder, Tirol, Österreich.jpg"
rename_file(old_file,new_file)

old_file = folder / "4ce05edae0003bf62e50c9e64f39709f6781bba60525e5ee058b51cbc12dc357.jpg"
new_file = folder / "Brücke - Rakotzbrücke im Rhododendronpark in Gablenz-Kromlau, Sachsen, Deutschland.jpg"
rename_file(old_file,new_file)

old_file = folder / "7c3547f0c66ca94af1f93a77f68b4a93a2de2f264951ba4c88dcef6384577235.jpg"
new_file = folder / "See Umm el Ma (Mutter des Wassers) in der Oase Awbari (Ubari), Wüste Sahara, Fezzan, Libyen.jpg"
rename_file(old_file,new_file)

old_file = folder / "8283fedb6ff838d61826f7ff3bff83fbfa8f6d46c18eccdf46585362bba2fb37.jpg"
new_file = folder / "Wasserfall - Präfektur Chiba, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "ae0ed9ef2158660f4f15134896f3b4123bb53b8643d6b62a12bfb30f46531a5c.jpg"
new_file = folder / "Machu Picchu, Ruinen der verlorenen antiken Inka-Stadt, Cusco, Peru.jpg"
rename_file(old_file,new_file)

old_file = folder / "d05d4e1145e29d12965970a94bbee9add7d21917202a779e72a5d38a12247e50.jpg"
new_file = folder / "Riverband Crnojevica, Vranjina-Hügel und Skutarisee, Montenegro.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220208_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220208_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220208_1"
folder = Path(".") / folder_name

old_file = folder / "19d52c1968b7e5e04f045b945291411b442809df5929617138dd9229b9725571.jpg"
new_file = folder / "Nationalpark La Jacques-Cartier, Quai des Rabascas du Pont-Banc.jpg"
rename_file(old_file,new_file)

old_file = folder / "8867cae105f959d901370d260c3ad7370260c4c587154caa6afd4e0e17dc05cf.jpg"
new_file = folder / "Höhlen am Lake Superior, Munising, Michigan, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "c8e0c17bc12b547ceb53d1f784520e501e8ae82b8a60c7f57a738f210b81d25a.jpg"
new_file = folder / "Sahara, Dünen nahe Douz, Kebili, Tunesien.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220219_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220219_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220219_1"
folder = Path(".") / folder_name

old_file = folder / "0f8b1f8528c1a4ce957aae8945b9b5c5defc536988aca1b610e040ad3cf1fac2.jpg"
new_file = folder / "Wasserfall - Bergoase Chebika, Tozeur, Tunesien.jpg"
rename_file(old_file,new_file)

old_file = folder / "2cd39e54cc21e01dd500062708f14fcad2045835f0f0a6b010d535bb99b41bc5.jpg"
new_file = folder / "Gottes Fenster im Blyde River Canyon, Mpumalanga, Südafrika.jpg"
rename_file(old_file,new_file)

old_file = folder / "67c30a33b180da21f987f55d70d78897171679454d4106951cbfde4abc478698.jpg"
new_file = folder / "Maharaja Sayajirao Universität Baroda, Fakultät der Künste. Indien.jpg"
rename_file(old_file,new_file)

old_file = folder / "96b1ad08d4cdf371c5fcf8dc48c8017550b1797905b9119b766d19674f0bfb75.jpg"
new_file = folder / "Semifonte-Kapelle und Ort Petrognano, Barberino Val d'Elsa, Toskana, Italien.jpg"
rename_file(old_file,new_file)

old_file = folder / "a86a6147c0e2429065620cedf97e8b619bdc452c83fa87a39fdb38f11f67b3fc.jpg"
new_file = folder / "Morning in Bolivia. Salar de Uyuni. Isla Incahuasi.jpg"
rename_file(old_file,new_file)

old_file = folder / "cd4a6ee06e14572dcfba70e6c0e2b0a122c8ea30bc20afe3dd91b5d0cd3c29de.jpg"
new_file = folder / "Pyramiden von Gizeh im Drohnenfoto, Kairo, Ägypten.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220223_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220223_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220223_1"
folder = Path(".") / folder_name

# old_file = folder / "96c47efaf584419b7d4255ba627629ebc0a17fb07f529f6b253fcf5222e48b70.jpg"
# new_file = folder / "Bay - Scarborough Beach bei Sonnenuntergang, Südafrika.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "a0458a4a3e870c0bbbf89656a8e5a4bce857e7206bd38af17647ec88aa1b5f86.jpg"
# new_file = folder / "Nationalpark Borjomi Kharagauli, Georgien.jpg"
# rename_file(old_file,new_file)

old_file = folder / "dfabd215c3459636f4a46acf2f9083b1caf382bab445fe6f085734d4bd177d2d.jpg"
new_file = folder / "Nationalpark Mount Rainier, Washington, USA.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220225_1/
<!-- #endregion -->

```python hidden=true
folder_name = "20220225_1"
file_name_list = reduce_to_wallpapers(folder_name,True)
```

```python hidden=true
from pathlib import Path

folder_name = "20220225_1"
folder = Path(".") / folder_name

old_file = folder / "01bbd2343fc40e19340b7895da541287568ea57d7af0235c80e52bbaf1686f18.jpg"
new_file = folder / "Pedra dos Tres Pontoes, Afonso Claudio, Espirito Santo State, Brazil.jpg"
rename_file(old_file,new_file)

old_file = folder / "553e4190c69295e069beaf22980683472198ba0df2c226a5b00eb7f31aa352d2.jpg"
new_file = folder / "Tempel des Poseidon am Kap Sounion in der Ägäis, Griechenland.jpg"
rename_file(old_file,new_file)

old_file = folder / "57989f37059745075c6af3ccf00fb3a99b8a1569642a90a814e6ce73a21c3b67.jpg"
new_file = folder / "Castillejas indivisas in Norman, Oklahoma, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "9773df6a44b8d498aedf474e289f896104300bd9b93ae21290ab1a2baadb40ef.jpg"
new_file = folder / "ESALQ, Öffentliche Landwirtschaftshochschule von oben in Piracicaba, Sao Paulo, Brasilien.jpg"
rename_file(old_file,new_file)

old_file = folder / "e542ce355cea271e2a0888bc4484fc16ae1e845fe412a37edaae2fe3b9916198.jpg"
new_file = folder / "Berg Salkantay über Tal entlang des Salkantay Trek nach Machu Picchu, Peru.jpg"
rename_file(old_file,new_file)

old_file = folder / "fe6885fa8a7d1d476c442e5031a26b090630a386c4996c62f742783e686ba65a.jpg"
new_file = folder / "Vulkan Toliman, See Atitlán, Guatemala, Mittelamerika.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
#### March
<!-- #endregion -->

<!-- #region hidden=true -->
##### Show Wallpapers in 20220305_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220305_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220305_1"
folder = Path(".") / folder_name

old_file = folder / "003a258b0e6ee032d340c1613728c0073ac31477dd3ddb71af269dd4389722ba.jpg"
new_file = folder / "Palmenstraße in Los Angeles, Kalifornien, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "476b505ef23873b0ace27a80ee545bafaa5c4b774864b2b7d052e2c4eed0e6f4.jpg"
new_file = folder / "Bay - Lloret de Mar, Girona, Spanien.jpg"
rename_file(old_file,new_file)

old_file = folder / "e6ffd802bd71331eeb293b4764338dbc1f53dc1a8f6177f8a12b0eeab74ab979.jpg"
new_file = folder / "Bergsee, Himalaya, Nepal.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220309_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220309_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220309_1"
folder = Path(".") / folder_name

old_file = folder / "4795df8aa35fd72bfe9515897cdb2ec2a949fa747bebd14e5170ab1c9d5f208b.jpg"
new_file = folder / "Trevi-Brunnen zwischen der Via Poli und der Via della Stamperia, Trevi, Rom, Italien.jpg"
rename_file(old_file,new_file)

old_file = folder / "7abb99cd91337d29dc4d120a3eaf2f00feb305a94af82dd5e1f36af93835addc.jpg"
new_file = folder / "Mirissa, Matara, Southern Province, Sri Lanka.jpg"
rename_file(old_file,new_file)

old_file = folder / "87f516735a41d92f781c05ff00d58c9f46c85e2025b5c0a1a0068c4db6794a02.jpg"
new_file = folder / "Berg Fuji und Eiszapfen im Yachonomori Park, Präfektur Yamanashi, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "9cee40dd41de5642b90b8ca6bb688b713a0ce33c7215f32eac4e3f2f98bba1f5.jpg"
new_file = folder / "See Petén Itzá in El Ramate bei Sonnenuntergang, Guatemala.jpg"
rename_file(old_file,new_file)

old_file = folder / "aa08501898e7881f246682bfad1ccb362e2b3512a7f7c466b5e68db65fda390e.jpg"
new_file = folder / "Gemälde Canal Grande mit dem Campo della Carità, Venedig, Italien.jpg"
rename_file(old_file,new_file)

old_file = folder / "df2e8a3871930360adad925df3e25e36803ad135a93f5ff40050da7505a06543.jpg"
new_file = folder / "Walker Bay in Hermanus, Western Cape, Südafrika.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220313_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220313_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220313_1"
folder = Path(".") / folder_name

# old_file = folder / "06803b3b389ff15011281266e3e3592024389b174dc23e26610be0e3e77a5bc3.jpg"
# new_file = folder / "Messner Mountain Museum, Località Monte Rite, Cibiana di Cadore (BL), Italia.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "0cda7e8d074bd7e3f9b2a323f42c6d25f6057385c80d48117c445c3e043b7f5b.jpg"
# new_file = folder / "Cana Island am Lake Michigan im Winter, Door County, Wisconsin, USA.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "1a810f7ee0ec4e465e1262f7b9c5b2feed8ea596863ed4cb8630ba0c524bd8b1.jpg"
# new_file = folder / "Resurrection Bay, Kenai Peninsula Borough, Yunan Alaska, USA.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "4795df8aa35fd72bfe9515897cdb2ec2a949fa747bebd14e5170ab1c9d5f208b.jpg"
# new_file = folder / "Trevi-Brunnen zwischen der Via Poli und der Via della Stamperia, Trevi, Rom, Italien.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "907aa61608b8067d95c823438c95f7d374a9c21389a87ef50c0534664d5ac224.jpg"
# new_file = folder / "Nationalforst Wǔlíngyuán mit Sonne, Zhāngjiājiè, Hunan, China.jpg"
# rename_file(old_file,new_file)

old_file = folder / "Kolosseum, Piazza del Colosseo 1, 00184 Roma RM, Italien.jpg"
new_file = folder / "Kolosseum, Piazza del Colosseo 1, Rom, Italien.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220320_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220320_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220320_1"
folder = Path(".") / folder_name

old_file = folder / "2d8fb4e8f96f818445ae854e03441baa5ddb483f8670a110552ff4b98ef7293b.jpg"
new_file = folder / "Reine, Lofoten, Norwegen.jpg"
rename_file(old_file,new_file)

old_file = folder / "5c935167b6b598b6895dc6a381722d2d5721082a467d02253c3e42bc4288776f.jpg"
new_file = folder / "Sigiriya, Dambulla, Central Province, Sri Lanka.jpg"
rename_file(old_file,new_file)

old_file = folder / "7241495a4622193a61088f80db55329fcd379b75adf87bbcfb588aa2e4cc88af.jpg"
new_file = folder / "Fluss Gatesgarthdale Beck am Honister Pass, Lake District, Cumbria, England, UK.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220322_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220322_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220322_1"
folder = Path(".") / folder_name

old_file = folder / "0e53671ab7c7003b5fe294ef8bcca8bd3de60bf2417d973671f9b97300d6aefe.jpg"
new_file = folder / "Glendurgan Garten, Cornwall, England, Vereinigtes Königreich.jpg"
rename_file(old_file,new_file)

old_file = folder / "26ba86ac7fc2e6986b583f7ca8d9b2309ce889d940cb159f6183e36da4becb3e.jpg"
new_file = folder / "Las Vegas, Nevada, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "3075826873a8a22ad529fb960bc9f7b6b085a6339551d3da3200b84f7d470e08.jpg"
new_file = folder / "Bellagio - Perle des Comer Sees, Italien.jpg"
rename_file(old_file,new_file)

old_file = folder / "4ce05edae0003bf62e50c9e64f39709f6781bba60525e5ee058b51cbc12dc357.jpg"
new_file = folder / "Brücke - Rakotzbrücke im Rhododendronpark in Gablenz-Kromlau, Sachsen, Deutschland.jpg"
rename_file(old_file,new_file)

old_file = folder / "783eb6c6adfa7c216fa61bffd13d03b5d3b0436b2dc05fbabd745b672746ad02.jpg"
new_file = folder / "Geiranger, Norway.jpg"
rename_file(old_file,new_file)

old_file = folder / "978eeaec931141614069921879fe80e7f9622b512614f1dcc5df3cef38673e7d.jpg"
new_file = folder / "Schloss Bran, Nationalpark Piatra Craiului, Ciocanu, Rumänien.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220326_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220326_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220326_1"
folder = Path(".") / folder_name

old_file = folder / "5e30e1aaf94465039ab1843ea1fbe9fb15b9230f4b3796c9deeed2cad6a9653a.jpg"
new_file = folder / "Adivino-Pyramide des Wahrsagers, Uxmal, Yucatán, México.jpg"
rename_file(old_file,new_file)

old_file = folder / "86db543eee5587d785b09ebad71d4f3553b62f93c70cc7bd705cdcf7f608ff92.jpg"
new_file = folder / "Lí-Fluss (漓江 Lí Jiāng) und Karst Berge nahe der antiken Stadt 兴坪 Xìngpíng, 阳朔 Yángshuò, 桂林 Guìlín, 廣西 Guǎngxī, China.jpg"
rename_file(old_file,new_file)

old_file = folder / "e9e0e1c64c91871cebc0bc202e72165577a7cb2ef6fff6a20ccd6c2897dbe478.jpg"
new_file = folder / "Echo Parksee und die Skyline von Los Angeles.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220328_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220328_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220328_1"
folder = Path(".") / folder_name

old_file = folder / "6ebcf25540bc0981cc6feb855acca0ef89305f7d9a66ed1042de5ad9e3825c78.jpg"
new_file = folder / "Cap Blanc-Nez, Escalles, Nord-Pas-de-Calais, Frankreich.jpg"
rename_file(old_file,new_file)

old_file = folder / "7cd55949d47dc344c096f437fd33970bc658d5b63d937e9d937cf8adb99bf9c1.jpg"
new_file = folder / "Granite Island, Encounter Bay, South Australia, Australien.jpg"
rename_file(old_file,new_file)

old_file = folder / "e0c9bdba2087a826a465f7fe39edb6366fb0252b1304ae875009dc61c98f2a4b.jpg"
new_file = folder / "Rotes Haus über Sognefjord, Norwegen.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220330_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220330_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220330_1"
folder = Path(".") / folder_name

old_file = folder / "2d8fb4e8f96f818445ae854e03441baa5ddb483f8670a110552ff4b98ef7293b.jpg"
new_file = folder / "Reine, Lofoten, Norwegen.jpg"
rename_file(old_file,new_file)

old_file = folder / "75eae37de95f53598a6d3e897a793eb9e3bdf63ce3af5bdd5d98580f3caee0a3.jpg"
new_file = folder / "Nationalpark Sehlabathebe, Ostlesotho.jpg"
rename_file(old_file,new_file)

old_file = folder / "7fde0b52997d40da43965e41efa104d16ce499573748cb4efe1be2e0b63b8eae.jpg"
new_file = folder / "Ilulissat-Eisfjord (Ilulissat Kangerlua) an der Diskobucht, Grönland.jpg"
rename_file(old_file,new_file)

old_file = folder / "9cc8daa8e6f98cf36b1acb3e13dfce0190de70f859ee54fc9b51e8b29d1d7470.jpg"
new_file = folder / "Nationalpark Big-Bend, Texas, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "b03ad6f62837de6b3f6096d4d9a0f114a40e9683eef976e3bea1fdcbe4cf13c7.jpg"
new_file = folder / "Lí-Fluss (漓江 Lí Jiāng) mit Karstbergen, Guǎngxī 广西, China.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
#### April
<!-- #endregion -->

<!-- #region hidden=true -->
##### Show Wallpapers in 20220405_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220405_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220405_1"
folder = Path(".") / folder_name

old_file = folder / "009d5eed55a0e21f305533f742a8b8b1824622e8804e91902ba0d2f13c6dc633.jpg"
new_file = folder / "Nationalpark Banff, Moraine Lake mit Kanus, Alberta, Kanada.jpg"
rename_file(old_file,new_file)

old_file = folder / "60b8d703fad24d59b56bb85c30fb5a8b9c938626ce049b24cb188f53214bca8d.jpg"
new_file = folder / "Nationalpark Voyageurs, Ash River Visitor Center, Minnesota, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "7c19709d159a9f5fb768f4b489efdb496c9238021c017dec3386004b5eba6c56.jpg"
new_file = folder / "naturpark sintra cascais azeNaturpark Sintra-Cascais, Azenhas Do Mar, Costa de Lisboa, Portugal.jpg"
rename_file(old_file,new_file)

old_file = folder / "c1eea3db0741f1d83a08c08a4fb52e2af3be616b82cd705e0a58f91ff9104ac0.jpg"
new_file = folder / "Bay - Horseshoe Bay Beach, Southhampton Parish, Bermuda.jpg"
rename_file(old_file,new_file)

old_file = folder / "c7b92a9b31fe16ecf9c63c5194430a10ef40f536eaa1a245e9e4466213ff5fea.jpg"
new_file = folder / "Vestrahorn Beach, Batman Mountain, Island.jpg"
rename_file(old_file,new_file)

old_file = folder / "df0f784e155248b805063c00ad4c0289bfd829a6bcedeeb2646c951e7c4ca9c3.jpg"
new_file = folder / "Hana, Maui, Hawaii, USA.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220407_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220407_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220407_1"
folder = Path(".") / folder_name

old_file = folder / "10452b84469d1ee9524c4af4af705fa15caf1e7ca76ad98c0f42b0561ef58ad5.jpg"
new_file = folder / "Leuchtturm Farol da Barra in der Forte de Santo Antonio da Barra, UNESCO-Weltkulturerbe, Salvador, Bahia, Brasilien.jpg"
rename_file(old_file,new_file)

old_file = folder / "45e010735d5dff527a5d5f18914d6233762cc3796585a96d0219977d14c087f7.jpg"
new_file = folder / "Ounianga Kebir, Sahara, Tschad.jpg"
rename_file(old_file,new_file)

old_file = folder / "91c25cbdab597876f8556003fe7e3ff4295203be72ebcb50bec44638f304bcf5.jpg"
new_file = folder / "Warebeth Strand, Orkney-Inseln, Schottland, Großbritannien.jpg"
rename_file(old_file,new_file)

old_file = folder / "b02c4d5cf33455e626fb0bb26a2ad3f7d816badd3cc7aa2afa8a572f1049c5ec.jpg"
new_file = folder / "Wasserfall - Celeste-Fluss im Tenorio-Nationalpark, Costa Rica.jpg"
rename_file(old_file,new_file)

old_file = folder / "b4e2044926a88304da0221e66ae919c015684a901b55852be8c065a366df1136.jpg"
new_file = folder / "Tauchgondel Zingst.jpg"
rename_file(old_file,new_file)

old_file = folder / "db87f474f06f2d4eee68ee2dbbc50d3835e16553a5aa0a2a5ec6969db38fc824.jpg"
new_file = folder / "Aiguilles Rouges bei Chamonix, Haute Savoie, Frankreich.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220410_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220410_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220410_1"
folder = Path(".") / folder_name

old_file = folder / "Berg Huáng Shān 黃山, Ānhuī 安徽, China.jpg"
new_file = folder / "Vulkan Toliman, See Atitlán, Guatemala, Mittelamerika.jpg"
rename_file(old_file,new_file)

old_file = folder / "TTTTT.jpg"
new_file = folder / "Berg Huáng Shān 黃山, Ānhuī 安徽, China.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220414_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220414_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220414_1"
folder = Path(".") / folder_name

# old_file = folder / "06803b3b389ff15011281266e3e3592024389b174dc23e26610be0e3e77a5bc3.jpg"
# new_file = folder / "Messner Mountain Museum, Località Monte Rite, Cibiana di Cadore (BL), Italia.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "37a5dc662a21e2e4f39235304b188e73bc2e198a60d3cbaa712dc378bc941121.jpg"
# new_file = folder / "Claiborne Pell Newport Bridge view with rocky seascape at sunrise, Jamestown, Rhode Island, USA.jpg"
# rename_file(old_file,new_file)

# old_file = folder / "7d43f1ede76e6b02d7c0a388e951c677a2a28d0635fc8d8bb68d9b6f46bbf9f7.jpg"
# new_file = folder / "Red Rock Canyon National Conservation Area, Nevada, USA.jpg"
# rename_file(old_file,new_file)

old_file = folder / "Claiborne Pell Newport Bridge view with rocky seascape at sunrise, Jamestown, Rhode Island, USA.jpg"
new_file = folder / "Brücke - Claiborne Pell Newport Bridge view with rocky seascape at sunrise, Jamestown, Rhode Island, USA.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220419_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220419_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220419_1"
folder = Path(".") / folder_name

old_file = folder / "26ba86ac7fc2e6986b583f7ca8d9b2309ce889d940cb159f6183e36da4becb3e.jpg"
new_file = folder / "Las Vegas, Nevada, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "436f5c45947ff8c3f932bbb0ee262ec49d47fe9d2cb5de40cb4df042cdee6ca3.jpg"
new_file = folder / "Sanipass, Drakensberge, Südafrika.jpg"
rename_file(old_file,new_file)

old_file = folder / "60b8d703fad24d59b56bb85c30fb5a8b9c938626ce049b24cb188f53214bca8d.jpg"
new_file = folder / "Nationalpark Voyageurs mit Polarlichters, Ash River Visitor Center, Minnesota, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "9e02f2ca3fd46bae0bdb74e07237864d37ec6191538af955cdf1adec6fa899a2.jpg"
new_file = folder / "Wasserfall - Victoriafälle.jpg"
rename_file(old_file,new_file)

old_file = folder / "aab37e73041d6c597e769472059550be53961bbdbf889a12cdf454a186aedd67.jpg"
new_file = folder / "Bonsai Rock, Lake Tahoe, Nevada, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "d3eedb83164482c35b9bf5057a67514a6d30ccc1c43cadacc08c0526ac994779.jpg"
new_file = folder / "Nationalpark Canaima mit Tafelbergen und Carrao-Fluss bei Ucaima, Venezuela.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220422_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220422_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220422_1"
folder = Path(".") / folder_name

old_file = folder / "15b8a714d49c6315f87356cc8275654f989246672131ab942ff6ed62fc8003e4.jpg"
new_file = folder / "Nationalpark La-Maddalena-Archipel, Budelli, Sardinien, Italien.jpg"
rename_file(old_file,new_file)

old_file = folder / "60b8d703fad24d59b56bb85c30fb5a8b9c938626ce049b24cb188f53214bca8d.jpg"
new_file = folder / "Nationalpark Voyageurs mit Polarlichters, Ash River Visitor Center, Minnesota, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "e1eb6c54db7691f71a594d7aeea5051ed3225a1933b1a7b83999c9f14422adeb.jpg"
new_file = folder / "Kanincheneulen, Sublette County, Wyoming, USA.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220423_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220423_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220423_1"
folder = Path(".") / folder_name

old_file = folder / "15b8a714d49c6315f87356cc8275654f989246672131ab942ff6ed62fc8003e4.jpg"
new_file = folder / "Nationalpark La-Maddalena-Archipel, Budelli, Sardinien, Italien.jpg"
rename_file(old_file,new_file)

old_file = folder / "4d1d833aad4e8f7c51a1a2041d41d59a8fba9b7cb2f825df48b5b944670a9852.jpg"
new_file = folder / "Goldstrand (Slatni Pjasazi), Bulgarien.jpg"
rename_file(old_file,new_file)

old_file = folder / "60b8d703fad24d59b56bb85c30fb5a8b9c938626ce049b24cb188f53214bca8d.jpg"
new_file = folder / "Nationalpark Voyageurs mit Polarlichters, Ash River Visitor Center, Minnesota, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "a7909d29c491c9b53a238d672aa8796da835d55d0813b2ae3a1ab1421ddd780c.jpg"
new_file = folder / "Blaue Stunde, Brücken, Moldau, Prag, Tschechien.jpg"
rename_file(old_file,new_file)

old_file = folder / "e1eb6c54db7691f71a594d7aeea5051ed3225a1933b1a7b83999c9f14422adeb.jpg"
new_file = folder / "Kanincheneulen, Sublette County, Wyoming, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "f3de784698c40b7e1211205816e521bf637defbba3c40b5e57e351174839ad0d.jpg"
new_file = folder / "Hitachi-Küstenpar, Nemophila-Blumen, Hitachinaka, Präfektur Ibaraki, Japan.jpg"
rename_file(old_file,new_file)
```

```python hidden=true
from pathlib import Path

folder_name = "20220423_1"
folder = Path(".") / folder_name

old_file = folder / "Hitachi-Küstenpar, Nemophila-Blumen, Hitachinaka, Präfektur Ibaraki, Japan.jpg"
new_file = folder / "Hitachi-Küstenpark, Nemophila-Blumen, Hitachinaka, Präfektur Ibaraki, Japan.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220424_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220424_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220424_1"
folder = Path(".") / folder_name

old_file = folder / "3c3d5c711a4ab4b64377f018e9cae292324d7123598ca2e83098bda2f3937aea.jpg"
new_file = folder / "Nationalpark Zhāngyè (张掖) Geopark mit Dānxiá -Landschaften (丹霞), Gānsù (甘肃), China.jpg"
rename_file(old_file,new_file)

old_file = folder / "4d1d833aad4e8f7c51a1a2041d41d59a8fba9b7cb2f825df48b5b944670a9852.jpg"
new_file = folder / "Goldstrand (Slatni Pjasazi), Bulgarien.jpg"
rename_file(old_file,new_file)

old_file = folder / "8ca90ab686003c2c8d43020ebea309d60dd333bbc2f28cc3f0eff9221e580d6b.jpg"
new_file = folder / "Bay - Playa del Silencio, Cudillero, Asturien, Spanien.jpg"
rename_file(old_file,new_file)

old_file = folder / "9925418eb05335c1bcb75729faffe1be92f46bdbd33c9a3cbf63b12d6f630fdd.jpg"
new_file = folder / "Bay - Cabo de la Vela, Halbinsel Guajira, La Guajira, Riohacha, Kolumbien.jpg"
rename_file(old_file,new_file)

old_file = folder / "a7909d29c491c9b53a238d672aa8796da835d55d0813b2ae3a1ab1421ddd780c.jpg"
new_file = folder / "Blaue Stunde, Brücken, Moldau, Prag, Tschechien.jpg"
rename_file(old_file,new_file)

old_file = folder / "f3de784698c40b7e1211205816e521bf637defbba3c40b5e57e351174839ad0d.jpg"
new_file = folder / "Hitachi-Küstenpar, Nemophila-Blumen, Hitachinaka, Präfektur Ibaraki, Japan.jpg"
rename_file(old_file,new_file)
```

```python hidden=true
from pathlib import Path

folder_name = "20220423_1"
folder = Path(".") / folder_name

old_file = folder / "Hitachi-Küstenpar, Nemophila-Blumen, Hitachinaka, Präfektur Ibaraki, Japan.jpg"
new_file = folder / "Hitachi-Küstenpark, Nemophila-Blumen, Hitachinaka, Präfektur Ibaraki, Japan.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220426_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220426_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220426_1"
folder = Path(".") / folder_name

old_file = folder / "00eaa39532c263e12111cfa8ca334140afc9d5893f3da63443fbbabbabbd532f.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "097b0c6a161b2fdb42bbfbae7c0bf62936752cc0f501ab051e566e63fb078e8f.jpg"
new_file = folder / "Nationalpark Plitvicer Seen 4, Krotien.jpg"
rename_file(old_file,new_file)

old_file = folder / "10452b84469d1ee9524c4af4af705fa15caf1e7ca76ad98c0f42b0561ef58ad5.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "1a9932db03aea52ac08cf20acb418d833508e4c714475c632e1391f84a2e8143.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)

old_file = folder / "c6f2f450575eb147e82b6e622daee2e7c0bc8035b44eaeecab84a7764d4aa544.jpg"
new_file = folder / "TTTTT.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
#### May
<!-- #endregion -->

<!-- #region hidden=true -->
##### Show Wallpapers in 20220501_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220501_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220501_1"
folder = Path(".") / folder_name

old_file = folder / "10452b84469d1ee9524c4af4af705fa15caf1e7ca76ad98c0f42b0561ef58ad5.jpg"
new_file = folder / "Leuchtturm Farol da Barra in der Forte de Santo Antonio da Barra, UNESCO-Weltkulturerbe, Salvador, Bahia, Brasilien.jpg"
rename_file(old_file,new_file)

old_file = folder / "45a1cdade789a5162de9527a4eec2bb9f51a82f4b51f73cda21b28c4e3e5d019.jpg"
new_file = folder / "Bay - Scarborough Beach bei Sonnenuntergang, Südafrika.jpg"
rename_file(old_file,new_file)

# old_file = folder / "52d30ac6a10858d893b1eb96a046a0e8cbdaa979fffa20cbbc1ffd3e11d7a238.jpg"
# new_file = folder / "TTTTT.jpg"
# rename_file(old_file,new_file)

old_file = folder / "55ce5c24329914cff572756155402cc09c5f8d5e451b8ebfbcf16a964f41c891.jpg"
new_file = folder / "El Nido, Insel Matinloc, Palawan, Philippinen.jpg"
rename_file(old_file,new_file)

old_file = folder / "81713eeb901c71dff6836ed1076ba01e9aad6470cb1b42435fa1e27c966cf9f1.jpg"
new_file = folder / "Bay - Ha Long Bucht mit Kalksteinsäulen, Provinz Quang Ninh, Vietnam.jpg"
rename_file(old_file,new_file)

old_file = folder / "d44afcaaf387816c4c2e3c955301b184fb70f73a17a6941bb7cea325210efd95.jpg"
new_file = folder / "Megalithen auf der Argimusco-Hochebene, Montalbano Elicona, Sizilien, Italien.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220508_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220508_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220508_1"
folder = Path(".") / folder_name

old_file = folder / "03524bd7ed240954228c0bb4d55046c3861a758b5bd74a2a7e738e6181d04312.jpg"
new_file = folder / "Gemälde von Thomas Cole, Blick auf die Catskill Mountains im Bundesstaat New York im Frühherbst - 1837, Hudson River School.jpg"
rename_file(old_file,new_file)

old_file = folder / "4133576b5256cbd8d1e3c801445f0fc7dae14e621eece9f9c4521349dc3e614e.jpg"
new_file = folder / "Berg Zaō, Präfektur Yamagata, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "4e0cc388b882a2cef84c760282e6c4f5db02eaea8716e5bcf9a2d464ddf86662.jpg"
new_file = folder / "Reine, Lofoten, Norwegen.jpg"
rename_file(old_file,new_file)

old_file = folder / "7ada986b8aaa8d7099b8be947eb8f2f4fd21c047451f5dbf5a2e54775c30003b.jpg"
new_file = folder / "Allrad-Fahrzeuge auf unbefestigter Straße im Dschungel bei Korhogo, Elfenbeinküste.jpg"
rename_file(old_file,new_file)

old_file = folder / "8283fedb6ff838d61826f7ff3bff83fbfa8f6d46c18eccdf46585362bba2fb37.jpg"
new_file = folder / "Wasserfall - Präfektur Chiba, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "a5edc58b00c3c37eb92b2eb66446e22593efa83e1c275c0e0de3ba4148f287f8.jpg"
new_file = folder / "Hitachi Seaside Park mit kochias, Japan.jpg"
rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220513_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220513_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220513_1"
folder = Path(".") / folder_name

old_file = folder / "3ac0a12769e4c127af5de1ba63e533f47cc7062a50356ef8246855e8c1a04ff6.jpg"
new_file = folder / "Baikalsee mit Gebrochenem Eis, Insel Olchon, Sibirien, Russland.jpg"
rename_file(old_file,new_file)

old_file = folder / "476b505ef23873b0ace27a80ee545bafaa5c4b774864b2b7d052e2c4eed0e6f4.jpg"
new_file = folder / "Bay - Lloret de Mar, Girona, Spanien.jpg"
rename_file(old_file,new_file)

old_file = folder / "634134df223a8941c32e82333e1042f65f8b73a1a36c46d249abee4d4b1402ae.jpg"
new_file = folder / "Brant Point Lighthouse, Nantucket Town, , Nantucket Island, Massachusetts, New England, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "87f516735a41d92f781c05ff00d58c9f46c85e2025b5c0a1a0068c4db6794a02.jpg"
new_file = folder / "Berg Fuji und Eiszapfen im Yachonomori Park, Präfektur Yamanashi, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "bce1d17ae86a6ef6f3e6d4a5bface4dbe66fc2ad74958ea3caf776696220e795.jpg"
new_file = folder / "Grayson Lake Reservoirs bei buntem Sonnenaufgangshimmel, Kentucky, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "f7ec74c3776a6713d1dc44eb4cd951268884c663435decb8546262d135f0e88f.jpg"
new_file = folder / "Santo Antão Insel - Berglandschaft,  Kap Verde, Afrika.jpg"
rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220516_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220516_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220516_1"
folder = Path(".") / folder_name

old_file = folder / "0afa0eaf29fd77b51b86a8d002680868171203af3c0f0b886f4ac9f36ab34e7f.jpg"
new_file = folder / "Trethevy Quoit aka The Giant's House, Dolmen zwischen St Cleer und Darite in Cornwall, United Kingdom.jpg"
rename_file(old_file,new_file)

old_file = folder / "15e9e485d56a74562c8b0827103ac79348d06acb421adc0c23a03e9b2babd1f0.jpg"
new_file = folder / "Marblehead Leuchtturm auf See Erie, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "221c87459a2161b9f598b8095103a7f6914b0735b9fd9036e58f851d8b462a4b.jpg"
new_file = folder / "Bay - Küste, Buchten und Klippen am Cape Agulhas bei Arniston, Südafrika.jpg"
rename_file(old_file,new_file)

old_file = folder / "4c96b0408bf69ba8a38502ead0a56319c7c1457ad942de49423ee4da237c09ca.jpg"
new_file = folder / "Grundlsee, Steirisches Salzkammergut, Bezirk Liezen, Steiermark, Österreich.jpg"
rename_file(old_file,new_file)

old_file = folder / "4ce05edae0003bf62e50c9e64f39709f6781bba60525e5ee058b51cbc12dc357.jpg"
new_file = folder / "Brücke - Rakotzbrücke im Rhododendronpark in Gablenz-Kromlau, Sachsen, Deutschland.jpg"
rename_file(old_file,new_file)

old_file = folder / "d17563d6c19df0cc9d5662a1b88fdae3ea1eab3599877c632a8f58d2c516e098.jpg"
new_file = folder / "Holland im Winter, Niederlande.jpg"
rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220518_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220518_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220518_1"
folder = Path(".") / folder_name

old_file = folder / "0afa0eaf29fd77b51b86a8d002680868171203af3c0f0b886f4ac9f36ab34e7f.jpg"
new_file = folder / "Trethevy Quoit aka The Giant's House, Dolmen zwischen St Cleer und Darite in Cornwall, United Kingdom.jpg"
rename_file(old_file,new_file)

old_file = folder / "462a4ccb7e46262c187f98ff175ad5e6f754f4bead230d5691e069c394d0ba8d.jpg"
new_file = folder / "Mauritius mit Berg Le Morne Brabant, blauer Lagune und Unterwasserwasserfalls, Le Morne, Mauritius, Maskarenen, Indischer Ozean.jpg"
rename_file(old_file,new_file)

old_file = folder / "4c96b0408bf69ba8a38502ead0a56319c7c1457ad942de49423ee4da237c09ca.jpg"
new_file = folder / "Grundlsee, Steirisches Salzkammergut, Bezirk Liezen, Steiermark, Österreich.jpg"
rename_file(old_file,new_file)

old_file = folder / "4ce05edae0003bf62e50c9e64f39709f6781bba60525e5ee058b51cbc12dc357.jpg"
new_file = folder / "Brücke - Rakotzbrücke im Rhododendronpark in Gablenz-Kromlau, Sachsen, Deutschland.jpg"
rename_file(old_file,new_file)

old_file = folder / "68689b02479f103ed6f7dd14e34890fc59046ccc66847c71cc198e9b227ff340.jpg"
new_file = folder / "Hokkaido, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "c5827f7a5f4a5ba9c10cba9ca949b2861e2961e5cdb21cfae9cf1b2fb68fbd13.jpg"
new_file = folder / "Sumatras Nachbararchipel, Aceh, Indonesien.jpg"
rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

```python hidden=true
from pathlib import Path

folder_name = "20220518_1"
folder = Path(".") / folder_name

old_file = folder / "Mauritius mit Berg Le Morne Brabant, blauer Lagune und Unterwasserwasserfalls, Le Morne, Mauritius, Maskarenen, Indischer Ozean.jpg"
new_file = folder / "Mauritius mit Berg Le Morne Brabant, blauer Lagune und Unterwasserwasserfall, Le Morne, Mauritius, Maskarenen, Indischer Ozean.jpg"
rename_file(old_file,new_file)
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220521_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220521_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220521_1"
folder = Path(".") / folder_name

old_file = folder / "10452b84469d1ee9524c4af4af705fa15caf1e7ca76ad98c0f42b0561ef58ad5.jpg"
new_file = folder / "Leuchtturm Farol da Barra in der Forte de Santo Antonio da Barra, UNESCO-Weltkulturerbe, Salvador, Bahia, Brasilien.jpg"
rename_file(old_file,new_file)

old_file = folder / "2df94cbbd25dd8946fc62843f7f4d5f44346b283d448cd50beeca92ceff46041.jpg"
new_file = folder / "Tour De La Parata und Sanguinaires Islands Inseln, Sanguinaires Islands, Korsika, Frankreich.jpg"
rename_file(old_file,new_file)

old_file = folder / "458892381c6ca401f05d31e12f25ceb232af05ed2b68934e5850646b0d5fff5e.jpg"
new_file = folder / "Nationalpark Ordesa mit Wänden von Punta Galinero und Cotatuero Wasserfall, Pyrenäen, Aragon, Spanien.jpg"
rename_file(old_file,new_file)

old_file = folder / "45a1cdade789a5162de9527a4eec2bb9f51a82f4b51f73cda21b28c4e3e5d019.jpg"
new_file = folder / "Bay - Scarborough Beach bei Sonnenuntergang, Südafrika.jpg"
rename_file(old_file,new_file)

old_file = folder / "55ce5c24329914cff572756155402cc09c5f8d5e451b8ebfbcf16a964f41c891.jpg"
new_file = folder / "El Nido, Insel Matinloc, Palawan, Philippinen.jpg"
rename_file(old_file,new_file)

old_file = folder / "e14d6c53aac725646155104453347a1bdac3ec6150911ec427f6fd37645fa3bf.jpg"
new_file = folder / "Schreikraniche in Aransas National Wildlife Refuge.jpg"
rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220526_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220526_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220526_1"
folder = Path(".") / folder_name

old_file = folder / "10452b84469d1ee9524c4af4af705fa15caf1e7ca76ad98c0f42b0561ef58ad5.jpg"
new_file = folder / "Leuchtturm Farol da Barra in der Forte de Santo Antonio da Barra, UNESCO-Weltkulturerbe, Salvador, Bahia, Brasilien.jpg"
rename_file(old_file,new_file)

old_file = folder / "2cc5a71fb4d6a588ce8193c1a10c1c6b2c5f24edba72708866e14dee04bcb676.jpg"
new_file = folder / "Sieben Stier-Felsen, Jeti-Ögüz Bezirk, Issyk-Kul Region, Kirgisistan.jpg"
rename_file(old_file,new_file)

old_file = folder / "49c316f77a3ab5c69155faaa8ae740b1ee8018baa9e330b4c72c925797d4c215.jpg"
new_file = folder / "Mittelamerikanisches Barriereriff mit westindischem Seestern, Küste von Belize.jpg"
rename_file(old_file,new_file)

old_file = folder / "94cdbe43dbf1cddff5df12f83a51b8913d4164c1dd016e60c981ec833143c997.jpg"
new_file = folder / "Tabarin Halbinsel mit Tuff Klippen des Brown Bluff, Weddellmeer, Antarktis.jpg"
rename_file(old_file,new_file)

old_file = folder / "989e50e85ed1b27a7fa653244818a03ff85ed49da3f858439fd78101bf1109f0.jpg"
new_file = folder / "Luxor, Ägypten.jpg"
rename_file(old_file,new_file)

old_file = folder / "a5edc58b00c3c37eb92b2eb66446e22593efa83e1c275c0e0de3ba4148f287f8.jpg"
new_file = folder / "Hitachi-Park mit Besen-Radmelde, Japan.jpg"
rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
#### June
<!-- #endregion -->

<!-- #region hidden=true -->
##### Show Wallpapers in 20220602_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220602_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220602_1"
folder = Path(".") / folder_name

old_file = folder / "19d52c1968b7e5e04f045b945291411b442809df5929617138dd9229b9725571.jpg"
new_file = folder / "Nationalpark La Jacques-Cartier, Quai des Rabascas du Pont-Banc.jpg"
rename_file(old_file,new_file)

old_file = folder / "221e9aba418808eda5a651bde96864438de4d00d39a499740f7b36575a58498c.jpg"
new_file = folder / "Karthagos antike Überreste vom Hügel Byrsa aus, Tunis, Tunesien.jpg"
rename_file(old_file,new_file)

old_file = folder / "29c02991155e06500996c4f338ac9c6a60dbad069a63bd90dd0960972f4fa105.jpg"
new_file = folder / "Ponta de São Lourenço, Madeira, Portugal.jpg"
rename_file(old_file,new_file)

old_file = folder / "65b39877473d481299261f725c321c7c43733c877eab3cb1069eddd2c58fe079.jpg"
new_file = folder / "Tourlitis Leuchtturm in Chora auf Andros, Kykladen, Griechenland.jpg"
rename_file(old_file,new_file)

old_file = folder / "7632dfd93cd53b51e7438590c66f471c3563af602c6e1d8935c6ff62b4eaaaff.jpg"
new_file = folder / "Kaiserlicher Park Shinjuku, Tokio, Japan.jpg"
rename_file(old_file,new_file)

old_file = folder / "d44afcaaf387816c4c2e3c955301b184fb70f73a17a6941bb7cea325210efd95.jpg"
new_file = folder / "Megalithen auf der Argimusco-Hochebene, Montalbano Elicona, Sizilien, Italien.jpg"
rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220605_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220605_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


```python hidden=true
from pathlib import Path

folder_name = "20220605_1"
folder = Path(".") / folder_name

old_file = folder / "19d52c1968b7e5e04f045b945291411b442809df5929617138dd9229b9725571.jpg"
new_file = folder / "Nationalpark La Jacques-Cartier, Quai des Rabascas du Pont-Banc.jpg"
rename_file(old_file,new_file)

old_file = folder / "30d0876cee40893f28a3cb6730fd88d488e3e49c8e149e6f068771744fa747fc.jpg"
new_file = folder / "Bangaan in den Reisterrassen von Banaue, nördlichen Luzon, Philippinen.jpg"
rename_file(old_file,new_file)

old_file = folder / "45a1cdade789a5162de9527a4eec2bb9f51a82f4b51f73cda21b28c4e3e5d019.jpg"
new_file = folder / "Bay - Scarborough Beach bei Sonnenuntergang, Südafrika.jpg"
rename_file(old_file,new_file)

old_file = folder / "8d7b58cce8671b45cd30be2544fe8d378a524c6229b40044264736a158e4ac68.jpg"
new_file = folder / "Lima von Miraflores zur blauen Zeit im Luftbild, Lima, Peru.jpg"
rename_file(old_file,new_file)

old_file = folder / "d568608f57e44d07fb49a1218dc1b2526129f7c546b62d1a7758b22c993661e5.jpg"
new_file = folder / "Anza-Borrego Desert State Park im Frühling, Kalifornien, USA.jpg"
rename_file(old_file,new_file)

old_file = folder / "df1a79f931aff13c549abb21a68fcf9c9457b521636056ecf1be70197061b461.jpg"
new_file = folder / "Waiʻaleʻale, Kauaʻi, Hawaiʻi.jpg"
rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220607_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220607_1"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.reduce()
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region hidden=true -->
##### Show Wallpapers in 20220609_8/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection(True)
folder_name = "20220609_8"
if collection.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

```python hidden=true
old_file = "6e7831016ce666b65ee13cc749c22909fe63d55c87f2ae04655f793de020f872.jpg"
new_file = "Channel Country, v. A.  Queensland, Australiens.jpg"
collection.rename_file(old_file,new_file)

old_file = "ba2202f47e455833e70fe6d328846cd118956b533669200001fcf0d1e37f6915.jpg"
new_file = "Bay - Strand Praia Ursa mit Steinformationen, Sintra, Portugal.jpg"
collection.rename_file(old_file,new_file)

old_file = "cd2035be101dacf78a8405b6637c2befeab2a83d08bfbc15b2ea2d70989fde11.jpg"
new_file = "Vestrahorn Beach, Batman Mountain, Island.jpg"
collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220615_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220615_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

```python hidden=true
old_file = "2608cd41d1055504897310eb0258d52dce3d832d1ac8fb09a16b30df5f15281d.jpg"
new_file = "Clutha River, Südinsel, Neuseeland.jpg"
collection.rename_file(old_file,new_file)

old_file = "7abb99cd91337d29dc4d120a3eaf2f00feb305a94af82dd5e1f36af93835addc.jpg"
new_file = "Mirissa, Matara, Southern Province, Sri Lanka.jpg"
collection.rename_file(old_file,new_file)

old_file = "8752040e33b4a624e774ff47033bd449e6f3ea707ae3c09df908056aed87062c.jpg"
new_file = "Königspinguine in der Royal Bay von Südgeorgien, Antarktis.jpg"
collection.rename_file(old_file,new_file)

old_file = "8b7a9508c4aecbe67fb699a2c145f3ab86e92000db5b769a092ffbaf7dc26a91.jpg"
new_file = "Berg Wildseeloder, Tirol, Österreich.jpg"
collection.rename_file(old_file,new_file)

old_file = "8bf6ef77c2cf270488a155378ac90d77c348fde3e02829f86cea9aa34021b7ce.jpg"
new_file = "Kelingking Strand, Nusa Penida, Bali, Indonesien.jpg"
collection.rename_file(old_file,new_file)

old_file = "f2f70cf5eacab4f248cd56a2119a642f2fb447f2d0747733f399778421d4d0a1.jpg"
new_file = "Rote Heidelbeeren im Nationalpark Mount Rainier, Washington, USA.jpg"
collection.rename_file(old_file,new_file)



print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220622_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220622_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

```python hidden=true
old_file = "42cbe3e93f69f8faa3624cae80d1e88c7b909765ff858cb0e20050be68377b1d.jpg"
new_file = "Miradouro da Boca do Inferno, São Miguel, Açores, Portugal.jpg"
collection.rename_file(old_file,new_file)

old_file = "44ea2f944866a7e940ca6ef37c0370f2781d473a68ba91715d35808427689fca.jpg"
new_file = "Sigiriya (Löwenfelsen), UNESCO Weltkulturerbe, Sri Lanka.jpg"
collection.rename_file(old_file,new_file)

old_file = "45a1cdade789a5162de9527a4eec2bb9f51a82f4b51f73cda21b28c4e3e5d019.jpg"
new_file = "Bay - Scarborough Beach bei Sonnenuntergang, Südafrika.jpg"
collection.rename_file(old_file,new_file)

old_file = "5f21bd69ab0db1ae82d4b94077b13d23fe2f945faf81cf03fd6ca4b2f278f331.jpg"
new_file = "Bay - Gamboa Strand, Ilha de Tinhare, Bahia, Brasil.jpg"
collection.rename_file(old_file,new_file)

old_file = "a600a1ca1655eb331d303fa162534d6baed6a4247cb3c476aadb708d09cded77.jpg"
new_file = "Spiegelrei und das Spinolarei in Brügge, Belgien.jpg"
collection.rename_file(old_file,new_file)

old_file = "f99573e434b9e49a30a16efcfaeb313aa1f78796163859fa8af60ed30f8bf0da.jpg"
new_file = "Essens Pfad unter schiefen Bäumen über See, Nordrhein-Westfalen, Deutschland.jpg"
collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220629_2/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220629_2"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

```python hidden=true
old_file = "03524bd7ed240954228c0bb4d55046c3861a758b5bd74a2a7e738e6181d04312.jpg"
new_file = "Gemälde von Thomas Cole, Blick auf die Catskill Mountains im Bundesstaat New York im Frühherbst - 1837, Hudson River School.jpg"
collection.rename_file(old_file,new_file)

old_file = "4f95dcbd4b1a010d72aa2351df4d907b728f3c1fecfff32e0e0bf29afd39cde9.jpg"
new_file = "Hachijō-jimas Insellandschaft, Izu-Inseln, Japan.jpg"
collection.rename_file(old_file,new_file)

old_file = "94cdbe43dbf1cddff5df12f83a51b8913d4164c1dd016e60c981ec833143c997.jpg"
new_file = "Tabarin Halbinsel mit Tuff Klippen des Brown Bluff, Weddellmeer, Antarktis.jpg"
collection.rename_file(old_file,new_file)

old_file = "dc134d96990d839dc8f5b4b38644273466fceb55b0d7e0955b725b4f2949ec0c.jpg"
new_file = "Ubatuba (Município da Estância Balneária), São Paulo, Brasilien.jpg"
collection.rename_file(old_file,new_file)

old_file = "e3a03676cbba99ed276c45451a4e5828dffa52258ab51b111c5dc4cf03f8ddd1.jpg"
new_file = "Leuchtturm Farol da Barra in der Forte de Santo Antonio da Barra, UNESCO-Weltkulturerbe, Salvador, Bahia, Brasilien.jpg"
collection.rename_file(old_file,new_file)



print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
#### July
<!-- #endregion -->

<!-- #region hidden=true -->
##### Show Wallpapers in 20220703_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220703_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

```python hidden=true
old_file = "18f11fc28cb1f75157cbd72795452aa50ccf3376743602df34e6693d2270c8be.jpg"
new_file = "Mount Magazine State Park in den Ozark Mountains, Arkansas, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "a1e25d2c4db7adeb3233eb8dca661a77dbc1dab8305112d89156c88833681c38.jpg"
new_file = "Nationalpark - Agave-Kaktus in Chisos Mountains, Chihuahua-Wüste, Big Bend National Park, Texas, USA.jpg"
collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

```python hidden=true
old_file = "Campingplatz, Geiranger, Norwegen.jpg"
new_file = "Geiranger, Norway.jpg"
collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220705_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220705_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

```python hidden=true
old_file = "1e73073a88891bc5b80bddcf22438d994fea336c6902ea3617fa7f6e191502e1.jpg"
new_file = "Ecola State Park, Oregon, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "29c02991155e06500996c4f338ac9c6a60dbad069a63bd90dd0960972f4fa105.jpg"
new_file = "Ponta de São Lourenço, Madeira, Portugal.jpg"
collection.rename_file(old_file,new_file)

old_file = "45a1cdade789a5162de9527a4eec2bb9f51a82f4b51f73cda21b28c4e3e5d019.jpg"
new_file = "Bay - Scarborough Beach bei Sonnenuntergang, Südafrika.jpg"
collection.rename_file(old_file,new_file)

old_file = "553e4190c69295e069beaf22980683472198ba0df2c226a5b00eb7f31aa352d2.jpg"
new_file = "Tempel des Poseidon am Kap Sounion in der Ägäis, Griechenland.jpg"
collection.rename_file(old_file,new_file)

old_file = "95f2eddbeb2c82699c0e28a28df7fe3e371f0abed467b43d16479c6fb100f851.jpg"
new_file = "Essaouira (ehem. Mogador) um UNESCO Weltkulturerbe Altstadt Medina, Essaouira, Marokko.jpg"
collection.rename_file(old_file,new_file)

old_file = "e98e801e564ae61e5d9d7731d4e3a238b8a65adfb9dca09460bdfa9c8c2d25fe.jpg"
new_file = "Kalalau-Tal im Nā Pali Coast State Park, Kauaʻi, Hawaiʻi, United States.jpg"
collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

```python hidden=true
old_file = "Ecola State Park, Oregon, USA.jpg"
new_file = "Ecola State Park (2), Oregon, USA.jpg"
collection.rename_file(old_file,new_file)
```


<!-- #region hidden=true -->
##### Show Wallpapers in 20220708_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220708_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

```python hidden=true
old_file = "783eb6c6adfa7c216fa61bffd13d03b5d3b0436b2dc05fbabd745b672746ad02.jpg"
new_file = "Geiranger, Norway.jpg"
collection.rename_file(old_file,new_file)

old_file = "7ee1840bff3cf2aae9a16d71dbfae373b24efc7e00eae198466bedcc7e2fc254.jpg"
new_file = "Amalfi, Neapel, Italien.jpg"
collection.rename_file(old_file,new_file)

old_file = "dc9c7d30e68bb3105f7405ad40065acd47a02d7562cd58881e06896bb0b75606.jpg"
new_file = "Derwentwater, Lake District, England, UK.jpg"
collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220716_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220716_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

```python hidden=true
old_file = "06803b3b389ff15011281266e3e3592024389b174dc23e26610be0e3e77a5bc3.jpg"
new_file = "Messner Mountain Museum, Località Monte Rite, Cibiana di Cadore (BL), Italia.jpg"
collection.rename_file(old_file,new_file)

old_file = "736603bbe486b6d3e85c339ece8c527be84f31a86d1593deec066158295d9482.jpg"
new_file = "Dead Horse Point State Park, Utah, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "e0716c062ea4e234626e41e2fc0921a674b04b99d4be7449f81ed876a8b04d3c.jpg"
new_file = "Geparden im Savannenfeld Maasai Mara National Park, Kenia.jpg"
collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220721_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220721_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "01181c83b9ec3827e2b069279a0367b0e71beef69f865bcb61150dc06ea4a960.jpg"
new_file = "Brücke - Marius Gontard Brücke und Seilbahn in Grenoble, Dépt 38 Isère, Frankreich.jpg"
collection.rename_file(old_file,new_file)

old_file = "29c02991155e06500996c4f338ac9c6a60dbad069a63bd90dd0960972f4fa105.jpg"
new_file = "Ponta de São Lourenço, Madeira, Portugal.jpg"
collection.rename_file(old_file,new_file)

old_file = "c5b6ca6296e0687629a516001ecd746c4990400d131c27b0127b4766f75587e2.jpg"
new_file = "bipolare Sternenwiege  Sharpless 2-106, Hubble Teleskop.jpg"
collection.rename_file(old_file,new_file)

old_file = "eb1725abc07e895d88dd4eb9af4aa8df13760673169c10c57c52bb396fdf632e.jpg"
new_file = "Maui, Hawai'i, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "f6f35467f0e6c0fd709097bce69ca2f23d3391a33667a5db1f567c62fbdb463b.jpg"
new_file = "Strandläufer, ruht auf Bottle Beach während der Frühjahrswanderung, Washington, USA.jpg"
collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

```python hidden=true
old_file = "bipolare Sternenwiege  Sharpless 2-106, Hubble Teleskop.jpg"
new_file = "Astronomie - bipolare Sternenwiege Sharpless 2-106, Hubble Teleskop.jpg"
collection.rename_file(old_file,new_file)
```


<!-- #region hidden=true -->
##### Show Wallpapers in 20220723_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220723_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "116abda59844a4565d84fbdbd1a86f357778bc111267152e63ce758113981e35.jpg"
new_file = "Jardim Botanico mit Seerosen, Rio de Janeiro, Brasilien .jpg"
collection.rename_file(old_file,new_file)

old_file = "3714499fe5539a79e5f55baf21acae8e474f29601085be9791908047d51f001c.jpg"
new_file = "Gilf el-Kebir, Ägypten.jpg"
collection.rename_file(old_file,new_file)

old_file = "7d43f1ede76e6b02d7c0a388e951c677a2a28d0635fc8d8bb68d9b6f46bbf9f7.jpg"
new_file = "Red Rock Canyon National Conservation Area, Nevada, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "7ee1840bff3cf2aae9a16d71dbfae373b24efc7e00eae198466bedcc7e2fc254.jpg"
new_file = "Amalfi, Neapel, Italien.jpg"
collection.rename_file(old_file,new_file)

old_file = "a890b25c518dcc8c2e3f6ab5bfe63a3cb0602210d5236061bc9a30f3c076fa5e.jpg"
new_file = "Taba, Süd-Sinai, Ägypten.jpg"
collection.rename_file(old_file,new_file)

old_file = "b33bb59be796e9dadb77ee15ae271cc42c6a126b43f15e70c74a53b96904d39c.jpg"
new_file = "Verschiedene Eiscremearomen auf rustikalem Hintergrund, Sommer und süßes kaltes Eis.jpg"
collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220730_6/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220730_6"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "12311eb7f7df7461655fb99b5e4d880e3d145bc3112cdb9ba99ba8340f0cce82.jpg"
new_file = "Glastonbury Tor mit Stufen, Glastonbury, Somerset, England, United Kingdom.jpg"
collection.rename_file(old_file,new_file)

old_file = "7eb6a5950e66e5023e36a1f14686135a47ab2b948330ee1c185a8adae6470446.jpg"
new_file = "Drachenbrücke über den Fluss Hàn in Da Nang bei Nacht, Vietnam.jpg"
collection.rename_file(old_file,new_file)

old_file = "828a64955b851275c4ff6f54d8ea1628e9b089cb4c04227ce7ef79bc052e35bd.jpg"
new_file = "Moschusochsen (Ovibos Moschatus) Herde in der arktischen Tundra, Seward-Halbinsel, Alaska, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "ac0d7c8243c8ca7e0ecb278fa90074a9c639d5cf18ce067472b126fcff12ee4b.jpg"
new_file = "Tepuy Kukenán, Bolívar, Venezuela.jpg"
collection.rename_file(old_file,new_file)

old_file = "f557bf8b5cdbf145c190f2f2f9400d5d8c5c895b607750b04742683f62fc3a90.jpg"
new_file = "Mykonos, Kykladen, Griechenland.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Karawanenroute in der Nähe der Oase Dakhla, libysche Wüste, Sahara, Ägypten.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
#### August/
<!-- #endregion -->

<!-- #region hidden=true -->
##### Show Wallpapers in 20220805_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220805_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
# old_file = "Alien Throne Felsformation kurz nach Sonnenuntergang, Bisti De-Na-Zin Wildnis, New Mexico, USA.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

old_file = "eb804a5d3c6cf4fa363dddf1211882a8f2b1aa7e986e64eee8f4338d98c3d769.jpg"
new_file = "Reisterrassen im Distrikt Tegallalang, Bali, Indonesia .jpg"
collection.rename_file(old_file,new_file)

# old_file = "Essens Pfad unter schiefen Bäumen über See, Nordrhein-Westfalen, Deutschland.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

old_file = "fefaae5bc8913d4bd35ee8add09ced2738eb365d504096c47fc08947e0416c48.jpg"
new_file = "Midagahara-Feuchtgebiete in der Nähe von Tateyama in der Präfektur Toyama, Japan.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Grayson Lake Reservoirs bei buntem Sonnenaufgangshimmel, Kentucky, USA.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220807_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220807_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "02fca3eee5d37ce38d1420fa2eeccd80bc918d9a602f6d33f8c4c004658f2913.jpg"
new_file = "Mangrovenwald und Fluss auf der Insel Siargao, Philippinen.jpg"
collection.rename_file(old_file,new_file)

old_file = "369a01dc8a184fc0ce4615dca6ac5a7755a162eb807fbb613845e44b367aa580.jpg"
new_file = "Berg Teurafaatiu, Maupiti, Französisch-Polynesien, Frankreich.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Bellagio - Perle des Comer Sees, Italien.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Wasserfall - Celeste-Fluss im Tenorio-Nationalpark, Costa Rica.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220813_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220813_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "01fe7d49e20a3936896924062a504c837cfe08d54915b8390fa359074a75441d.jpg"
new_file = "Delaware River zwischen Pennsylvania und New Jersey während der Laubsaison, USA.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Blick vom Goodnow Mountain, Adirondack Mountains, New York State, USA.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Dead Horse Point State Park, Utah, USA.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

old_file = "e04c0d0250f35e7eabafffcd6c985a92de3ecefe41f4d0d7da25ffe9d2efd289.jpg"
new_file = "Herefordshire, West Midlands, England.jpg"
collection.rename_file(old_file,new_file)

old_file = "f8504b522a9c6ec71355c3bb99f8acc18569ff30504f47d15a18abfa0416183e.jpg"
new_file = "Nasca Wüste mit weißer Düne bei Nasca, Nasca, Peru.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Sahara, Dünen nahe Douz, Kebili, Tunesien.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20220829_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220829_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "208953f4f617da37bae76e8b0824977f1f0f899e03000df179dd66e32717cf61.jpg"
new_file = "Marmor-Höhlen, General Carrera-See, Puerto Tranquilo, Region Aysén, Chile.jpg"
collection.rename_file(old_file,new_file)

old_file = "447112c727ff4c671923956ba4880c940c51fad5961a7926a9bb2f6dcb1071a4.jpg"
new_file = "Wasserfall - Kuang-Si-Wasserfall mit Sinterterrassen, Provinz Luang Prabang, Laos.jpg"
collection.rename_file(old_file,new_file)

old_file = "e605dbbee3f37a10d2d110ccf608a16fa01aebec5f35dc5e735de39da6890001.jpg"
new_file = "Badland-Landschaft mit Butte, Theodore-Roosevelt-Nationalpark, Medora, North Dakota, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "f2d950e15b8c854979f1149b68f941ce5ac62d09f7cb83f94b648b3bcb2b9dd8.jpg"
new_file = "Wasserfall - Springbrook-Nationalpark, Queensland, Australien.jpg"
collection.rename_file(old_file,new_file)

old_file = "f5079bd8bb30aa87d9edfcb8aa410091a69eccd82ed7fee8f67c75e49fc2c606.jpg"
new_file = "Mussenden-Tempel bei Castlerock, Londonderry, Nordirland, UK.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Santorini mit Meerblick, weißer Architektur und rosa Blumen, Griechenland.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
#### September/
<!-- #endregion -->

<!-- #region hidden=true -->
##### Show Wallpapers in 20220913_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20220913_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "76b458321c1d81f5459c160340da356f5c989bd57630ba4a61de20f6c999a346.jpg"
new_file = "Nationalpark - Peak-District-Nationalpark, Derbyshire, England.jpg"
collection.rename_file(old_file,new_file)

old_file = "9208ef0f3e6590dbafa0739009a6bb06e15a64e46424e796d74fabc29f57daf3.jpg"
new_file = "Bucht von Kotor, Montenegro.jpg"
collection.rename_file(old_file,new_file)

old_file = "c6f2f450575eb147e82b6e622daee2e7c0bc8035b44eaeecab84a7764d4aa544.jpg"
new_file = "El Arco de Cabo San Lucas, Baja California, México.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Gemälde Lake George von John Frederick Kensett.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Half Dome, Yosemite-Nationalpark, Kalifornien, USA.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Sumatras Nachbararchipel, Aceh, Indonesien.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
#### October/
<!-- #endregion -->

<!-- #region hidden=true -->
##### Show Wallpapers in 20221001_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221001_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
###### renaming
```

<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

<!-- #region hidden=true -->
##### Show Wallpapers in 20221002_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221002_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "810d4c989ef2a9f8c83d98d44284de0c38ccdaa0c9b87096734b2c7ce9c8c0ca.jpg"
new_file = "TTTTT.jpg"
collection.rename_file(old_file,new_file)

old_file = "c3a4ffc452d70162d61684741db166395d975ad3b0839e15b60dab23eb45f9dc.jpg"
new_file = "TTTTT.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Gemälde Shandaken Mountains von Asher Brown Durand, Metropolitan Museum of Art (Met), New York, USA.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Inka-Brücke Puente del Inca bei Sonnenuntergang, zentrale Anden, Provinz Mendoza, Argentinien.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Nationalpark Cotopaxi, Ecuador.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Nationalpark Denali mit Karibu-Stier vor dem Mount McKinley, Alaska, USA.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20221009_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221009_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "192fc9f7e1963e2fd275d35030c1bf6430da4fcb702a2641d19c3c0d8708c459.jpg"
new_file = "Nationalpark Unesco Weltnaturerbe Wǔlíngyuán (武陵源), Zhāngjiājiè (张家界市), Húnán (湖南省), China.jpg"
collection.rename_file(old_file,new_file)

old_file = "3aae6c26dc27353bcdb9ad54214b3a57ecdd74a0d875273a24469ff489c3dad8.jpg"
new_file = "Kilimandscharo (Tansania) durch Akazien im Amboseli-Nationalpark, Kajiado County, Kenia.jpg"
collection.rename_file(old_file,new_file)

old_file = "f7182dc3c17d55bfa180fdff7bfb1b02d0d3e1d34cada88f99fc70cb9a75b23c.jpg"
new_file = "Assalsee mit Salzkristallen, Region Tadjoura, Dschibuti.jpg"
collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20221011_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221011_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "34de00544fa421472021e409430c19ca2174ffd8277362ac4c1847ef4338fa7e.jpg"
new_file = "Pianiseen nahe der Antonio Locatelli Hütte, Naturpark Drei Zinnen, Sextener Dolomiten, Südtirol, Italien.jpg"
collection.rename_file(old_file,new_file)

old_file = "a642d0ae9b62d1b3be9efd2a50acfd090242ccefe4be6607554f28c1d7b04f60.jpg"
new_file = "Seimon Ishibashi Brücke am Imperial Palace in Tokyo, Japan.jpg"
collection.rename_file(old_file,new_file)

old_file = "e57e214de6c733004a72c8eda8d71c7e6db1e3e8880581feddff908a109e075a.jpg"
new_file = "Havasu Falls of Havasu Creek, Havasupai Indian Reservation im Grand Canyon National Park, Arizona, USA.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Hill Country RV Park, Kerrville and Kerr County, Texas.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Nationalpark - Peak-District-Nationalpark, Derbyshire, England.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Nationalpark Unesco Weltnaturerbe Wǔlíngyuán (武陵源), Zhāngjiājiè (张家界市), Húnán (湖南省), China.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20221014_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221014_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "9ac544497eced0f24650fa3f0526790680ffcf9e096551a6284c03118ff76d8f.jpg"
new_file = "Reims, Frankreich.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Dead Horse Point State Park, Utah, USA.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

old_file = "e693c8731a84ac6729ad673edc3aafa4469b2579ee7bde65d0b5d8ffcfb7e83d.jpg"
new_file = "Olmoti -Krater im Naturschutzgebiet Ngorongoro, Arusha, Tansania.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Hill Country RV Park, Kerrville and Kerr County, Texas.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Metropolregion.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Spiegelrei und das Spinolarei in Brügge, Belgien.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20221015_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221015_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "1396be3ecc05519efd2715760d085f8ac69ca653f5b41aaf6fff721b8b64b965.jpg"
new_file = "Yamanakasee und Berg Fuji, Präfektur Yamanashi, Japan.jpg"
collection.rename_file(old_file,new_file)

old_file = "f8c652619172d31c9c3b8f1f7fe59a39df8f5a4788a892a1bea9c9d68d71c836.jpg"
new_file = "Santa Croce und Vinci, Toskana, Italien.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Hill Country RV Park, Kerrville and Kerr County, Texas.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Humantaysee am Salkantay Trek nach Machu Picchu, Provinz Cusco, Peru.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Reims, Frankreich.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Spiegelrei und das Spinolarei in Brügge, Belgien.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
#### November/
<!-- #endregion -->

<!-- #region hidden=true -->
##### Show Wallpapers in 20221105_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221105_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "0f74186b15b521f6ba72ad3b6e715d2e7694592352784f7b9a6f2bf606f8cb5b.jpg"
new_file = "Head Harbour Lightstation lighthouse, Campobello Island, Charlotte County, New Brunswick, Kanada.jpg"
collection.rename_file(old_file,new_file)

old_file = "168ca10acdd24f14dbd9bf2f89996155bbba3e4b859070c7551675f718a174d4.jpg"
new_file = "Seelöwen am Strand von Playa Mann, Isla San Cristobal, Galapagos-Inseln, Ecuador.jpg"
collection.rename_file(old_file,new_file)

old_file = "1b7eb37435c95f3f959738ffeddf7d464f5fb44e6fe91f9d7539a73c902cf9e4.jpg"
new_file = "Küste mit Palmen einer Insel der Republik Malediven.jpg"
collection.rename_file(old_file,new_file)

old_file = "af5e7892663f3c480be30dcaa81c33f03e0e283bfbe6af6678618fed17fdd539.jpg"
new_file = "Ponte dei Salti über die Verzasca in Lavertezzo, Tessin, Schweiz.jpg"
collection.rename_file(old_file,new_file)

old_file = "e1104f2af42ce9029b755f31c83359b45fb2ebd26965aaa6f248ab5a5e193780.jpg"
new_file = "Chinesische Mauer Jinshanling (金山岭长城), Peking, China.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Messner Mountain Museum, Località Monte Rite, Cibiana di Cadore (BL), Italia.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20221109_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221109_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "2706a0e6e830e5203d989fd1ecef2a9b81705a99a731d594ee6ca7b0cfc71782.jpg"
new_file = "Nizza, Côte d'Azur, Frankreich.jpg"
collection.rename_file(old_file,new_file)

old_file = "c72f616970d550dc3b0027dd8975f440588eb1d19d11e240a1a3379eeba1f76d.jpg"
new_file = "Nationalpark Zion mit roten Höhlen, Utah, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "d9f1945c462dbf5ffc978f6eab93928c0c7c1aeb7fd6acfb4a96e53598f75df6.jpg"
new_file = "Kap Innojofuta, Insel Tokunoshima, Amami-Guntō-Nationalpark, Präfektur Kagoshima, Japan.jpg"
collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20221114_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221114_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "1dd3f4e99aac6de24ff8cb2eb01afcaa363bd81e2ab5f205f4987239947a48b6.jpg"
new_file = "Bay - Hạ Long-Bucht von der Insel Cát Bà, Vietnam.jpg"
collection.rename_file(old_file,new_file)

old_file = "3c7578f6605ec582c5e44d72bcde960d434f44b7f153bafe001462e0bcb33eba.jpg"
new_file = "Reschensee unter dramatischem Himmel bei Sonnenuntergang, Südtirol, Italien.jpg"
collection.rename_file(old_file,new_file)

old_file = "a5a150e117aa7cdd1c54cfcd56abc340e4c80d692633b8cfdae634cc79283707.jpg"
new_file = "Eiði mit Schafen und Straße am Meer, Eysturoy, Färöer-Inseln.jpg"
collection.rename_file(old_file,new_file)

old_file = "bf2eea096d43f22ff7169a8b48dad2486080978c0150a63ecdbfc7b5e42024f4.jpg"
new_file = "Wasserfall Havasu Falls, Havasupai Indian Reservation im Grand Canyon National Park, Arizona, USA.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Wasserfall - Celeste-Fluss im Tenorio-Nationalpark, Costa Rica.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Zebras Familie, Tansania. Afrika.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
##### Show Wallpapers in 20221127_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221127_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "39b2412803ac1432781c9ec2d9742d06f758b3a88cbb3824b539356b64bb4556.jpg"
new_file = "TTTTT.jpg"
collection.rename_file(old_file,new_file)

old_file = "77f23b6b7aca2d52f330383b70e97812f64d9a4d09e38a5c1f6b4784d2b03ac1.jpg"
new_file = "TTTTT.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Wasserfall - Präfektur Chiba, Japan.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
#### December/
<!-- #endregion -->

<!-- #region heading_collapsed=true hidden=true -->
##### Show Wallpapers in 20221201_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221201_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "0d0992756c0b783e91779f17e6f83b6833b671659ff56d7c7a1ae2ba782dd4f4.jpg"
new_file = "Brücke - Manhattan Bridge über den East River, New York City, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "2e245d235f775f9323a522d0fa7af2773740ccdf6d61e1efec8562ee691ab31d.jpg"
new_file = "Nationalpark Yoshino-Kumano mit Hashi-gui-iwa-Felsen, Kushimoto, Wakayama, Japan.jpg"
collection.rename_file(old_file,new_file)

old_file = "42f7338a3098585e3f85a1dbd00342885c4b1d63b1db785e096fefa5c06d99d9.jpg"
new_file = "Weihnachtsbaum auf dem Weinberg mit Watzmann, Plateau Steinernes Meer, Bayern, Deutschland.jpg"
collection.rename_file(old_file,new_file)

old_file = "7cb0d1143787c729b616d3b6957981602e607b55a272179e91b83f6855268d08.jpg"
new_file = "Roms Ruinen, Italien.jpg"
collection.rename_file(old_file,new_file)

old_file = "ca929849de9bd101af75f1587a6790f85303164aab840bd5abaa865c10e19224.jpg"
new_file = "Nationalpark Tasmanien - Kap Raoul, Tasmanien, Australien.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Zebras Familie, Tansania. Afrika.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
##### Show Wallpapers in 20221204_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221204_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "3938c9c5f61758970fa190e9b8e91df51fd9585aa787f5772e2e949347ee0cf7.jpg"
new_file = "Nationalpark Canyonlands, Utah, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "71fd073dd6ff231d1cdc4d430f037dd3f5e1ae485d78b6eabf6c81d838fe9db4.jpg"
new_file = "Brücke - Bigsweir Brücke über den Fluss Wye in der Nähe von Monmouth, Monmouthshire, Wales, UK.jpg"
collection.rename_file(old_file,new_file)

old_file = "c1815538d79d618aaf0ec40e74c1b144c3e0e2d0dccf606d2080df7326295960.jpg"
new_file = "Nationalpark Picos de Europa mit Stausee von Riano, Leon, Spanien.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Gemälde Jalais Hill, Pontoise von Camille Pissarro, The Metropolitan Museum of Art.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Löwe im Naturschutzgebiet Masai Mara, Kenia.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Nationalpark Unesco Weltnaturerbe Wǔlíngyuán (武陵源), Zhāngjiājiè (张家界市), Húnán (湖南省), China.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
##### Show Wallpapers in 20221217_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221217_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "0a88740fc29779d83d0852d1ef79312ba2cec5393a92b7132075dff17b8e603d.jpg"
new_file = "Miradouro da Boca do Inferno, Sete Cidades, São Miguel, Azoren, Portugal.jpg"
collection.rename_file(old_file,new_file)

old_file = "6c2168460cf77cdfe6b83a3e5b0861fc5f56171f1cc7666761196e156965320f.jpg"
new_file = "Akropolis, Athen, Griechenland.jpg"
collection.rename_file(old_file,new_file)

old_file = "7d1633d2edcfa7e266563ec7721b6585ef810a9ae34ae4af5a522ead9bd3d9f4.jpg"
new_file = "Point of Arches, Olympic National Park, Washington, Usa.jpg"
collection.rename_file(old_file,new_file)

old_file = "fa1615ac22b46fae7b9614993eca64614cb210e21f93dafbd571f3e6058daad4.jpg"
new_file = "Leuchtturm in Castlepoint, Wairarapa, Wellington, Neuseeland.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Sizilien, Italien.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Weihnachtsbaum auf dem Weinberg mit Watzmann, Plateau Steinernes Meer, Bayern, Deutschland.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
##### Show Wallpapers in 20221226_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20221226_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "b923a9e9134f22a210f2e85c44b595dd7ae5c4b63ccca84e49a82806e2020949.jpg"
new_file = "Grand View Point im Canyonlands-Nationalpark bei Moab, Utah, USA.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Gezeitenbecken am Sandstrand bei Sonnenuntergang, Teneriffa, Kanarische Inseln, Spanien.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Miradouro da Boca do Inferno, Sete Cidades, São Miguel, Azoren, Portugal.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Point of Arches, Olympic National Park, Washington, Usa.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Weihnachtsbaum auf dem Weinberg mit Watzmann, Plateau Steinernes Meer, Bayern, Deutschland.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Zebras Familie, Tansania. Afrika.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

### 2023

<!-- #region heading_collapsed=true -->
#### Q1
<!-- #endregion -->

<!-- #region heading_collapsed=true hidden=true -->
##### January/

<!-- #endregion -->

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230127_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230127_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
# old_file = "0bbb6e84d0b83b7c5994380b8a30b828b3a91747976806605438ae09e1d0ffb7.jpg"
# new_file = "Bay - Strand von Lamai bei Sonnenaufgang, Koh Samui, Thailand.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "39b2412803ac1432781c9ec2d9742d06f758b3a88cbb3824b539356b64bb4556.jpg"
# new_file = "Bay - Insel Porquerolles, Iles d'Hyeres, Var, Provence-Alpes-Côte d'Azur, Frankreich.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "5dd42bf850b59b272370414ca2a5d4ade9d985927130b16e9654044f76059b2e.jpg"
# new_file = "Rapsfeld in Hamm, Nordrhein-Westfalen, Deutschland.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "810d4c989ef2a9f8c83d98d44284de0c38ccdaa0c9b87096734b2c7ce9c8c0ca.jpg"
# new_file = "Wasserfall - Erawan-Wasserfall mit Fischteich im Erawan-Nationalpark, Provinz Kanchanaburi, Thailand.jpg"
# collection.rename_file(old_file,new_file)

old_file = "a3a2c3da5f3325a07d8bfc357802ceec80859b12d849cb15fb0daf2cd0290164.jpg"
new_file = "Bay - Loggas Beach, Esperii, Korfu, Griechenland.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Brücke - Bigsweir Brücke über den Fluss Wye in der Nähe von Monmouth, Monmouthshire, Wales, UK.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230129_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230129_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "1cc7a8bada703ab93ea4999622ad6f88ec97ffd098976434c4680904cc42a1d0.jpg"
new_file = "Royal Flora Ratchaphruek Park in Abenddämmerung, Chiang Mai, Thailand.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Bay - Loggas Beach, Esperii, Korfu, Griechenland.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

old_file = "bec9da1da4cd08d2a5181896229f3ec7d924b9fa2610e29449e38e33cb444530.jpg"
new_file = "Gemälde - Lake George von John Frederick Kensett, 1872.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Brücke - Bigsweir Brücke über den Fluss Wye in der Nähe von Monmouth, Monmouthshire, Wales, UK.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

old_file = "c315de6d0aa14dae917498d59cd42bed46eff642110dabf8f658622df66a54ad.jpg"
new_file = "Kap Kamenjak, Adria, Kroatien.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Wasserfall - Erawan-Wasserfall mit Fischteich im Erawan-Nationalpark, Provinz Kanchanaburi, Thailand.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230201_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230201_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "2203e62d91fcb8e4f0dfc6a691e58a536a726163d83666ae40016d5bfef7cfbc.jpg"
new_file = "Haubenkapuzineraffe (Sapajus Apella), Bonito, Mato Grosso do Sul, Brasilien.jpg"
collection.rename_file(old_file,new_file)

old_file = "3f79f3e8735ec41a357b7112629175c2d22e54c08056a04efd2af849eba394ae.jpg"
new_file = "Eiffelturm gesehen am Ende der Straße in Paris, Île-de-France, Frankreich.jpg"
collection.rename_file(old_file,new_file)

old_file = "a5bbc8eb7edafeec416a27677be0f2d51e71dd1dac28f00fc2873550b78b2b41.jpg"
new_file = "Regenbogenberg bei Sonnenuntergang, Zhangye Danxia Geo-Nationalpark, Gansu, China.jpg"
collection.rename_file(old_file,new_file)

old_file = "b0e3df66c7168e3a454f8c0c0607f27a7bceda56fdba04036a31f3100f25d381.jpg"
new_file = "Sternentstehungsstätte NGC 3324 in der nordwestlichen Ecke des Carinanebels.jpg"
collection.rename_file(old_file,new_file)

old_file = "ca57f8fdf73e08035205948715a8f19b7bb347034feac4fbcc97c2813edd4c60.jpg"
new_file = "Kirche San Juan auf der Insel Gaztelugatxe und lange Steintreppe, Golf von Biskaya, Spanien.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Yamanakasee und Berg Fuji, Präfektur Yamanashi, Japan.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
##### February/
<!-- #endregion -->

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230205_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230205_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "4d2156c2e3617982b7933bc467ad5d6361c1348602c53e831209b9c6dfa8a761.jpg"
new_file = "Big Ben und Houses Of Parliament mit Westminster Bridge.jpg"
collection.rename_file(old_file,new_file)

old_file = "9baee670fdc64e9db7513797f71b6617091367fcf96bd1318346bb8a99068c06.jpg"
new_file = "Tulpen als Regenbogen, Noord-Holland, Niederlande.jpg"
collection.rename_file(old_file,new_file)

old_file = "b383a9300fd29f69a20d9ea5e0c469008d7d4f39f0f7b78760ba541abdc6b91f.jpg"
new_file = "Brücke - Tower Bridge bei Sonnenuntergang, London, England, UK.jpg"
collection.rename_file(old_file,new_file)

old_file = "b93aa54e08fb7d1b672d7bffb332bf73d96e942696dad2a2c67358d840bb5cbc.jpg"
new_file = "Laguna del Caminante im Nationalpark Tierra del Fuego, Ushuaia, Argentinien.jpg"
collection.rename_file(old_file,new_file)

old_file = "e2b7b79e9e4c0a36879c6d5282068517c0ea9e0d387f83e0fc639cc1993ab055.jpg"
new_file = "Palácio da Pena aus Vogelperspektive, Sintra, Lissabon, Portugal.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Reschensee unter dramatischem Himmel bei Sonnenuntergang, Südtirol, Italien.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region hidden=true -->
####### Show Wallpapers in 20230209_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230209_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "3739ba0dc52f19e9ca4b4d135d92bf6aa116cec6cfee195809cd694f0d9f28e7.jpg"
new_file = "Lorbeerwald bei Sonnenuntergang, Madeira.jpg"
collection.rename_file(old_file,new_file)

old_file = "3f705d21f8285d7242ea91d2ba1a552a26a08bf1e736634184a4b158c603a51d.jpg"
new_file = "Dyrhólaey, Pfad entlang einer Lagune, Island.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Big Ben und Houses Of Parliament mit Westminster Bridge.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Spitzmaulnashorn.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Toge, Tokamachi, Niigata 942-1351, Japan.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Wasserfall - Celeste-Fluss im Tenorio-Nationalpark, Costa Rica.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230215_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230215_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```

<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "76081aa2d365f87be53baa378ff646f81799ae5be36b7ded3526c73ac736a9bb.jpg"
new_file = "Bay - Beauty Beach mit Kalksteinfelsen, Thailand.jpg"
collection.rename_file(old_file,new_file)

old_file = "88cb7f2c449f0fb9c8b196fda27277d599f28c43fe3ff365344dc376e0291b5c.jpg"
new_file = "Astronomie - Eph Hanks Tower und Milchstraße, Capitol Reef National Park, Utah, USA.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Bellagio - Perle des Comer Sees, Italien.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

old_file = "cbe08dfd20aa279904886c7dc465ee9f925a587bf96b948262f4f844db8f6c2f.jpg"
new_file = "Lofoten bei Sonnenaufgang im Sommer, Nordland, Norwegen.jpg"
collection.rename_file(old_file,new_file)

old_file = "e8f91a5f0327ecaec496ede1d22ee7cc3875c2bc0fa4ea06914b016f8fa4481a.jpg"
new_file = "Rosapelikane in Formation am Avis-Staudamm am Stadtrand von Windhoek, Namibia.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Ounianga Kebir, Sahara, Tschad.jpg"
# new_file = "Ennedi-Massiv, Ennedi Est und Ouest, Tschad.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230219_1/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230219_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "4d1bb55f6481c051f670e9c5bb5905f40b3a70ac487d58224edaa9c70846a623.jpg"
new_file = "Donaudurchbruch bei Weltenburg, Weltenburger Enge (NSG200.002) zwischen Kelheim und dem Kloster Weltenburg, südliche Frankenalb, Bayern, Deutschland.jpg"
collection.rename_file(old_file,new_file)

old_file = "9b4a1325076381ee2533368b05c1abee714cdf14c61a24a6913fcbd7a61298a2.jpg"
new_file = "Weiße Wüste, Sahara, Ägypten.jpg"
collection.rename_file(old_file,new_file)

old_file = "af1fb5804a39baddfa4a46865b14c93a6dec0d78a8ac27bfac9d57dbf49ddf12.jpg"
new_file = "Nationalpark - Turmbogenansicht durch das Nordfenster im Arches-Nationalpark, Moab, Utah, USA.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Brücke - Bigsweir Brücke über den Fluss Wye in der Nähe von Monmouth, Monmouthshire, Wales, UK.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Brücke - Manhattan Bridge über den East River, New York City, USA.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Nationalpark Tasmanien - Kap Raoul, Tasmanien, Australien.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
##### March/

<!-- #endregion -->

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230325_1/

<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230325_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "27b13ca21857c6fef584dab07f02d389c58b0a8b6d08c1a527c123e0675b4a66.jpg"
new_file = "Guelta d’Archei, Ennedi-Plateau, Tschad.jpg"
collection.rename_file(old_file,new_file)

old_file = "43888d28cfdaa9ca696f4e18125e92bc47e94539f673e1902a93fdcc301ed635.jpg"
new_file = "Drei Capybara (Hydrochaeris Hydrochaeris) am Flussufer, Pantanal, Mato Grosso, Brasilien.jpg"
collection.rename_file(old_file,new_file)

old_file = "52d5ae8b85527edda43ad27c01b82639b026af54b0247f1e472eef965e4d5078.jpg"
new_file = "Tai Mei Tuk (大美督 dà měi dū), Tai Po District (大埔區 dà pǔ qū), Hong Kong (香港 Xiānggǎng), China.jpg"
collection.rename_file(old_file,new_file)

old_file = "57759c5704fbda1ac25de3e0dca8394643b777c34eb08fd9ad070f79b852d669.jpg"
new_file = "Nationalpark - Peruanischer Amazonas-Regenwald im Tambopata National Reserve, Madre de Dios, Peru.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Big Ben und Houses Of Parliament mit Westminster Bridge.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

old_file = "d3ab6823c5d824bf0a8f79fcc5fdc5a03f3a2a0cb45d9140c38d746ced732066.jpg"
new_file = "Himalaya Mount Everest in einer nebligen Sonnenuntergangsnacht, Sagarmatha Nationalpark, Nepal.jpg"
collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

#### Q2

<!-- #region heading_collapsed=true -->
#####  April/
<!-- #endregion -->

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230403_1/

<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230403_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "17ab0900458371232f7e3f8ae10948193bdc3060bd7c1d669a81f284c366b271.jpg"
new_file = "TTTTT.jpg"
collection.rename_file(old_file,new_file)

old_file = "5769a5357bb1acbef5a74650795f02cc00502af01fed84be392836f5089c4e89.jpg"
new_file = "TTTTT.jpg"
collection.rename_file(old_file,new_file)

# old_file = "6f35985afb0041cd71055f6951fc92907dca94aed9b39eac721c96a98cdb0217.jpg"
# new_file = "Leuchtturm in Pilsum, Gemeinde Krummhörn, Ostfriesland, Deutschland.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Big Ben und Houses Of Parliament mit Westminster Bridge.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Naturpark Sintra-Cascais, Azenhas Do Mar, Costa de Lisboa, Portugal.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Wasserfall - Salto Ángel, Auyan-Tepui, Nationalpark Canaima, Venezuela.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true -->
##### May/

<!-- #endregion -->

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230510_1/

<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230510_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "6bc8203268cc29ed7ee6ff8d999db96f2264a20e9985e06fa1d685012f20e402.jpg"
new_file = "Happisburgh Lighthouse.jpg"
collection.rename_file(old_file,new_file)

old_file = "7c655da21b075c3b05a09c37e2b44e592f05bcd4f915f63340b94b68194470b8.jpg"
new_file = "Smaragdfarbene Gletscherflüsse in Vogelperspektive, Mýrdalsjökull, Hochland von Island.jpg"
collection.rename_file(old_file,new_file)

old_file = "89747e97dfcc97b2d0d3a2d992b905784628af6e9405384d208651f871937b7c.jpg"
new_file = "TTTTT.jpg"
collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230516_2/
<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230516_2"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "1e252a0b5f946b5862b358fff74a653fa00df8fa0b77c7754e95f2055c86d9da.jpg"
new_file = "Moscow, Idaho, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "28440766fd76ce2579ca1d336500eda764b9a796de0b37e161188b84467d7fa6.jpg"
new_file = "Budapest-Schloss und berühmte Hängebrücke in Budapest, Ungarn.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Windmühle Schellemolen, Noorweegse Kaai 4, 8340 Damme, Belgien.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)



print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230518_1/

<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230518_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "06dc33d14ae449b02309d5a7c10ad797fb846a1ab52f8c1ac2b676b8db9c46ff.jpg"
new_file = "Eilean Donan Castle, Dornie, Kyle of Lochalsh IV40 8DX, Vereinigtes Königreich.jpg"
collection.rename_file(old_file,new_file)

old_file = "210f0cecad28e5cf003a91891ebaf5d42f0baf507b5040c544fade895a387759.jpg"
new_file = "La plage d'Étretat, Normandie, Frankreich.jpg"
collection.rename_file(old_file,new_file)

old_file = "46206cfe6ace03c9ecbc57b6a5c760a0687f23ed8479ae0d6c750cc53bfc4e73.jpg"
new_file = "Royal Flora Ratchaphruek Garden, der berühmteste öffentliche Garten, Chiengmai, Thailand.jpg"
collection.rename_file(old_file,new_file)

old_file = "816267e1d983a5439e86110ee1f2d5f22edf34c04e5f8fc83dd3292b3c2c4c96.jpg"
new_file = "Indianischer Wachturm, Desert View am Südrand des Grand Canyon, Arizona, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "98a03f4124db21993586fbc9ee6140c67b4ee45d91223b2ef1d91612a9fc64b6.jpg"
new_file = "Berg Montserrat, Barcelona, ​​Katalonien, Spanien.jpg"
collection.rename_file(old_file,new_file)

old_file = "ca9622f60629ec1d38b92e4377cedcf180b7683caf31e87d8deee0e9c04fa498.jpg"
new_file = "Keilschwanz-Regenpfeifer über dem Teich im Venice Area Audubon Rookery, Venice, Florida, USA.jpg"
collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230524_1/

<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230524_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "5e8d7582e0e4cce751a704c33889221e5095658b84ceee62731960f44b8e5247.jpg"
new_file = "Sonora-Wüste mit Saguaro-Kaktus bei Sturmregen, Arizona, USA.jpg"
collection.rename_file(old_file,new_file)

old_file = "dc0e4390414a6e965657bf2dbf3e4e3148aee93d8210923c29e74265349ccf98.jpg"
new_file = "Burg Santa Bárbara auf dem Berg Benacantil mit Regenbogen, Alicante, Spanien.jpg"
collection.rename_file(old_file,new_file)

old_file = "dc80adad2a54aab52c61d9acc727e9861023d4dd089b092827d98058db8a0d11.jpg"
new_file = "Prebischtor, Böhmische Schweiz, Elbsandsteingebirge, Tschechien.jpg"
collection.rename_file(old_file,new_file)



print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true hidden=true -->
###### Show Wallpapers in 20230526_1/

<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230526_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true hidden=true -->
###### renaming

<!-- #endregion -->

```python hidden=true
old_file = "2ac7e2da1797cac4917b447ac3dcf8416e9b375ade049a76bba8bb244e2d6fec.jpg"
new_file = "Mitre Peak im Milford Sound (Piopiotahi) Fjord, Southland, New Zealand.jpg"
collection.rename_file(old_file,new_file)

old_file = "374de43a94cd95fee331bc29ca0405ae7a2bbcadb353ebf388cf74edae7124fd.jpg"
new_file = "Glenfinnan-Viadukt der West Highland Line, Glenfinnan, Highland, Schottland.jpg"
collection.rename_file(old_file,new_file)

old_file = "bc6f07ca7553dd064b03d4d29e47ae952503becfecccb9e00e9da518ee255454.jpg"
new_file = "Lagune auf Pulau Ubin (Granitinsel), Singapur.jpg"
collection.rename_file(old_file,new_file)

# old_file = "Moscow, Idaho, USA.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Nationalforst Monongahela, West Virginia, USA.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)

# old_file = "Sternentstehungsstätte NGC 3324 in der nordwestlichen Ecke des Carinanebels.jpg"
# new_file = "TTTTT.jpg"
# collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

##### June/


<!-- #region heading_collapsed=true -->
###### Show Wallpapers in 20230602_1/

<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230602_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')
```


<!-- #region heading_collapsed=true -->
###### renaming
<!-- #endregion -->

```python hidden=true
old_file = "37a097cb1264605cc25d10e2ecc3ab37cd86043248750100254539e2df0a0800.jpg"
new_file = "Daigo-ji-Tempel in Fushimi-ku, Kyoto, Japan.jpg"
collection.rename_file(old_file,new_file)

old_file = "3dab4cdffc9788229508b366217e2e5531b68d63226a1992c258e7d525d844d6.jpg"
new_file = "Castel del Monte, Parco Nazionale dell'Alta Murgia, Apulien, Italien.jpg"
collection.rename_file(old_file,new_file)

old_file = "6ba0855c242d80c4e78f28d52321d4c75c7513d634673b44fe58da2c34b3f1e5.jpg"
new_file = "Badlands-Nationalparks, South-Dakota, USA.jpg"
collection.rename_file(old_file,new_file)


print("\nDo NOT forget to reload collection!")
```

<!-- #region heading_collapsed=true -->
###### Show Wallpapers in 20230613_1/

<!-- #endregion -->

```python hidden=true
collection = SplashScreenCollection()
folder_name = "20230613_1"
if collection.set_path_name(folder_name,True):
    print(f'[ OK ] {folder_name=} exists')
    collection.show()
else:
    print(f'[FAIL] {folder_name=} not found')

```

<!-- #region heading_collapsed=true -->
###### renaming
<!-- #endregion -->

## Final Destination Collection/


### Codebase 3-1 (Merge)

<!-- #region heading_collapsed=true -->
#### Test `append()`:
<!-- #endregion -->

```python hidden=true
destination = SplashScreenCollection(False)
folder_name = "Collection"
if destination.set_path_name(folder_name):
    print(f'[ OK ] {folder_name=} exists')
    for w in collection:
        destination.append(w)
else:
    print(f'[FAIL] {folder_name=} not found')
```


#### Test `from_collection()` 

```python
destination = SplashScreenCollection(False)
destination.set_path_name("Collection")

destination.from_collection(collection)

```

### Show Wallpapers in Collection/


Execute if all wallpapers in final collection **need to be reloaded**, e.g. because some were removed.

```python
destination = SplashScreenCollection(False)
destination.set_path_name("Collection")
```


Show all wallpapers in final collection in a dataframe after making them clickable directing the the results of a reverse Google Image Search.

```python
destination.show()
```

```python
remove_non_wallpapers("Collection")
```

```python
from pathlib import Path

folder = Path(".") / "Collection"

old_file = folder / "Claiborne Pell Newport Bridge view with rocky seascape at sunrise, Jamestown, Rhode Island, USA.jpg"
new_file = folder / "Brücke - Claiborne Pell Newport Bridge view with rocky seascape at sunrise, Jamestown, Rhode Island, USA.jpg"
rename_file(old_file,new_file)

```


```python
from pathlib import Path

folder = Path(".") / "Collection"

path = folder / "Robberg-Halbinsel, Südafrika.jpg"
path.unlink()

```

## TODO: Find Locations Parsing Results of Google Reverse Image Search

```python
!pip install googlesearch-python
```

```python
from googlesearch import search
####
# googlesearch.search(str: term, int: num_results=10, str: lang="en") -> list
#   rf. https://pypi.org/project/googlesearch-python/
#   and https://github.com/Nv7-GitHub/googlesearch
####
search("Google") # Returns the list of first page results of searching the keyword "Google" with Google
```

List results of the Google search specified by the link given by variable `url`:

```python
from bs4 import BeautifulSoup
from requests import get

def location_of_image(url):
    '''
    Get the results of the Google Reverse Image Search given by `url` and 
    try to determine where the photo was taken
    '''
    usr_agent = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      # 'Chrome/61.0.3163.100 Safari/537.36'
    }
    def fetch_results(search_url):
        response = get(search_url, headers=usr_agent)
        response.raise_for_status()
        return response.text

    def parse_results(raw_html):
        soup = BeautifulSoup(raw_html, 'html.parser')
        result_block = soup.find_all('div', attrs={'class': 'g'})
        # FIXME: len(result_block)=0
        print(f'{len(result_block)=}')
        for result in result_block:
            link = result.find('a', href=True)
            title = result.find('h3')
            text = link.get_text()
            print("Extracted {}".format(text))
            if link and title:
                yield text[:text.index("https://")] # + " -> " + link['href']
    
    # return("location_of_image({})".format(url))
    
    html = fetch_results(url)
    results = list(parse_results(html))
    # results = ["links","from","Google"]
    results = ''.join([ "<li>{}</li>".format(elem) for elem in results ])
    return ''.join(['<ol>',results,'</ol>'])


# to search
url = "http://www.google.com/search?tbs=sbi:AMhZZiur1dWe7P_1e3XOQOu6LqpKWxDu7IITdc4LKlgmF2bbOpB4L-WVRz1nGcel_1s8n7U7_18qUWsGgHXvjUj_15wctPXJzR2v1D35RaofRc8GIzawOL2HuMcSL2mydMCtQzsnmMI0ZyMdne-QSIPrcc8AWqh8rzIX2THdKQTV-_1PEYsjUh66HOIj3odw4DZVIS4YoFImy_1DxNCJ1we26UFZDAkRS-4UMQLMMjPGKJ8VgHjfnpbRkJzRPjnSbv3QWwsEymnfAe3faR4kw91j2h6K2ZhspXqXVlnth9Ki-daDeeHm5WVIv_1Goj2o-lBCrWCg2eufMOmE2pM6BEK3Yyb7IvC05nKNDiRww"
print(f'{url=}')

location_of_image(url)
```

```python
from bs4 import BeautifulSoup
from requests import get

def search_url(passed_url):
    usr_agent = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      # 'Chrome/61.0.3163.100 Safari/537.36'
    }
    def fetch_results(raw_url):
        # escaped_search_term = search_term.replace(' ', '+')
        # google_url = 'https://www.google.com/search?q={}&num={}&hl={}'.format(escaped_search_term, number_results+1,language_code)
        
        print(fetch_results.__name__+": raw_url='%s'" % raw_url)
        response = get(raw_url, headers=usr_agent)    
        response.raise_for_status()

        # print(fetch_results.__name__+": response.text='%s'" % response.text)
        return response.text
    def parse_results(raw_html):
        soup = BeautifulSoup(raw_html, 'html.parser')
        result_block = soup.find_all('div', attrs={'class': 'g'})
        show_enter_loop = True
        for result in result_block:
            link = result.find('a', href=True)
            title = result.find('h3')
            text = link.get_text()
            print("Extracted {}".format(text))
            if link and title:
                # yield text[:text.index("https://")] + " -> " + link['href']
                yield text[:text.index("https://")]

    html = fetch_results(passed_url)
    return list(parse_results(html))

# to search
url = "http://www.google.com/search?tbs=sbi:AMhZZiur1dWe7P_1e3XOQOu6LqpKWxDu7IITdc4LKlgmF2bbOpB4L-WVRz1nGcel_1s8n7U7_18qUWsGgHXvjUj_15wctPXJzR2v1D35RaofRc8GIzawOL2HuMcSL2mydMCtQzsnmMI0ZyMdne-QSIPrcc8AWqh8rzIX2THdKQTV-_1PEYsjUh66HOIj3odw4DZVIS4YoFImy_1DxNCJ1we26UFZDAkRS-4UMQLMMjPGKJ8VgHjfnpbRkJzRPjnSbv3QWwsEymnfAe3faR4kw91j2h6K2ZhspXqXVlnth9Ki-daDeeHm5WVIv_1Goj2o-lBCrWCg2eufMOmE2pM6BEK3Yyb7IvC05nKNDiRww"
print(url)

for j in search_url(url):
    print(j)
```

List results of searching the string in variable `query` with Google:

```python
from bs4 import BeautifulSoup
from requests import get

def search(term, num_results=10, lang="en", proxy=None):
    usr_agent = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/61.0.3163.100 Safari/537.36'}

    def fetch_results(search_term, number_results, language_code):
        escaped_search_term = search_term.replace(' ', '+')

        google_url = 'https://www.google.com/search?q={}&num={}&hl={}'.format(escaped_search_term, number_results+1,
                                                                              language_code)
        proxies = None
        if proxy:
            if proxy[:5]=="https":
                proxies = {"https":proxy} 
            else:
                proxies = {"http":proxy}
        
        response = get(google_url, headers=usr_agent, proxies=proxies)    
        response.raise_for_status()

        return response.text

    def parse_results(raw_html):
        soup = BeautifulSoup(raw_html, 'html.parser')
        result_block = soup.find_all('div', attrs={'class': 'g'})
        for result in result_block:
            link = result.find('a', href=True)
            title = result.find('h3')
            text = link.get_text()
            if link and title:
                yield text[:text.index("https://")] + " -> " + link['href']

    html = fetch_results(term, num_results, lang)
    return list(parse_results(html))

# to search
query = "Geeksforgeeks"

for j in search(query, num_results=10):
    print(j)
```

```python
try:
    from googlesearch import search
except ImportError:
    print("No module named 'google' found")
 
# to search
query = "Geeksforgeeks"
# query = "A computer science portal"

# googlesearch.search(str: term, int: num_results=10, str: lang="en") -> list

for j in search(query, num_results=10):
    print(j)
```

```python
from googlesearch import search
import urllib
from bs4 import BeautifulSoup

def google_scrape(url):
    thepage = urllib.urlopen(url)
    soup = BeautifulSoup(thepage, "html.parser")
    return soup.title.text

i = 1
query = 'search this'
for url in search(query, stop=10):
    a = google_scrape(url)
    print(str(i) + ". " + a)
    print(url)
    print(" ")
    i += 1

```

```python
import urllib
import json as m_json
query = input ( 'Query: ' )
query = urllib.parse.urlencode ( { 'q' : query } )
response = urllib.request.urlopen ( 'https://www.google.com/search?&q=Forest' + query ).read()
json = m_json.loads ( response )
results = json [ 'responseData' ] [ 'results' ]
for result in results:
    title = result['title']
    url = result['url']   # was URL in the original and that threw a name error exception
    print ( title + '; ' + url )
```
